This directory is for Elisp libraries that aren't managed by ‘package.el’.

To download a library from its repo, for example Sunrise Commander from https://github.com/escherdragon/sunrise-commander, run

  git submodule add https://github.com/escherdragon/sunrise-commander.git ~/.emacs.d/non-elpa-packages/sunrise-commander

See 'man gitsubmodules' and 'git help submodule' for more information.
