(defun scratchvenv ()
  "Activate the \"safety\" virtual environment."
  (interactive)
  (pythonic-activate (expand-file-name "~/src/python/scratch/.scratchvenv")))

(defun lpvenv ()
  "Activate the virtual environment for the excercises in Fabrizio Romano's book
\"Learning Python\"."
  (interactive)
  (pythonic-activate (expand-file-name "~/src/python/learning_python/.lpvenv")))
