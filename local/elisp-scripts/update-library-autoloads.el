#!/usr/bin/env -S emacs -Q --script

;; Use this function to byte-compile a manually installed library and enable its
;; autoloads from cookies.
;;
;; Usage:
;;
;;   $ path/to/update-library-autoloads.el <library name w/o extension>
;;
;; REVIEW: Why does it create a file '...-autoloads.el~'?

(let* ((library-basename (format "%s" (pop argv)))
       (library-dir (concat
                     user-emacs-directory
                     (file-name-as-directory "local/non-elpa-packages")
                     library-basename))
       ;; Don't change the name of this variable or ‘update-directory-autoloads’
       ;; won't work. See ‘C-h f update-directory-autoloads’.
       (generated-autoload-file (concat
                                 (file-name-as-directory library-dir)
                                 library-basename
                                 "-autoloads.el")))
  (add-to-list 'load-path library-dir t)
  ;; Update autoloads from cookies
  (update-directory-autoloads library-dir)
  ;; Byte-compile Elisp files
  (byte-recompile-directory library-dir 0 nil))
