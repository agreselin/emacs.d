;; Fix bug#52356: Isearch prompt changes very briefly at every character typed
;; when char folding is enabled
;;
;; Patch author: Juri Linkov
;; URL: https://lists.gnu.org/archive/html/bug-gnu-emacs/2021-12/msg01040.html
;; Fixed in version 29.0.50
;;
;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=52356
;;
;; How to patch:
;;
;; Run
;;
;;   zcat /usr/share/emacs/28.1/lisp/isearch.el.gz > ~/.emacs.d/local/patches/isearch/isearch.el
;;   patch ~/.emacs.d/local/patches/isearch/isearch.el ~/.emacs.d/local/patches/isearch/bug52356_isearch-adjusted-lax.patch
;;   emacs -Q --eval '(byte-compile-file "~/.emacs.d/local/patches/isearch/isearch.el")'
;;
;; In order load fewer things, in ‘isearch-patched-functions.el’ I’ve
;; kept only the two functions that the patch modifies.

(defun isearch-message-prefix (&optional ellipsis nonincremental)
  ;; If about to search, and previous search regexp was invalid,
  ;; check that it still is.  If it is valid now,
  ;; let the message we display while searching say that it is valid.
  (and isearch-error ellipsis
       (condition-case ()
	   (progn (re-search-forward isearch-string (point) t)
		  (setq isearch-error nil))
	 (error nil)))
  ;; If currently failing, display no ellipsis.
  (or isearch-success (setq ellipsis nil))
  (let ((m (concat (if isearch-success "" "failing ")
		   (if (eq isearch-adjusted t) "pending " "")
		   (if (and isearch-wrapped
			    (not isearch-wrap-function)
			    (if isearch-forward
				(> (point) isearch-opoint)
			      (< (point) isearch-opoint)))
		       "over")
		   (if isearch-wrapped "wrapped ")
                   (if (and (not isearch-success) (buffer-narrowed-p) widen-automatically)
                       "narrowed " "")
                   (if (and (not isearch-success) (not isearch-case-fold-search))
                       "case-sensitive ")
                   (let ((prefix ""))
                     (advice-function-mapc
                      (lambda (_ props)
                        (let ((np (cdr (assq 'isearch-message-prefix props))))
                          (if np (setq prefix (concat np prefix)))))
                      isearch-filter-predicate)
                     prefix)
                   (isearch--describe-regexp-mode isearch-regexp-function)
		   (cond
		    (multi-isearch-file-list "multi-file ")
		    (multi-isearch-buffer-list "multi-buffer ")
		    (t ""))
		   (or isearch-message-prefix-add "")
		   (if nonincremental "search" "I-search")
		   (if isearch-forward "" " backward")
		   (if current-input-method
		       ;; Input methods for RTL languages use RTL
		       ;; characters for their title, and that messes
		       ;; up the display of search text after the prompt.
		       (bidi-string-mark-left-to-right
			(concat " [" current-input-method-title "]: "))
		     ": ")
		   )))
    (apply #'propertize (concat (isearch-lazy-count-format)
                        (upcase (substring m 0 1)) (substring m 1))
	   isearch-message-properties)))

(defun isearch-search-fun-default ()
  "Return default functions to use for the search."
  (lambda (string &optional bound noerror count)
    (let (;; Evaluate this before binding `search-spaces-regexp' which
          ;; can break all sorts of regexp searches.  In particular,
          ;; calling `isearch-regexp-function' can trigger autoloading
          ;; (Bug#35802).
          (regexp
           (cond (isearch-regexp-function
                  (let ((lax (and (not bound)
                                  (isearch--lax-regexp-function-p))))
                    (when lax
                      (setq isearch-adjusted 'lax))
                    (if (functionp isearch-regexp-function)
                        (funcall isearch-regexp-function string lax)
                      (word-search-regexp string lax))))
                 (isearch-regexp string)
                 (t (regexp-quote string))))
          ;; Use lax versions to not fail at the end of the word while
          ;; the user adds and removes characters in the search string
          ;; (or when using nonincremental word isearch)
          (search-spaces-regexp (when (if isearch-regexp
                                          isearch-regexp-lax-whitespace
                                        isearch-lax-whitespace)
                                  search-whitespace-regexp)))
      (funcall
       (if isearch-forward #'re-search-forward #'re-search-backward)
       regexp bound noerror count))))
