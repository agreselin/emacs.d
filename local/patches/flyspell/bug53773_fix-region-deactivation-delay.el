;; Fix bug#53773: The region stays active after typing in a character when
;; ‘flyspell-mode’ (in addition to ‘delete-selection-mode’ and
;; ‘transient-mark-mode’) is enabled
;;
;; Fixed in v. 29.1.
;;
;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=53773
;; https://git.savannah.gnu.org/cgit/emacs.git/commit/?id=118a911159e75f3ad9305cd7f298816bfb59d715

(defun flyspell-check-word-p ()
  "Return t when the word at `point' has to be checked.
The answer depends of several criteria.
Mostly we check word delimiters."
  (let ((ispell-otherchars (ispell-get-otherchars)))
    (cond
     ((<= (- (point-max) 1) (point-min))
      ;; The buffer is not filled enough.
      nil)
     ((and (and (> (current-column) 0)
		(not (eq (current-column) flyspell-pre-column)))
	   (save-excursion
	     (backward-char 1)
	     (and (looking-at (flyspell-get-not-casechars))
		  (or (string= "" ispell-otherchars)
		      (not (looking-at ispell-otherchars)))
		  (or flyspell-consider-dash-as-word-delimiter-flag
		      (not (looking-at "-"))))))
      ;; Yes because we have reached or typed a word delimiter.
      t)
     ((symbolp this-command)
      (cond
       ((get this-command 'flyspell-deplacement)
	(not (eq flyspell-previous-command this-command)))
       ((get this-command 'flyspell-delayed)
	;; The current command is not delayed, that
	;; is that we must check the word now.
        (deactivate-mark) ;; Fix bug #53773.
	(and (not unread-command-events)
	     (sit-for flyspell-delay)))
       (t t)))
     (t t))))
