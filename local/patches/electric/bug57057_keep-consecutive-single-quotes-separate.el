;; Fix bug #57057: ‘electric-quote-mode’ merges single quotes
;;
;; Fixed in v. 29.1
;;
;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=57057
;; https://git.savannah.gnu.org/cgit/emacs.git/commit/?id=41169b55340878120fdf695eb4ac1fcb2679e7b8
;;
;; How to patch:
;;
;; Run
;;
;;   zcat /usr/share/emacs/28.1/lisp/electric.el.gz > ~/.emacs.d/local/patches/electric/electric.el
;;   patch ~/.emacs.d/local/patches/electric/electric.el ~/.emacs.d/local/patches/electric/keep-consecutive-single-quotes-separate.el
;;   emacs -Q --eval '(byte-compile-file "~/.emacs.d/local/patches/electric/electric.el")'

(defcustom electric-quote-replace-consecutive t
  "Non-nil means to replace a pair of opening or closing single
quotes with the corresponding double quote when the second quote
of the pair is entered electrically (i.e. by typing ` or ')."
  :version "29.1"
  :type 'boolean :safe #'booleanp :group 'electricity)

(defun electric-quote-post-self-insert-function ()
  "Function that `electric-quote-mode' adds to `post-self-insert-hook'.
This requotes when a quoting key is typed."
  (when (and electric-quote-mode
             (or (eq last-command-event ?\')
                 (and (not electric-quote-context-sensitive)
                      (eq last-command-event ?\`))
                 (and electric-quote-replace-double
                      (eq last-command-event ?\")))
             (not (run-hook-with-args-until-success
                   'electric-quote-inhibit-functions))
             (if (derived-mode-p 'text-mode)
                 electric-quote-paragraph
               (and comment-start comment-use-syntax
                    (or electric-quote-comment electric-quote-string)
                    (let* ((syntax (syntax-ppss))
                           (beg (nth 8 syntax)))
                      (and beg
                           (or (and electric-quote-comment (nth 4 syntax))
                               (and electric-quote-string (nth 3 syntax)))
                           ;; Do not requote a quote that starts or ends
                           ;; a comment or string.
                           (eq beg (nth 8 (save-excursion
                                            (syntax-ppss (1- (point)))))))))))
    (pcase electric-quote-chars
      (`(,q< ,q> ,q<< ,q>>)
       (save-excursion
         (let ((backtick ?\`))
           (if (or (eq last-command-event ?\`)
                   (and (or electric-quote-context-sensitive
                            (and electric-quote-replace-double
                                 (eq last-command-event ?\")))
                        (save-excursion
                          (backward-char)
                          (skip-syntax-backward "\\")
                          (or (bobp) (bolp)
                              (memq (char-before) (list q< q<<))
                              (memq (char-syntax (char-before))
                                    '(?\s ?\())))
                        (setq backtick ?\')))
               (cond ((and electric-quote-replace-consecutive
                           (search-backward (string q< backtick) (- (point) 2) t))
                      (replace-match (string q<<))
                      (when (and electric-pair-mode
                                 (eq (cdr-safe
                                      (assq q< electric-pair-text-pairs))
                                     (char-after)))
                        (delete-char 1))
                      (setq last-command-event q<<))
                     ((search-backward (string backtick) (1- (point)) t)
                      (replace-match (string q<))
                      (setq last-command-event q<))
                     ((search-backward "\"" (1- (point)) t)
                      (replace-match (string q<<))
                      (setq last-command-event q<<)))
             (cond ((and electric-quote-replace-consecutive
                         (search-backward (string q> ?') (- (point) 2) t))
                    (replace-match (string q>>))
                    (setq last-command-event q>>))
                   ((search-backward "'" (1- (point)) t)
                    (replace-match (string q>))
                    (setq last-command-event q>))
                   ((search-backward "\"" (1- (point)) t)
                    (replace-match (string q>>))
                    (setq last-command-event q>>))))))))))
