Sometimes patching the source code relative to the byte-compiled
binary returned by ‘M-x locate-library RET LIBRARY RET’ and
recompiling it doesn’t work because some libraries actually are baked
into the executable when going through the `make` process. In that
case you'll need to evaluate your patched version after Emacs starts
up. In addition, certain variables (such as those assigned with
‘defvar’) will need special treatment if you change the value of the
‘defvar’ without using something like ‘setq’.

See the loadup library
    lawlist’s comment on https://emacs.stackexchange.com/questions/69829/how-to-load-a-patched-library#comment112171_69829
