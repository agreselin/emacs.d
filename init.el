;;;;; Environment

;;;; Variable definitions

(defvar my-emacs-directory
  (file-name-as-directory (expand-file-name "local" user-emacs-directory))
  "Directory of personal, locally managed Emacs files.")

(defvar my-lib-dir
  (file-name-as-directory (expand-file-name "non-elpa-packages" my-emacs-directory))
  "Directory of Emacs libraries that are not managed by ‘package.el’.")

(defvar my-shell "/usr/bin/bash")

(defvar my-terminal "/usr/bin/gnome-terminal")

(defvar my-file-manager "/usr/bin/nautilus")



;;;; Libraries

;; Load newer source code rather than old byte-code
;; NOTE: if you customize this, obviously it will not affect files that are loaded before your customizations are read! (From ‘C-h v load-prefer-newer’.)
;; https://ambrevar.xyz/emacs2/
(setq load-prefer-newer t)

;;; Package archives

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Package-Installation.html
;; https://www.emacswiki.org/emacs/MELPA

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

;; List of packages installed explicitly by the user
;; Type ‘M-x package-install-selected-packages’ to install them.
;; Also used by the package manager to distinguish between packages installed on request of the user and packages installed as dependencies.
;; http://endlessparentheses.com/new-in-package-el-in-emacs-25-1-user-selected-packages.html
(setq package-selected-packages
      '(anaconda-mode ;           https://github.com/proofit404/anaconda-mode
        adaptive-wrap ;           https://elpa.gnu.org/packages/adaptive-wrap.html
        auctex ;                  https://www.gnu.org/software/auctex/
        avy ;                     https://github.com/abo-abo/avy
        browse-kill-ring ;        https://github.com/browse-kill-ring/browse-kill-ring
        company ;                 http://company-mode.github.io/
        company-anaconda ;        https://github.com/proofit404/anaconda-mode
        company-math ;            https://github.com/vspinu/company-math
        company-reftex ;          https://github.com/TheBB/company-reftex
        csv-mode ;                https://elpa.gnu.org/packages/csv-mode.html
        easy-kill ;               https://github.com/leoliu/easy-kill
        erc ;                     https://www.gnu.org/software/emacs/erc.html
        ess ;                     https://ess.r-project.org/
        expand-region ;           https://github.com/magnars/expand-region.el
        flycheck ;                https://www.flycheck.org
        gnuplot ;                 https://github.com/emacs-gnuplot/gnuplot
        highlight-indent-guides ; https://github.com/DarthFennec/highlight-indent-guides
        idomenu ;                 https://github.com/birkenfeld/idomenu
        imenu-anywhere ;          https://github.com/vspinu/imenu-anywhere
        julia-mode ;              https://github.com/JuliaEditorSupport/julia-emacs
        markdown-mode ;           https://jblevins.org/projects/markdown-mode/
        multiple-cursors ;        https://github.com/magnars/multiple-cursors.el
        nhexl-mode ;              https://elpa.gnu.org/packages/nhexl-mode.html
        olivetti ;                https://github.com/rnkn/olivetti
        openwith ;                https://github.com/jpkotta/openwith
        quack ;                   https://www.neilvandyke.org/quack/
        selectric-mode ;          https://github.com/rbanffy/selectric-mode
        sly ;                     https://github.com/joaotavora/sly
        smex ;                    http://github.com/nonsequitur/smex/
        spell-fu ;                https://codeberg.org/ideasman42/emacs-spell-fu
        stripes ;                 https://gitlab.com/stepnem/stripes-el
        tiny ;                    https://github.com/abo-abo/tiny
        transpose-frame ;         https://github.com/emacsorphanage/transpose-frame
        undo-fu-session ;         https://codeberg.org/ideasman42/emacs-undo-fu-session
        vundo ;                   https://github.com/casouri/vundo
        yasnippet ;               http://github.com/joaotavora/yasnippet
        ;; Themes
        color-theme-solarized)) ; https://github.com/sellout/emacs-color-theme-solarized/



;;;; Miscellaneous settings

(setq custom-file (expand-file-name "custom.el" user-emacs-directory)) ; Save to another file the customizations done via the Customize interface. This file is not loaded so it only works as a dump. https://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Customizations.html
(setq echo-keystrokes 0.001) ;                                           Echo prefix keys immediately.
(setq completions-format 'vertical) ;                                    Sort completions along columns. https://stackoverflow.com/questions/4719991/how-to-get-emacs-to-list-suggestions-like-ls-along-columns
(setq line-number-display-limit-width 2000000) ;                         Try harder not to lose count for the line number displayed in the mode line. It avoids having “??” as the line number. https://emacs.stackexchange.com/questions/3824/what-piece-of-code-in-emacs-makes-line-number-mode-print-as-line-number-i
(setq-default major-mode 'text-mode) ;                                   The main convenience with ‘text-mode’ is that it has the pairing of quotes set up the way I like. There’s no “fundamental-mode-hook” whatsoever that can be used to apply settings for ‘fundamental-mode’. See https://www.gnu.org/software/emacs/manual/html_node/emacs/Major-Modes.html
(setq initial-major-mode 'text-mode) ;                                   This is the initial buffer’s major mode; it’s used for any buffer with the same name as the initial one (for instance one opened after the initial one was killed).

;; When launched at system start-up, the Emacs daemon doesn’t pick up changes to the ‘PATH’ introduced by shell initialization scripts.
;; Adapted from https://github.com/Wilfred/ag.el/blob/master/docs/configuration.rst
;; See https://emacs.stackexchange.com/questions/64081/how-to-get-the-path-from-the-shell
(defun set-exec-path-from-shell-PATH ()
  "Set up Emacs’ ‘exec-path’ and ‘PATH’ to match the ‘PATH’
environment variable used by the user’s shell."
  (interactive)
  (let ((path-from-shell (replace-regexp-in-string "[ \t\n]*$" "" (shell-command-to-string "$SHELL --login -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq exec-path (split-string path-from-shell path-separator))))
(set-exec-path-from-shell-PATH)

;; Use UTF-8
;; 1 https://www.masteringemacs.org/article/working-coding-systems-unicode-emacs
;; 2 http://xahlee.info/emacs/emacs/emacs_n_unicode.html
;; 3 https://www.emacswiki.org/emacs/UnicodeEncoding
;; 4 https://stackoverflow.com/questions/20723229/how-to-reset-emacs-to-save-files-in-utf-8-unix-character-encoding
;(set-language-environment "UTF-8") ; REVIEW: When ‘use-default-font-for-symbols’ is nil, this setting makes Emacs use other fonts for some symbols, even if the main one covers them.
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(setq-default buffer-file-coding-system 'utf-8)
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)) ; Treat clipboard input as UTF-8 string first; compound text next, etc.

;; Fontsets
;; Tests:
;;   - ‘C-h h’ (‘view-hello-file’);
;;   - ‘M-x eww RET https://unicode.org/Public/emoji/latest/emoji-test.txt’;
;;   - the text files hosted at https://www.cl.cam.ac.uk/%7Emgk25/ucs/examples/
;;   - Emoji Zero Width Joiner (#x200D) sequence example: 👨‍👩‍👧‍👦 (#x1F468 + #x200D + #x1F469 + #x200D + #x1F467 + #x200D + #x1F466).
;; FIXME:
;;   - (Bug?) Unicode Variation Selector 15 (#xFE0E), which is supposed to request that a character be displayed as monochrome text, doesn’t work.
;;     For example, #x1F30D + #xFE0E, 🌍︎, should display as a single monochrome character (it works in Gedit and Gnome Terminal).
;; References
;;   1 https://www.gnu.org/software/emacs/manual/html_node/emacs/Fontsets.html
;;   2 https://www.gnu.org/software/emacs/manual/html_node/emacs/Modifying-Fontsets.html
;;   3 https://www.gnu.org/software/emacs/manual/html_node/elisp/Fontsets.html
;;   4 ‘M-x find-library RET fontset’
;;   5 ‘M-x find-library RET mule-conf’
;;   6 https://www.reddit.com/r/emacs/comments/9l3n3y/best_practice_for_configuring_fonts_in_emacs/
;;   7 http://emacs.1067599.n8.nabble.com/bug-21028-Performance-regression-in-revision-af1a69f4d17a482c359d98c00ef86fac835b5fac-Apr-2014-td363153.html
;;   8 https://idiocy.org/emacs-fonts-and-fontsets.html
;;   9 https://www.reddit.com/r/emacs/comments/ggd90c/color_emoji_in_emacs_27/
;;  10 https://emacs.stackexchange.com/questions/62049/override-the-default-font-for-emoji-characters/
;; Tables of characters
;;  11 https://unicode.org/charts/
;;  12 https://unicode-table.com
;;  13 https://en.wikipedia.org/wiki/Unicode_block#List_of_blocks
;;  14 https://unicode.org/emoji/charts/full-emoji-list.html
;;  15 https://unicode.org/Public/emoji/latest/emoji-sequences.txt
;;  16 https://en.wikipedia.org/wiki/Emoji#Unicode_blocks

(setq use-default-font-for-symbols nil) ; Make Emacs honor the fontsets.

(defvar emoji-font "Twemoji"
  "Font to use for displaying the characters listed in ‘emoji-chars’.")

(defvar emoji-fallback-font "Symbola"
  "Fallback font for the characters listed in ‘emoji-chars’,
in case the ‘emoji-font’ lacks some.")

(defvar emoji-chars
  ;; This list is based on Wikipedia’s table of emojis as of Unicode 13.0 [16]. See also [15].
  '(  #x200D ;             Zero Width Joiner
     (#x231A . #x231B) ;   ⌚ ⌛
      #x2328 ;             ⌨
     (#x23F0 . #x23F3) ;   ⏰ ⏱ ⏲ ⏳
     (#x2600 . #x2604) ;   ☀ ☁ ☂ ☃ ☄
      #x260E ;             ☎
     (#x2614 . #x2615) ;   ☔ ☕
      #x2618 ;             ☘
      #x261D ;             ☝
    ; #x2620 ;             ☠
    ;(#x2622 . #x2623) ;   ☢ ☣
     (#x2639 . #x263A) ;   ☹ ☺
    ; #x2660 ;             ♠
    ; #x2663 ;             ♣
    ;(#x2665 . #x2666) ;   ♥ ♦
      #x267B ;             ♻
     (#x2692 . #x2694) ;   ⚒ ⚓ ⚔
     (#x2696 . #x2697) ;   ⚖ ⚗
      #x2699 ;             ⚙
     (#x26A0 . #x26A1) ;   ⚠ ⚡
     (#x26B0 . #x26B1) ;   ⚰ ⚱
     (#x26BD . #x26BE) ;   ⚽ ⚾
     (#x26C4 . #x26C5) ;   ⛄ ⛅
      #x26C8 ;             ⛈
      #x26CF ;             ⛏
      #x26D1 ;             ⛑
     (#x26D3 . #x26D4) ;   ⛓ ⛔
     (#x26E9 . #x26EA) ;   ⛩ ⛪
     (#x26F0 . #x26F5) ;   ⛰ ⛱ ⛲ ⛳ ⛴ ⛵
     (#x26F7 . #x26FA) ;   ⛷ ⛸ ⛹ ⛺
      #x26FD ;             ⛽
     (#x2708 . #x270D) ;   ✈ ✉ ✊ ✋ ✌ ✍
      #x270F ;             ✏
      #x2712 ;             ✒
      #x2744 ;             ❄
     (#x2763 . #x2764) ;   ❣ ❤
      #x2B50 ;             ⭐
      #xFE0F ;             Variation Selector 16
     #x1F004 ;             🀄
    ;#x1F0CF ;             🃏
    (#x1F300 . #x1F5FF) ;  Miscellaneous Symbols and Pictographs block (🌀 to 🗿)
    (#x1F600 . #x1F64F) ;  Emoticons block (😀 to 🙏)
    (#x1F680 . #x1F6FF) ;  Transport and Map Symbols block (🚀 to 🛼)
    (#x1F7E0 . #x1F7EB) ;  🟠 🟡 🟢 🟣 🟤 🟥 🟦 🟧 🟨 🟩 🟪 🟫
    (#x1F90C . #x1F9FF) ;  Supplemental Symbols and Pictographs block
    (#x1F900 . #x1F9FF) ;  Supplemental Symbols and Pictographs block (🤀 to 🧿)
    (#x1FA70 . #x1FAFF) ;  Supplemental Symbols and Pictographs block (🩰 to 🫖)
    (#x1F3FB . #x1F3FF)) ; Emoji Modifier Fitzpatrick
  "List of characters to display as color emojis.
Each element of this list should be a valid target for ‘set-fontset-font’.")

(defun set-up-emoji-fonts ()
  "Use the ‘emoji-font’ to display the ‘emoji-chars’,
with the ‘emoji-fallback-font’ as a fallback."
  ;; REVIEW: If the ‘emoji-font’ is missing a character that both the main font and the ‘fallback-font’ have, which one is used?
  ;; Note: These settings can have a significant effect on Emacs’ performance when a visible portion of the current buffer contains tens of symbols.
  ;;   As far as I understand a good rule of thumb is: the narrower the target of a fontset, and the smaller the pool of fonts, the better. Also, whether the fontsets use ‘'prepend’ or ‘'append’ can influence performance.
  ;;   See [7], especially at p. 4, “futile search through these fonts” and “> How can searching two fonts take so much time?”.
  (let ((fs "fontset-default")) ; If you want to modify all the fontsets replace this line with ‘(dolist (fs (fontset-list))’.
    (dolist (target emoji-chars)
      (set-fontset-font fs target emoji-font)
      (set-fontset-font fs target emoji-fallback-font nil 'append))))
(if (daemonp)
    (add-hook 'server-after-make-frame-hook #'set-up-emoji-fonts)
  (set-up-emoji-fonts))

;; Try to always get the Emacs frame in the foreground and focused when I open it
;; See ‘(info "(elisp) Input Focus")’
;;     https://emacs.stackexchange.com/questions/34737/start-emacsclient-with-focus-from-command-line
;;     https://emacs.stackexchange.com/questions/9647/open-new-emacs-frame-that-steals-focus-from-other-apps
;; See also the function ‘x-focus-frame’; https://www.gnu.org/software/auctex/manual/auctex/I_002fO-Correlation.html
;; REVIEW: Is this still useful?
(add-hook 'server-switch-hook #'raise-frame)
(defun raise-this-frame ()
  "Raise the currently selected frame and give it input focus."
  (select-frame-set-input-focus (selected-frame)))
(add-hook 'server-after-make-frame-hook #'raise-this-frame)

;; Set up hooks to be run on focus in and focus out

;; Mutter sends spurious focus events, including sending focus out events when Emacs never really was unfocused.
;; The timer filters out these false focus out events, while checking the ‘last-focus-state’ allows ignoring repeated focus events of the same kind.
;; REVIEW: False focus out events are still received sometimes when using the menu bar.
;; See ‘(info "(elisp) Input Focus")’
;;     ‘(info "(elisp) Timers")’
;;     ‘C-h v after-focus-change-function’
;;     https://www.reddit.com/r/emacs/comments/kxsgtn/ignore_spurious_focus_events_for/
;;     https://lists.gnu.org/archive/html/help-gnu-emacs/2021-01/msg00328.html
;;     https://emacs.stackexchange.com/questions/62783/ignore-spurious-focus-events-for-after-focus-change-function

(defvar focus-events-timer nil)

(defvar last-focus-state (frame-focus-state)
  "Focus state of the Emacs frame at the last invocation of
‘handle-change-of-focus’.
The meaning of its value is explaned in the doc string of
‘frame-focus-state’.")

(defvar my-focus-in-hook nil
  "Normal hook run when all frames lose input focus.")

(defvar my-focus-out-hook nil
  "Normal hook run when a frame gains focus.")

(defun handle-change-of-focus ()
  "Determine the focus state of the frame, then run the
appropriate focus hook."
  (unless (eq last-focus-state (frame-focus-state))
    (if (frame-focus-state)
        (run-hooks 'my-focus-in-hook)
      (run-hooks 'my-focus-out-hook)))
  (setq focus-events-timer nil)
  (setq last-focus-state (frame-focus-state)))

(defun run-with-timer-handle-change-of-focus ()
  (unless (timerp focus-events-timer)
    (setq focus-events-timer
          (run-at-time "0.015 sec" nil ; Delay chosen by trial and error. Test it if you change it.
                       #'handle-change-of-focus))))

(add-function :after after-focus-change-function #'run-with-timer-handle-change-of-focus)

;; Unlock some disabled commands
(dolist (command '(upcase-region
                   downcase-region
                   narrow-to-region
                   dired-find-alternate-file
                   set-goal-column))
  (put command 'disabled nil))

;; Mark some risky variable and value pairs as safe
(setq safe-local-variable-values '((electric-quote-mode . nil)))



;;;; Key bindings (environment)

(customize-set-variable 'flyspell-use-meta-tab nil) ;                   Free up the ‘M-TAB’ key combination in ‘flyspell-mode’. The definition of this variable uses ‘:set’ and ‘:initialize’, so in order to trigger them ‘customize-set-variable’ has to be used instead of ‘setq’. See “What to use to set customizable variables” in my ‘emacsguide’.
(global-unset-key "\C-z") ;                                             Unbind ‘C-z’ (from ‘suspend-frame’).
(add-hook 'outline-minor-mode-hook
          (lambda () (local-set-key "\C-z" outline-mode-prefix-map))) ; Change the prefix for outline-minor-mode commands. https://www.emacswiki.org/emacs/OutlineMinorMode

;; REVIEW (bug?): The fringe indicators are all piled up in a corner of the screenshot.
;; https://www.reddit.com/r/emacs/comments/idz35e/emacs_27_can_take_svg_screenshots_of_itself/g2c2c6y/
(defun screenshot-svg ()
  "Save a screenshot of the current frame as an SVG image.
Saves to a temp file and puts the file name in the kill ring."
  (interactive)
  (let* ((prefix (format-time-string "Emacs_%F_%H.%M.%S_"))
         (filename (make-temp-file prefix nil ".svg"))
         (data (x-export-frames nil 'svg)))
    (with-temp-file filename
      (insert data))
    (kill-new filename)
    (message filename)))
(global-set-key (kbd "C-s-s") #'screenshot-svg)

;; https://unix.stackexchange.com/questions/44932/open-terminal-from-emacs
(defun ag-open-terminal-here ()
  "Open a terminal in the current directory."
  (interactive)
  (call-process-shell-command my-terminal nil 0))
(global-set-key (kbd "C-c t") #'ag-open-terminal-here)

(defun ag-open-file-manager-here ()
  "Open a file manager in the current directory."
  (interactive)
  (let ((command (concat my-file-manager " " (shell-quote-argument (expand-file-name default-directory)))))
    (call-process-shell-command command nil 0)))
(global-set-key (kbd "C-c n") #'ag-open-file-manager-here)

;; Resize the frame

(defun frame-size-fullwidth ()
  "Maximize the frame horizontally."
  (interactive)
  (if window-system
      (let ((fullscreen (frame-parameter nil 'fullscreen)))
        (cond
         ((memq fullscreen '(maximized fullscreen fullboth))
          (set-frame-parameter nil 'fullscreen 'fullheight))
         ((eq fullscreen 'fullheight)
          (set-frame-parameter nil 'fullscreen 'maximized))
         ((eq fullscreen 'fullwidth)
          (set-frame-parameter nil 'fullscreen nil))
         (t
          (set-frame-parameter nil 'fullscreen 'fullwidth))))
    (user-error "Can’t resize a terminal frame")))
(global-set-key (kbd "<f2> w") #'frame-size-fullwidth)

(defun frame-size-fullheight ()
  "Maximize the frame vertically."
  (interactive)
  (if window-system
      (let ((fullscreen (frame-parameter nil 'fullscreen)))
        (cond
         ((memq fullscreen '(maximized fullscreen fullboth))
          (set-frame-parameter nil 'fullscreen 'fullwidth))
         ((eq fullscreen 'fullwidth)
          (set-frame-parameter nil 'fullscreen 'maximized))
         ((eq fullscreen 'fullheight)
          (set-frame-parameter nil 'fullscreen nil))
         (t
          (set-frame-parameter nil 'fullscreen 'fullheight))))
    (user-error "Can’t resize a terminal frame")))
(global-set-key (kbd "<f2> h") #'frame-size-fullheight)

(defun frame-resize (h w)
  "Make the frame H lines high and W columns wide."
  (interactive "nHeight (lines): \nnWidth (columns): ")
  (if window-system
      ;; REVIEW: I don’t know why, but a single call to ‘modify-frame-parameters’, targeting ‘fullscreen’, ‘height’ and ‘width’, doesn’t work.
      (progn
        (set-frame-parameter (selected-frame) 'fullscreen nil)
        (set-frame-size (selected-frame) w h))
    (user-error "Can’t resize a terminal frame")))
(global-set-key (kbd "<f2> s") (lambda () (interactive) (frame-resize 24 80)))
(global-set-key (kbd "<f2> m") (lambda () (interactive) (frame-resize 40 80)))



;;;; Files, directories, buffers and windows

(setq delete-by-moving-to-trash t) ;                   Move deleted files to the trash can instead of erasing them for good.
(setq large-file-warning-threshold (* 30 1024 1024)) ; Don’t ask for confirmation before opening files under 30 MB in size.
(setq confirm-nonexistent-file-or-buffer nil) ;        Don’t ask for confirmation before creating a new buffer or file.
(setq compilation-ask-about-save nil) ;                Don’t ask about saving when running ‘compile’ or ‘recompile’, just save and compile. https://superuser.com/questions/799859/suppress-prompt-for-save-file-in-emacs-with-recompile
(global-set-key (kbd "C-x x s") #'set-visited-file-name)

;;; Auto-save

;; Auto save on loss of focus
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Auto-Save-Control.html
(add-hook 'my-focus-out-hook #'do-auto-save)

;; Delete the auto-save file when buffer is killed
;; https://groups.google.com/forum/#!topic/gnu.emacs.help/tWClcJFC3rg
;; https://www.reddit.com/r/emacs/comments/47xoi1/remove_autosave_files_when_i_kill_a_buffer_with/
(defun delete-auto-save-file-if-necessary-forcefully ()
  (delete-auto-save-file-if-necessary 'forcefully))
(add-hook 'kill-buffer-hook #'delete-auto-save-file-if-necessary-forcefully) ; REVIEW: Is it safe?

;;; Backups

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Backup.html
;; https://stackoverflow.com/questions/151945/how-do-i-control-how-emacs-makes-backup-files

(setq backup-by-copying t) ;    “There are a number of arcane details associated with how Emacs might create your backup files. […] In general, the safest but slowest bet is to always make backups by copying.” https://stackoverflow.com/questions/151945/how-do-i-control-how-emacs-makes-backup-files
(setq vc-make-backup-files t) ; Back up files that are under version control.
(setq version-control t) ;      Use version numbers for backups.
(setq delete-old-versions t) ;  Don’t ask whether to delete old backups.
(setq kept-new-versions 6) ;    Number of newest versions to keep.
(setq kept-old-versions 2) ;    Number of oldest versions to keep.

;; Put all backup files (‘file.ext~’) in one directory
;; https://stackoverflow.com/questions/151945/how-do-i-control-how-emacs-makes-backup-files
;; https://www.emacswiki.org/emacs/BackupDirectory
(setq backup-directory-alist '(("." . "~/.backups/")))

;;; Find file at point

(defun my-find-file-at-point (&optional arg)
  "Prompt for a file to visit, guessing a default from the text
around the point, or, if the region is active, using the text in
the region as default.
Use ffap’s facilities when there’s no active region and no prefix
argument, otherwise use Ido’s facilities. ffap is smarter, for
example it can find a LaTeX package from a ‘\\usepackage{}’
command, but its handling of the region is poorer. Another
difference between Ido and ffap is that ‘ido-find-file’ uses the
full ‘ffap-string-at-point’ as default, while ‘find-file-at-point’
trims it after the last directory separator."
  ;; See https://tex.stackexchange.com/questions/54756/open-included-files-in-emacsauctex/54943#54943
  ;; see also the function ‘dired-x-find-file’.
  (interactive "P")
  (if (or arg (use-region-p))
      (let ((ido-use-filename-at-point t)
            (ido-use-url-at-point t))
        (call-interactively #'ido-find-file))
    (call-interactively #'find-file-at-point)))
(global-set-key (kbd "C-c f") #'my-find-file-at-point)

;;; Dired

(setq dired-listing-switches ;              Switches passed to ‘ls’ for Dired. NOTE: they must include the ‘-l’ option. See ‘man ls’, ‘C-h v dired-listing-switches’
      (combine-and-quote-strings
       '("-l" "--all" "--dired" ;           For information about the flag ‘--dired’, see ‘(info "(coreutils) What information is listed")’.
         "-v" "--group-directories-first" ; Sorting
        ;"--indicator-style=slash" ;        Append a slash to directory names. REVIEW (bug?): It breaks ‘dired-goto-file’ (‘j’) for directories.
        ;"-g" "--no-group" ;                Hide owner and group.
         "--human-readable" ;               Print file sizes in easily readable format.
         "--time-style=long-iso")))
(setq dired-recursive-copies 'always) ;     Don’t ask about copying directories recursively. http://ergoemacs.org/emacs/emacs_dired_tips.html

;; Visit directories without opening a new dired buffer

;; This behaves like ‘dired-find-alternate-file’ (bound to ‘a’ by default) but it goes in the opposite direction.
;; https://stackoverflow.com/questions/9992041/dired-backward-forward-function
(defun my-dired-up-dir ()
  "Go to parent directory without opening a new dired buffer."
  (interactive)
  (let ((current-dir (dired-current-directory)))
    (find-alternate-file "..")
    (dired-goto-file current-dir)))

(with-eval-after-load 'dired
  (define-key dired-mode-map (kbd "M-<up>")   #'my-dired-up-dir)
  (define-key dired-mode-map (kbd "M-<down>") #'dired-find-alternate-file)
  (define-key dired-mode-map (kbd "RET")      #'dired-find-alternate-file)) ; RET was originally bound to ‘dired-find-file’, which is also bound to ‘e’ and ‘f’.

;; Dired Extra

;; Code adapted from
;;   1 https://www.gnu.org/software/emacs/manual/html_node/dired-x/Installation.html
;;   2 http://www.xsteve.at/prg/emacs/power-user-tips.html
;; https://stackoverflow.com/questions/1110118/in-emacs-dired-how-to-find-visit-multiple-files/3715358#3715358

(defun set-up-dired-global-settings ()
  ;; Set dired-x global variables here. For example:
  ;; (setq dired-guess-shell-gnutar "gtar")
  ;; (setq dired-x-hands-off-my-keys nil) ;                         See https://www.gnu.org/software/emacs/manual/html_node/dired-x/Optional-Installation-File-At-Point.html
  (require 'dired-x)
  (setq dired-omit-files (concat dired-omit-files "\\|^\\..+$"))) ; Hide dot-files in omit mode. (‘C-u s’ and removing the ‘--all’ flag also does the job.) https://www.emacswiki.org/emacs/DiredOmitMode
(add-hook 'dired-load-hook #'set-up-dired-global-settings)

(defun set-up-dired-buffer-local-settings ()
  ;; Set dired-x buffer-local variables here. For example:
  ;; (dired-omit-mode)
  (auto-revert-mode) ;                                              Refresh automatically. https://superuser.com/questions/566393/how-to-configure-dired-to-update-instantly-when-files-folders-change
  (setq-local auto-revert-verbose nil)) ;                           Suppress messages about Dired buffers being reverted.
(add-hook 'dired-mode-hook #'set-up-dired-buffer-local-settings)

;; Make ‘dired-jump’ and ‘dired-jump-other-window’ work before dired and dired-x have been loaded
;; https://www.gnu.org/software/emacs/manual/html_node/dired-x/Optional-Installation-Dired-Jump.html

(autoload 'dired-jump "dired-x"
  "Jump to Dired buffer corresponding to current buffer." t)

(autoload 'dired-jump-other-window "dired-x"
  "Like \\[dired-jump] (dired-jump) but in other window." t)

(define-key global-map "\C-x\C-j" 'dired-jump)
(define-key global-map "\C-x4\C-j" 'dired-jump-other-window)

;;; Openwith

;; https://www.emacswiki.org/emacs/OpenWith
;; https://emacs.stackexchange.com/questions/3105/how-to-use-an-external-program-as-the-default-way-to-open-pdfs-from-emacs

;; Unlike the version on MELPA [1], this one, downloaded from [2], doesn’t show any warning about existing subprocesses on exit.
;; REVIEW: Check if the version on MELPA is updated (last time I saw it was v. 20120531.2136) or if another one comes out.
;; 1 https://melpa.org/#/openwith
;; 2 https://github.com/garberw/openwith/blob/d6347a6de6733e56d39f89eba522c76109728049/openwith.el
(add-to-list 'load-path (expand-file-name "openwith" my-lib-dir))
(require 'openwith)

(setq openwith-associations
      (list
       ;; Audio
       (list (openwith-make-extension-regexp
              '("aac"  "flac" "m4a"  "mp3"  "wav"))
             "mpv" '(file))
       ;; Video
       (list (openwith-make-extension-regexp
              '("avi"  "flv"  "mkv"  "mov"  "mp4"  "mpeg" "mpg"  "ogg"  "ogm" "webm" "wmv"))
             "mpv" '(file))
       ;; Images
       (list (openwith-make-extension-regexp
              '("bmp"  "gif"  "jpeg" "jpg"  "nef"  "pbm"  "pgm"  "png"  "pnm"  "ppm" "tif"  "xbm"))
             "gthumb" '(file))
       ;; Documents
       (list (openwith-make-extension-regexp
              '("cbr" "cbz" "djvu" "pdf"  "ps"   "ps.gz"))
             "zathura" '(file))
       ;; Ebooks
       (list (openwith-make-extension-regexp
              '("epub" "mobi")) ; Note: Foliate can’t open ‘.chm’ files.
             "gjs /usr/bin/com.github.johnfactotum.Foliate" '(file))
       ;; Office documents
       (list (openwith-make-extension-regexp
              '("ods"  "xls"  "xlsx"
                "odt"  "doc"  "docx"
                "odp"  "ppt"  "pptx"
                "odg"))
             "libreoffice" '(file))))

;; Prevent ‘openwith-mode’ from interfering with attachments when writing a message in Gnus.
;; https://www.emacswiki.org/emacs/OpenWith
;; https://www.metalevel.at/misc/openwith.el
;; https://stackoverflow.com/questions/22942802/disable-openwith-during-execution-of-a-mu4e-function
(with-eval-after-load 'mm-util
  (add-to-list 'mm-inhibit-file-name-handlers 'openwith-file-handler))

(openwith-mode)

(defvar openwith-regexps-combined (mapconcat #'car openwith-associations "\\|")
  "Files whose name matches this regexp are handled by Openwith.")

;; Adapted from https://emacs.stackexchange.com/questions/17095/how-supress-dired-confirmation-of-large-file-for-specific-extensions
(define-advice abort-if-file-too-large
    (:around (orig-fn size op-type filename &optional offer-raw) unless-openwith-handles-it)
  "Suppress the warning if FILENAME isn’t supposed to open in a
text editing buffer."
  (let ((safe-filenames
         (mapconcat #'identity (list openwith-regexps-combined
                                     (regexp-opt '(".7z" ".rar" ".tar" ".tar.gz" ".tgz" ".tar.xz" ".zip")))
                    "\\|")))
    (unless (string-match-p safe-filenames filename)
      (funcall orig-fn size op-type filename offer-raw))))

;;; Compile and run at once

;; Call ‘c&r-recompile-and-run’ to compile the current buffer’s file and run the compiled program (or open the compiled file, e.g. a plot made with gnuplot).
;; ‘c&r-recompile-and-run’ compiles the source code and adds ‘c&r-post-compilation’ to the hook ‘compilation-finish-functions’. When ‘c&r-post-compilation’ is called from the hook it runs the compiled file. These functions use the shell command stored in the variable ‘compile-command’ to compile the source code and the shell command stored in ‘c&r-run-command’ to run the compiled file. Set these variables each with a separate function, and then set ‘c&r-set-compile-command-function’ and ‘c&r-set-run-command-function’ to those functions.

(autoload 'recompile "compile" nil t)

(defvar-local c&r-compiled-file-extension nil
  "Extension used for the compiled file.")

(defvar-local c&r-run-command nil
  "Shell command for running the compiled file.")

(defvar-local c&r-run-command-maybe-use-terminal nil
  "Shell command run after a successful compilation.
Its value is set by the function ‘c&r-recompile-and-run’.
When ‘c&r-use-terminal’ is t (the default) it includes the
command that launches the terminal in addition to
‘c&r-run-command’, otherwise it’s just set to the value of
‘c&r-run-command’.")

(defvar-local c&r-set-compile-command-function
  (lambda () (user-error "‘c&r-set-compile-command-function’ is not set."))
  "Function that sets the ‘compile-command’.")

(defvar-local c&r-set-run-command-function
  (lambda () (user-error "‘c&r-set-run-command-function’ is not set."))
  "Function that sets the ‘c&r-run-command’.")

(defvar-local c&r-use-terminal t
  "Whether to run ‘c&r-run-command’ from a terminal.")

(defun c&r-source-file-name ()
  "Return the source code file name as a string ready to be used
in a shell command."
  (if buffer-file-name
      (prin1-to-string ; escapes the quotes added by ‘shell-quote-argument’ so that they are kept in the ‘compile-command’ when it is put together.
       (shell-quote-argument buffer-file-name))
    (user-error "This buffer is not visiting a file"))) ; Cf. https://books.google.it/books?id=FjzWEfXNMfwC&pg=PA27&lpg=PA27&dq=buffer+not+visiting+file+error+emacs

(defun c&r-compiled-file-name ()
  "Return the compiled file name as a string ready to be used in
a shell command."
  (if buffer-file-name
      (prin1-to-string
       (shell-quote-argument (concat
                              (file-name-sans-extension buffer-file-name)
                              "." c&r-compiled-file-extension)))
    (user-error "This buffer is not visiting a file")))

;; Function to be called after a successful compilation
;; See “Success or failure of compile” at https://www.emacswiki.org/emacs/CompileCommand#toc2
;;     https://emacs.stackexchange.com/questions/22372/was-compilation-successful-or-not
;;     https://emacs.stackexchange.com/questions/62/hide-compilation-window
(defun c&r-post-compilation (_buffer msg)
  "Function invoked to run the compiled file."
  (when (string-match-p "^finished" msg)
    (quit-window t (get-buffer-window "*compilation*"))
    (process-file-shell-command c&r-run-command-maybe-use-terminal nil 0))
  (remove-hook 'compilation-finish-functions #'c&r-post-compilation)) ; This function must not be called by other compilation commands. From https://emacs.stackexchange.com/questions/3323/is-there-any-way-to-run-a-hook-function-only-once

(defun c&r-recompile-and-run ()
  "Call ‘recompile’ on the current buffer. If it is successful,
run the compiled file using ‘c&r-run-command’.
If ‘c&r-use-terminal’ is non-nil (the default), open a terminal
and launch the compiled file from there, running ‘c&r-run-command’
in ‘my-shell’."
  (interactive)
  (funcall c&r-set-compile-command-function)
  (funcall c&r-set-run-command-function)
  (unless (bound-and-true-p c&r-run-command)
    (setq c&r-run-command "echo Emacs: function ‘c&r-recompile-and-run’ error: variable ‘c&r-run-command’ is void or nil"))
  (setq c&r-run-command-maybe-use-terminal
        (if c&r-use-terminal
            (concat
             ;; Add the command to launch the compiled file to the shell’s history
             ;; NOTE: This works with Gnome Terminal and Bash. It might break with other terminals or shells.
             my-shell " -ci 'history -s \"" c&r-run-command "\"'; "
             ;; Open a terminal and run the compiled file
             my-terminal " -- " my-shell " -c '" c&r-run-command "; exec " my-shell "'")
          c&r-run-command))
  (add-hook 'compilation-finish-functions #'c&r-post-compilation)
  (recompile)) ; ‘recompile’ basically invokes ‘compile’ without prompting for a different compile command. https://stackoverflow.com/questions/443302/emacs-how-to-compile-run-make-without-pressing-enter-for-the-compile-command
(global-set-key [f8] #'c&r-recompile-and-run)

;;; Buffer and window management

;; https://lists.gnu.org/archive/html/help-gnu-emacs/2007-05/msg00975.html
(define-minor-mode pinned-buffer-mode
  "Make the current window always display its current buffer."
  :lighter " 📌" ; REVIEW: If Unicode Variation Selector 15 (#xFE0E) becomes supported, use it to force the pin glyph to be monochrome instead of an emoji.
  (set-window-dedicated-p (selected-window) pinned-buffer-mode))
(global-set-key (kbd "C-c p") #'pinned-buffer-mode)

;; https://emacs.stackexchange.com/questions/63356/how-to-make-the-current-window-display-the-buffer-it-was-displaying-before-the-c
(defun previous-buffer-cycle ()
  "Switch to the last live buffer that was displayed in the
current window."
  (interactive)
  (pop-to-buffer-same-window
   (if (eq (current-buffer) (caar (window-prev-buffers)))
       (caadr (window-prev-buffers))
     (caar (window-prev-buffers))))) ; IOW, ‘(pop-to-buffer-same-window (caar (assq-delete-all (current-buffer) (window-prev-buffers))))’.
(global-set-key (kbd "C-c c") #'previous-buffer-cycle)

;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Switching-Buffers.html
;; https://emacs.stackexchange.com/questions/34250/can-other-buffer-in-split-windows-select-a-visible-buffer
(defun my-pop-to-last-buffer ()
  "Switch to the previously selected buffer in its window, or in
some other existing one if no window is displaying it."
  (interactive)
  (select-window (selected-window)) ; Move the current buffer to the front of the buffer list, which is where ‘other-buffer’ looks for the most recently selected buffer. This allows switching back to a buffer that was reached using commands (like ‘{previous,next}-buffer’) that change the current buffer but not the buffer list. It’s what ‘switch-to-buffer’ does (see also the definition of ‘select_window’ in ‘window.c’).
  (pop-to-buffer (other-buffer (current-buffer) 'even-if-visible) '((display-buffer-reuse-window
                                                                     display-buffer-use-some-window))))
(global-set-key (kbd "C-c x") #'my-pop-to-last-buffer)

;; https://superuser.com/questions/132225/how-to-get-back-to-an-active-minibuffer-prompt-in-emacs-without-the-mouse
(defun switch-to-minibuffer-window ()
  "Select the minibuffer window if it’s active."
  (interactive)
  (if (active-minibuffer-window)
      (progn
        (select-frame-set-input-focus (window-frame (active-minibuffer-window)))
        (select-window (active-minibuffer-window)))
    (user-error "Minibuffer is inactive"))) ; Taken from ‘windmove-do-window-select’.
(global-set-key (kbd "C-c m") #'switch-to-minibuffer-window)

(defun switch-to-scratch-buffer ()
  "Switch to the ‘*scratch*’ buffer in its window or in the
current one if no window is displaying it."
  (interactive)
  (pop-to-buffer "*scratch*" '((display-buffer-reuse-window
                                display-buffer-same-window))))
(global-set-key (kbd "C-c b") #'switch-to-scratch-buffer)

(defun visit-todo-file (&optional arg)
  "Visit the todo file. If called with argument,
offer to trash it and kill its buffer instead."
  (interactive "P")
  (let* ((todo-file "~/todo.txt")
         (todo-buffer (get-file-buffer todo-file))
         (prompt (when (or (file-exists-p todo-file) todo-buffer)
                   (if (file-exists-p todo-file)
                       (concat "Trash the todo file" (if todo-buffer " and kill its buffer without saving? " "? "))
                     (format "Kill buffer %s without saving? " (buffer-name todo-buffer))))))
    (if arg
        (if (or (file-exists-p todo-file) todo-buffer)
            (if (y-or-n-p prompt)
                (progn
                  (when (file-exists-p todo-file)
                    (delete-file todo-file 'trash))
                  (when todo-buffer
                    (with-current-buffer todo-buffer
                      (set-buffer-modified-p nil) ; To prevent the request for confirmation, clear the modified flag before calling ‘kill-buffer’ (from ‘(info "(elisp) Killing Buffers")’).
                      (kill-buffer todo-buffer))))
              (message "Cancelled"))
          (user-error "Neither the todo file nor a buffer visiting it exist"))
      (let ((display-buffer-overriding-action '(display-buffer-reuse-window)))
        (find-file todo-file)))))
(global-set-key (kbd "C-c v") #'visit-todo-file)

;; Directional window display, selection and deletion

;; Key bindings are set up so that
;;   - ‘C-c <direction>’ moves to an adjacent window;
;;   - ‘C-u C-c <direction>’ swaps the current window and an adjacent one;
;;   - ‘C-c M-<direction>’ kills the buffer displayed in an adjacent window;
;;   - ‘C-c S-<direction>’ deletes an adjacent window;
;;   - ‘C-u C-c S-<direction>’ deletes an adjacent window and kills the buffer in that window;
;;   - ‘C-0 C-c S-<direction>’ deletes the current window and moves to an adjacent one;
;;   - ‘C-M-S-<arrow key>’ before a command that displays a new buffer places that buffer in the window at the direction of the arrow key, creating it if necessary;
;;   - ‘C-M-S-0’ before a command that displays a new buffer places that buffer in the current window;
;;   - ‘C-M-S-t’ before a command that displays a new buffer places that buffer in a new tab.
;; The direction can be specified with ‘w’, ‘s’, ‘a’ and ‘d’ or with the arrow keys.
;; See https://emacs.stackexchange.com/questions/46259/force-new-window-to-open-to-the-left-or-right

;; Generate ‘my-move-or-swap-{up,down,left,right}’ functions
(mapc (lambda (direction)
        (let ((function-name-as-string (concat "my-move-or-swap-" direction))
              (windmove-swap-states-direction (read (concat "windmove-swap-states-" direction)))
              (windmove-direction (read (concat "windmove-" direction)))
              (docstring-variable (cond
                                   ((equal direction "up")
                                    "above")
                                   ((equal direction "down")
                                    "below")
                                   ((member direction '("left" "right"))
                                    (format "to the %s of" direction)))))
          (eval
           `(progn
              (defun ,(read function-name-as-string) (&optional arg)
                (interactive "P")
                (if arg
                    (,windmove-swap-states-direction)
                  (,windmove-direction)))
              ;; REVIEW: If these functions are advised the information about the advices overwrites these doc strings, or the other way around depending on which one comes last, the ‘put’ form or the advice. Delaying the definition of the function using ‘after-init-hook’ didn’t work, I presume because the form used to apply the advice didn’t see any function to advise.
              (put (intern function-name-as-string) 'function-documentation
                   (format "Select the window %s the current one, or swap it
with the current window if ARG is non-nil." docstring-variable))))))
      '("up" "down" "left" "right"))

;; Generate ‘my-kill-buffer-{above,below,left,right}’ functions
(mapc (lambda (direction)
        (let ((function-name-as-string (concat "my-kill-buffer-" direction))
              (docstring-variable (if (member direction '("left" "right"))
                                      (format "at the %s of" direction)
                                    direction)))
          (eval
           `(progn
              (defun ,(read function-name-as-string) ()
                (interactive)
                (let ((window (window-in-direction (intern ,direction))))
                  (if window
                      (kill-buffer (window-buffer window))
                    (user-error "No window %s from selected window" ,direction))))
              ;; REVIEW: If these functions are advised the information about the advices overwrites these doc strings, or the other way around depending on which one comes last, the ‘put’ form or the advice. Delaying the definition of the function using ‘after-init-hook’ didn’t work, I presume because the form used to apply the advice didn’t see any function to advise.
              (put (intern function-name-as-string) 'function-documentation
                   (format "Kill the buffer displayed %s the current window." docstring-variable))))))
      '("above" "below" "left" "right"))

(windmove-display-default-keybindings '(control meta shift))
(global-set-key (kbd "C-M-=") #'windmove-display-same-window) ; ‘C-M-=’ is how Emacs reads the default key ‘C-M-S-0’ with my keyboard layout.

(global-set-key (kbd "C-c w")    #'my-move-or-swap-up)
(global-set-key (kbd "C-c <up>") #'my-move-or-swap-up)
(global-set-key (kbd "C-c s")      #'my-move-or-swap-down)
(global-set-key (kbd "C-c <down>") #'my-move-or-swap-down)
(global-set-key (kbd "C-c a")      #'my-move-or-swap-left)
(global-set-key (kbd "C-c <left>") #'my-move-or-swap-left)
(global-set-key (kbd "C-c d")       #'my-move-or-swap-right)
(global-set-key (kbd "C-c <right>") #'my-move-or-swap-right)

(global-set-key (kbd "C-c W")      #'windmove-delete-up)
(global-set-key (kbd "C-c S-<up>") #'windmove-delete-up)
(global-set-key (kbd "C-c S")        #'windmove-delete-down)
(global-set-key (kbd "C-c S-<down>") #'windmove-delete-down)
(global-set-key (kbd "C-c A")        #'windmove-delete-left)
(global-set-key (kbd "C-c S-<left>") #'windmove-delete-left)
(global-set-key (kbd "C-c D")         #'windmove-delete-right)
(global-set-key (kbd "C-c S-<right>") #'windmove-delete-right)

(global-set-key (kbd "C-c M-w")    #'my-kill-buffer-above)
(global-set-key (kbd "C-c M-<up>") #'my-kill-buffer-above)
(global-set-key (kbd "C-c M-s")      #'my-kill-buffer-below)
(global-set-key (kbd "C-c M-<down>") #'my-kill-buffer-below)
(global-set-key (kbd "C-c M-a")      #'my-kill-buffer-left)
(global-set-key (kbd "C-c M-<left>") #'my-kill-buffer-left)
(global-set-key (kbd "C-c M-d")       #'my-kill-buffer-right)
(global-set-key (kbd "C-c M-<right>") #'my-kill-buffer-right)

;; Undo and redo changes to the arrangement of windows
;; https://www.emacswiki.org/emacs/WinnerMode

(setq winner-dont-bind-my-keys t) ; Prevent Winner mode from setting key bindings. https://emacs.stackexchange.com/questions/38768/how-can-i-get-winner-mode-to-not-modify-my-keybindings

(winner-mode)

(define-key winner-mode-map (kbd "<f2> 1") #'winner-undo)
(define-key winner-mode-map (kbd "<f2> 3") #'winner-redo)

;;; Buffers and files to ignore

;; Ignore some extensions
;; Files with these extensions are excluded from the completion list and greyed out in Dired buffers.
;; there might be other settings of this variable in the language-specific set-ups.
(setq completion-ignored-extensions
      (append '(;; Documents
                ".djvu"   ".pdf"    ".ps"
                ".epub"   ".mobi"
                ".odt"    ".doc"    ".docx"
                ".odp"    ".ppt"    ".pptx"
                ".ods"    ".xls"    ".xslx"
                ;; Image files
                ".gif"    ".jpeg"   ".jpg"    ".png"
                ;; Audio files
                ".aac"    ".flac"   ".m4a"    ".mp3"
                ;; Video files
                ".avi"    ".flv"    ".mkv"    ".mp4"    ".webm"
                ;; Object code, executable files, shared and dynamically linked libraries (‘.o’, ‘.so’, ‘.elc’, ‘.pyc’ and ‘.pyo’ are in the default list)
                ".exe"    ".mod"    ".out"
                ;; Archive files
                ".7z"     ".rar"    ".tar"    ".tar.gz" ".tgz"    ".tar.xz" ".zip"
                ;; LaTeX auxiliary files (‘.blg’, ‘.bbl’, ‘.lof’, ‘.glo’, ‘.idx’, ‘.lot’, ‘.toc’ and ‘.aux’ are in the default list)
                ".bcf"    ".run.xml" ".synctex.gz"      ".gz(busy)"
                ;; Others
                ".lnk"
                ;; Some uppercase variants.
                ;; TODO: Add the uppercase version of all elements to the list automatically. See https://emacs.stackexchange.com/questions/41106/ignore-both-uppercase-and-lowercase-versions-of-the-extensions-listed-in-complet
                ".JPG"    ".PDF"    ".PNG")
              completion-ignored-extensions))

(defun skip-buffer-p (_window buffer _bury-or-kill)
  "Return t if BUFFER should be skipped by ‘switch-to-{prev,next}-buffer’,
otherwise return nil.
This function is meant to be used as value of
‘switch-to-prev-buffer-skip’."
  (let ((buffername (buffer-name buffer))
        (buffer-filename (buffer-file-name buffer))
        (ido-ignore-buffers-subset (remove "\\`\\*scratch\\*\\'" ido-ignore-buffers)))
    (catch 'matched
      (dolist (elt ido-ignore-buffers-subset)
        (if (stringp elt)
            (when (string-match-p elt buffername)
              (throw 'matched t))
          ;; If an element of ‘ido-ignore-buffers-subset’ is not a string then it must be a predicate that takes a file name as argument.
          (when (funcall elt (or buffer-filename buffername)) ; Match ‘elt’ against the name of the file visited by ‘buffer’ or the name of ‘buffer’ itself. (The important thing here is not to give nil as argument to ‘elt’, because it may not handle it.)
            (throw 'matched t)))))))
(setq switch-to-prev-buffer-skip #'skip-buffer-p)

(defun TeX-log-file-p (name)
  "This function takes a file name as input and returns t if it’s
a log file and a ‘.tex’ file with the same name exists, otherwise
it returns nil."
  (when (string-match-p "\\.log\\'" name)
    (let ((tex-file-name (replace-regexp-in-string "\\.log\\'" ".tex" name)))
      (file-exists-p tex-file-name))))

;;; Ibuffer

;; http://www.xsteve.at/prg/emacs/power-user-tips.html
;; http://ergoemacs.org/emacs/emacs_buffer_management.html
;; http://martinowen.net/blog/2010/02/03/tips-for-emacs-ibuffer.html

(setq ibuffer-expert t) ;                     Don’t ask for confirmation before killing unmodified buffers.
(setq ibuffer-show-empty-filter-groups nil) ; Hide empty filters’ names.
(setq ibuffer-use-other-window t)

(defun set-up-ibuffer-mode ()
  "Function with customizations for ‘ibuffer-mode’."
  (ibuffer-auto-mode)) ; Keep the buffer list up to date
(add-hook 'ibuffer-mode-hook #'set-up-ibuffer-mode)

(global-set-key [remap list-buffers] #'ibuffer)

;;; Imenu

(setq imenu-auto-rescan t)
(global-set-key (kbd "C-c i") #'ido-imenu-anywhere) ; https://github.com/vspinu/imenu-anywhere



;;;; Minibuffer

;; Make ‘M-<’ and ‘C-<home>’ move to the end of the prompt
(setq minibuffer-beginning-of-buffer-movement t) ;                                    This takes care of ‘M-<’ and
(define-key minibuffer-local-map (kbd "C-<home>") #'minibuffer-beginning-of-buffer) ; this of ‘C-<home>’.

;; Match the input up to the point when using the minibuffer’s history completion
;; https://www.emacswiki.org/emacs/MinibufferHistory#toc2

(defun magic-next-history-element (arg) ; Took the name from Ido’s ‘magic-*’ commands that do different things based on the circumstances.
  "Insert the next minibuffer history element that matches the
current input up to the point. If we’re ahead of all history
elements (i.e. the previous element in history is the latest
one), fetch the default completions, AKA \“future history\”.
With prefix argument ARG, use the ARG-th suitable element,
ahead if ARG is positive or behind if it is negative."
  ;; The ‘minibuffer-history-position’ starts out at 0 on calling a command (or at least so it does on calling ‘write-file’) and goes up as we go back through history. ‘next-complete-history-element’ never brings it back to 0, it gets to 1 then raises an error if called again.
  ;; The history list that ‘minibuffer-history-position’ refers to can be seen by evaluating the function ‘minibuffer-history-value’.
  ;; The default completions are stored in the variable ‘minibuffer-default’.
  ;; See also ‘(info "(emacs) Minibuffer History")’.
  (interactive "p")
  (if (>= 0 minibuffer-history-position)
      (next-history-element arg)
    (next-complete-history-element arg)))

(define-key minibuffer-local-map [remap previous-history-element] #'previous-complete-history-element)
(define-key minibuffer-local-map [remap next-history-element] #'magic-next-history-element)

(define-key minibuffer-local-map (kbd "<M-tab>") #'next-history-element) ; Keep a key binding for ‘next-history-element’ always at hand as its default one (‘M-n’) sometimes is overridden  (e.g. by ‘ido-completion-map’). ‘next-history-element’ can fetch useful completions, referred to as “future history” sometimes in documentation and stored the variable ‘minibuffer-default’.

;;; Minibuffer completion

;; https://www.gnu.org/software/emacs/manual/html_mono/ido.html
;; https://www.emacswiki.org/emacs/InteractivelyDoThings
;; https://www.masteringemacs.org/article/introduction-to-ido-mode

(require 'ido)

(setq ido-everywhere t)
(setq ido-enable-flex-matching t)
(setq ido-use-virtual-buffers t) ;                          If recentf is enabled, you can use ‘C-x b’ to visit recently closed files. http://wikemacs.org/wiki/Ido#Virtual_Buffers
(setq ido-auto-merge-work-directories-length -1) ;          Disable automatic search in subdirectories during file name input. Type ‘M-s’ to launch the search manually. https://stackoverflow.com/questions/17986194/emacs-disable-automatic-file-search-in-ido-mode
(setq ido-create-new-buffer 'always) ;                      Don’t ask for confirmation before creating a new buffer. Note: there’s a separate confirmation prompt, unrelated with Ido, whose activation is controlled by the variable ‘confirm-nonexistent-file-or-buffer’. See (2 links) https://www.masteringemacs.org/article/introduction-to-ido-mode (see also Justin Andrusk’s comment), https://emacs.stackexchange.com/questions/2496/how-to-switch-to-a-new-buffer-with-ido-without-needing-to-confirm

;; Allow spaces when using ‘ido-find-file’
;; https://xgarrido.github.io/emacs-starter-kit/starter-kit-ido.html
(add-hook 'ido-make-file-list-hook
          (lambda ()
            (define-key ido-file-dir-completion-map (kbd "SPC") 'self-insert-command)))

;; Use ‘~’ to go straight to the home directory
;; It’s a bad idea if your files are prefixed with ‘~’.
;; Note: with the default settings you can go to the home directory by typing ‘~/’.
;; http://whattheemacsd.com/setup-ido.el-02.html
(defun ido-go-to-home-dir ()
  (interactive)
  (if (looking-back "/")
      (insert "~/")
    (call-interactively #'self-insert-command)))
(define-key ido-file-completion-map (kbd "~") #'ido-go-to-home-dir)

(defun ido-kill-buffer-no-virtuals ()
  "Like ‘ido-kill-buffer’, except this command doesn’t show
virtual buffers."
  (interactive)
  (let ((ido-use-virtual-buffers nil))
    (call-interactively #'ido-kill-buffer)))
(global-set-key (kbd "C-x k") #'ido-kill-buffer-no-virtuals)

;; REVIEW: Are there any downsides to this remapping?
;;   See https://lists.gnu.org/archive/html/help-gnu-emacs/2021-02/msg00695.html
;;       https://www.reddit.com/r/emacs/comments/lqp8qa/ido_let_cd_open_a_dired_buffer_for_commands_other/
(defun my-ido-magic-delete-char ()
  "Like ‘ido-magic-delete-char’ except this can open ‘dired’ on
the candidate directory of commands that use Ido completion only
when ‘ido-everywhere’ is non-nil."
  (interactive)
  (let ((ido-context-switch-command nil))
    (call-interactively #'ido-magic-delete-char)))
(define-key ido-common-completion-map [remap ido-magic-delete-char] #'my-ido-magic-delete-char)

;; Visit the buffer at the head of ‘ido-matches’ in another window
;; https://emacs.stackexchange.com/questions/70056/how-to-define-a-command-to-switch-to-the-current-candidate-buffer-in-ido

(defun ido-visit-buffer-other-window ()
  (interactive)
  (let ((buf (car ido-matches))
        entry)
    (cond
     ((get-buffer buf)
      (add-to-history 'buffer-name-history buf)
      (ido-visit-buffer buf 'other-window t))
     ;; Check for a virtual buffer reference.
     ((and ido-enable-virtual-buffers
           ido-virtual-buffers
           (setq entry (assoc buf ido-virtual-buffers)))
      (ido-visit-buffer (find-file-noselect (cdr entry)) 'other-window t))
     (t
      (user-error "No match")))))

(defun ido-run-visit-buffer-other-window ()
  (interactive)
  (setq ido-exit 'fallback)
  (setq ido-fallback 'ido-visit-buffer-other-window)
  (exit-minibuffer))
(define-key ido-buffer-completion-map "\C-n" #'ido-run-visit-buffer-other-window)

;; Ignore some files and buffers

(setq ido-ignore-files (append ;                    Note: Ido also ignores files in ‘completion-ignored-extensions’ if ‘ido-ignore-extensions’ is t (which is the default).
                        '("\\`\\." ;                Ignore hidden files and directories. REVIEW: Why doesn’t Ido ignore them without this setting? (It doesn’t even if I launch Emacs with ‘emacs -Q’ and just evaluate ‘(require 'ido) (ido-mode)’.) The manual says it should, see https://www.gnu.org/software/emacs/manual/html_node/ido/Hidden-Buffers-and-Files.html
                          "\\`_region_\\.prv/\\'" ; Created when ‘preview-region’ is used on (La)TeX documents.
                          "\\`_region_\\.tex\\'" ;  Created when ‘preview-region’ is used on (La)TeX documents.
                          TeX-log-file-p)
                        ido-ignore-files))

(setq ido-ignore-buffers
      (append (list "\\`\\*.*Completions\\*\\'"
                    "\\`\\*.*Help\\*\\'"
                    "\\`\\*.*[Ll]og\\*\\'" ;     Created when compiling documents with AUCTeX.
                    "\\`\\*.*[Oo]utput\\*\\'" ;  Created when compiling documents with AUCTeX.
                    "\\`\\*.*Select\\*\\'"
                    "\\`\\*anaconda-mode\\*\\'"
                    "\\`\\*Buffer List\\*\\'"
                    "\\`\\*Choices\\*\\'"
                    "\\`\\*compilation\\*\\'"
                    "\\`\\*ESS\\*\\'"
                    "\\`\\*gnuplot\\*\\'"
                    "\\`\\*Ibuffer\\*\\'"
                    "\\`\\*Kill Ring\\*\\'" ;    Created by ‘M-x browse-kill-ring’.
                    "\\`\\*Messages\\*\\'"
                    "\\`_region_\\.tex\\'" ;     Created when ‘preview-region’ is used on (La)TeX documents.
                    "\\`\\*scratch\\*\\'"
                    #'TeX-log-file-p
                    openwith-regexps-combined) ; Ignore files handled by Openwith.
              ido-ignore-buffers))

(ido-mode)

;; Disable Ido for some commands
;; Adding a command to the list ‘ido-read-file-name-non-ido’ disables Ido completion for that command. By itself this works only for commands that don’t use Ido completion unless ‘ido-everywhere’ is enabled. I think commands that use Ido even when ‘ido-everywhere’ is turned off do so because Ido remaps their key bindings to the ‘ido-’ version of the original command. In that case you have to undo Ido’s remapping in addition to disabling Ido completion for the original command.
;; https://emacs.stackexchange.com/questions/26869/disable-ido-mode-for-write-file
;; https://emacs.stackexchange.com/questions/13713/how-to-disable-ido-in-dired-create-directory/
(dolist (cmd '(dired-create-directory
               write-file
               set-visited-file-name
               my-find-file-at-point))
  (add-to-list 'ido-read-file-name-non-ido cmd))
(define-key (cdr ido-minor-mode-map-entry) [remap write-file] nil) ; must be evaluated after Ido has been initialized (i.e. after ‘(ido-mode)’).

;; Use Ido to complete ‘M-x’ commands
;; https://github.com/nonsequitur/smex

(when (fboundp 'smex) ;                                 Leave ‘M-x’ bound to ‘execute-extended-command’ if the function ‘smex’ is not defined. This relieves Emacs’ confusion when libraries are not at their place.
  (global-set-key (kbd "M-x") #'smex))
(global-set-key "\S-\M-x" #'smex-major-mode-commands) ; Show only major mode commands. REVIEW: Using ‘kbd’, the binding only works with ‘(kbd "M-X")’, it doesn’t with ‘(kbd "S-M-x")’, nor with ‘(kbd "M-S-x")’. Same for ‘\S-\M-y’, bound to ‘yank-pop-forwards’, and for ‘\S-\M-o’ (‘spawn-newline-above’).

(smex-initialize) ;                                     Can be omitted. This might cause a (minimal) delay when Smex is auto-initialized on its first run. https://github.com/nonsequitur/smex



;;;; Keyboard macros

(setq kmacro-ring-max 20)

;; Add the current keyboard macro, counter and counter format to the ‘kmacro-ring’ before quitting.
;; The ‘-10’ argument is for placing ‘kmacro-push-ring’ earlier in the hook, so that it gets called before ‘savehist-autosave’ when the hook runs.
;; REVIEW: In order to retrieve the last keyboard macro of the previous session when I start a new one I first have to invoke ‘kmacro-cycle-ring-previous’ followed by ‘kmacro-cycle-ring-next’ (or the other way around).
(add-hook 'kill-emacs-hook #'kmacro-push-ring -10)



;;;; Calc

;; See also ‘./calc.el’.

(setq calc-context-sensitive-enter t) ; Make ‘calc-enter’ copy the element at the cursor to the top of the stack and ‘calc-pop’ delete the element at the cursor.
(setq calc-undo-length 'yes) ;          Keep track of all undo steps.



;;;; Help and Info

(setq help-window-select t) ;                                     Select the help buffer when a new one is created.
(setq find-function-C-source-directory
      (concat "~/.rpmbuild/BUILD/emacs-" emacs-version "/src")) ; Location of the C source tree used to access function definitions through the help system.
(dolist (dir `("~/.local/share/info" ;                            This directory is one of those that are always searched by standalone Info, at least in Fedora 33. See https://ask.fedoraproject.org/t/how-to-change-the-infopath-possibly-only-user-wide/13026
               ,(car (last (file-expand-wildcards "/usr/local/texlive/20*/texmf-dist/doc/info"))))) ; Documentation installed by the TUG TeX Live installer.
  (add-to-list 'Info-additional-directory-list dir))



;;;; Diff and version control

(setq ediff-window-setup-function #'ediff-setup-windows-plain) ; Don’t create a new frame when Ediff starts. https://emacs.stackexchange.com/questions/17064/never-create-frame-in-ediff
;(setq diff-font-lock-prettify t) ;                              It makes it look like the insertion and deletion indicators are not part of the text when they actually are (and so they are killed and yanked with the rest of the text even if they’re displayed only on the fringe).
(defun disable-global-hl-line-mode-here ()
  "Disable ‘global-hl-line-mode’ in the current buffer."
  (setq-local global-hl-line-mode nil))
(add-hook 'diff-mode-hook #'disable-global-hl-line-mode-here)



;;;; Remote access

;; Make Tramp use the path assigned to the remote user by the remote host
;; Note: When remote search paths are changed, local Tramp caches must be recomputed. To force Tramp to recompute afresh, exit Emacs, remove the persistent file (whose full name is stored in the variable ‘tramp-persistency-file-name’), and restart Emacs.
;; https://www.gnu.org/software/emacs/manual/html_node/tramp/Remote-programs.html
(with-eval-after-load 'tramp
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path))

;; Set-up for remote shell sessions over SSH
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Connection-Local-Variables.html
;; § 6.5.2, https://www.gnu.org/software/emacs/manual/html_node/tramp/Remote-processes.html
(connection-local-set-profile-variables
 'remote-bash
 '((explicit-shell-file-name  . "/bin/bash")
   (shell-command-switch      . "-c")
   (shell-interactive-switch  . "-i")
   (shell-login-switch        . "-l")))
(connection-local-set-profiles
 '(:application  tramp
   :protocol     "ssh")
 'remote-bash)



;;;; Settings and data to save to file

;;; Filesets

;; See ‘C-h v filesets-data’
;;     https://www.gnu.org/software/emacs/manual/html_node/emacs/Filesets.html

(filesets-init)

;;; History of visited files

;; https://www.emacswiki.org/emacs/RecentFiles

(require 'recentf)

(setq recentf-max-saved-items 350) ;                  Number of items to save in the ‘recentf-list’.
(setq recentf-filename-handlers ;                     Abbreviate file names (basically, replace ‘/home/<user>’ with a tilde).
      (cons 'abbreviate-file-name recentf-filename-handlers))
;; ‘*Open Recent*’ buffer set-up
(setq recentf-menu-filter #'recentf-arrange-by-dir) ; Group files by directory.
(setq recentf-show-file-shortcuts-flag nil) ;         Hide the shortcuts for the most recent files.
(global-set-key (kbd "C-x C-r") #'recentf-open-files)

;; Files to exclude from the list
;; ‘C-h v read-library-name’ mentions a couple of variables that hold paths to libraries (namely, ‘load-path’ and ‘find-function-source-path’). You can append those to the ‘recentf-exclude’ list. Anyway, the regexps currently in use already match pretty much all libraries, I think, and they allow for finer control (e.g. non ‘.el’ files in library directories are allowed in the ‘recentf-list’).
;; https://stackoverflow.com/questions/8025119/force-emacs-recent-files-using-recentf-to-ignore-specified-files-windows-and
(setq recentf-exclude
      (let ((system-dir (file-name-as-directory (expand-file-name "../.." data-directory)))) ; I've copied the ‘..’ inside an ‘expand-file-name’ form from ‘startup.el’, where ‘(expand-file-name "../lisp" data-directory)’ is used a couple of times. There’s a caveat with using ‘..’ in ‘expand-file-name’ but it shouldn’t apply here; see the last paragraph of ‘expand-file-name’’s documentation.
        (append
         (list find-function-C-source-directory ;                    Emacs C source code files (accessible through the help system). Note that ‘find-function-C-source-directory’ has not yet been set at this point.
               (concat system-dir ".+\\.el") ;                       Emacs libraries (accessible through the help system).
               (concat package-user-dir ".+\\.el") ;                 Emacs libraries (installed packages). Adapted from https://www.reddit.com/r/emacs/comments/3g468d/stop_recent_files_showing_elpa_packages/
               ;(concat my-lib-dir ".+\\.el") ;                      Emacs libraries not managed by ‘package.el’.
               ;".*-autoloads\\.el\\'" ;                             Files of autoload objects (but also my script ‘update-library-autoloads.el’); from https://www.reddit.com/r/emacs/comments/3g468d/stop_recent_files_showing_elpa_packages/
               data-directory ;                                      Emacs documentation (accessible via ‘about-emacs’).
               (concat user-emacs-directory "bookmarks") ;           Emacs bookmarks list (that shown after calling ‘bookmark-bmenu-list’ (‘C-x r l’)).
               "filesets-cache.el" ;                                 Not necessary if ‘(recentf-mode)’ comes after ‘(filesets-init)’.
               "ido.last" ;                                          Not necessary if ‘(recentf-mode)’ comes after ‘(ido-mode)’.
               (concat user-emacs-directory "session\\.[0-9a-f]+") ; Session files which Emacs dumps when the window manager notifies it that it is shutting down; see https://emacs.stackexchange.com/questions/12679/session-files-in-emacs-d-folder
               "COMMIT_EDITMSG\\'" ;                                 From https://www.reddit.com/r/emacs/comments/3g468d/stop_recent_files_showing_elpa_packages/
               (cdar backup-directory-alist) ;                       Any file in the backup directory. Note that this works as intended only if all backups are dumped to a single directory.
               "_region_\\.tex\\'" ;                                 File created when ‘preview-region’ is run on (La)TeX documents.
               #'TeX-log-file-p) ;                                   ‘.log’ files (this is mostly for LaTeX compilation logs, which AUCTeX visit when it finds errors). REVIEW: Is there a way to make recentf ignore only *TeX compilation logs? (Maybe one based on a predicate that checks the content of the log file, or for the existence a file with the same name except for the extension ‘.tex’.)
         recentf-exclude)))

;; Display the list of recent files using Ido
;; TODO: Make them display just the basenames (like ‘ido-switch-buffer’ does), and make ‘C-f’ trigger fallback to non-Ido ‘find-file’ (see ‘C-h f ido-magic-forward-char’).
;; https://www.masteringemacs.org/article/find-files-faster-recent-files-package

(defun ido-recentf-open ()
  "Use ‘ido-completing-read’ to \\[find-file] a recent file."
  (interactive)
  (if (find-file (ido-completing-read "Find recent file: " recentf-list))
      (message "Opening file…")
    (message "Aborting")))
(global-set-key (kbd "C-x f") #'ido-recentf-open)

(defun ido-recentf-open-alternate ()
  "Use ‘ido-completing-read’ to \\[find-alternate-file] a recent file."
  (interactive)
  (let ((recentf-list-alt
         (if (buffer-file-name)
             (remove (abbreviate-file-name buffer-file-name) recentf-list) ; REVIEW: Does this slow down the evaluation of the function if the ‘recentf-list’ is very long?
           recentf-list)))
    (if (find-alternate-file (ido-completing-read "Find alternate recent file: " recentf-list-alt))
        (message "Opening file…")
      (message "Aborting"))))
(global-set-key (kbd "C-x M-v") #'ido-recentf-open-alternate)

(recentf-mode)

;;; Minibuffer histories and more

;; https://emacs-fu.blogspot.it/2009/05/saving-history-between-sessions.html
;; https://www.reddit.com/r/emacs/comments/b6kfox/questions_concerning_popupkillring_package_or/

;; You don’t need to add minibuffer history variables to this list, all minibuffer histories will be saved automatically as long as ‘savehist-save-minibuffer-history’ is non-nil (from ‘C-h v savehist-additional-variables’).
;; The variable ‘savehist-minibuffer-history-variables’ holds the list of histories that are saved by default. You can ignore any of those histories by adding them to the variable ‘savehist-ignored-variables’.
;; Note on saving registers, from https://www.reddit.com/r/emacs/comments/c9dewr/persistent_registers/
;;   Registers are saved in the variable ‘register-alist’. If ‘register-alist’ contains any window configurations then savehist won’t save that variable at all, so you’d lose *all* registers. By default ‘desktop-save-mode’ does slightly better with saving ‘register-alist’ than savehist in that window configs end up being saved as “Unprintable entity” and therefore do not prevent the *other* registers from being saved as well.
(setq savehist-additional-variables
      '(compile-command
        Info-history-list
        ;kill-ring
        kmacro-ring)) ; REVIEW: How can I save the last keyboard macro (which is not stored in ‘kmacro-ring’)? Saving ‘last-kbd-macro’ doesn’t work.

;; When Savehist mode is enabled, minibuffer history is saved to ‘savehist-file’ periodically and when exiting Emacs. The variable ‘savehist-autosave-interval’ controls the periodicity of saving minibuffer histories (from ‘C-h f savehist-mode’).
;; NOTE: You should do the customizations before enabling savehist-mode, or they will be ignored. https://emacs-fu.blogspot.it/2009/05/saving-history-between-sessions.html
(savehist-mode)

;;; Sessions

;; See ‘M-x find-library RET desktop’

(setq desktop-save 'ask-if-exists)

;; Variables to save in desktop files
;; These override those saved by ‘savehist.el’.
;; Note that there is not ‘command-history’.
;; Adapted from http://www.xsteve.at/prg/emacs/power-user-tips.html
(with-eval-after-load 'desktop
  (dolist (var '(compile-history
                 extended-command-history
                 grep-history
                 minibuffer-history
                 query-replace-history
                 read-expression-history
                 regexp-history
                 shell-command-history))
    (add-to-list 'desktop-globals-to-save var)))

;(desktop-save-mode)

;;; Last cursor position

;; When ‘save-place-mode’ is enabled it saves the point when a buffer is killed but it also restores it automatically when its file is visited again, which I don’t want it to do. It does that by adding ‘save-place-find-file-hook’ -- which does the restoring of the position -- to ‘find-file-hook’.
;; To change the behaviour so that the restoring can be done on demand, ‘save-place-find-file-hook’ is removed from the hook and wrapped in an interactive function.
;; See https://emacs.stackexchange.com/questions/50909/save-place-in-files-but-dont-move-the-cursor-to-the-saved-position-automaticall

(save-place-mode)
(remove-hook 'find-file-hook #'save-place-find-file-hook)
(remove-hook 'dired-initial-position-hook #'save-place-dired-hook)

(defun goto-saved-place ()
  "Move the point to the last place where it was when you
previously visited the same file."
  (interactive "^")
  (save-place-find-file-hook))
(global-set-key (kbd "C-c '") #'goto-saved-place)



;;;; Look

(defvar light-grey-focused      "grey80")
(defvar lighter-grey-focused    "grey84")
(defvar lightest-grey-focused   "grey93")

(defvar light-grey-unfocused    "grey84")
(defvar lighter-grey-unfocused  "grey90")
(defvar lightest-grey-unfocused "grey97")

(menu-bar-mode -1) ;                       Hide the menu bar.
(tool-bar-mode -1) ;                       Hide the tool bar.
(setq default-frame-alist
      (append ;                            Note: if there are any conflicting settings in ‘default-frame-alist’, it is the one that comes first that gets applied.
       '((font . "DejaVu Sans Mono-10")) ; Use DejaVu Sans Mono at 10 pt size. REVIEW: If this setting is in the normal init file it doesn’t affect non-client frames. https://www.gnu.org/software/emacs/manual/html_node/emacs/Fonts.html
       default-frame-alist))
(setq initial-frame-alist
      (append
       '((width . 80) (height . 40))
       initial-frame-alist))
(setq frame-title-format '("%b (" mode-name ") – " invocation-name)) ; See also the variable ‘icon-title-format’. https://emacs.stackexchange.com/questions/16834/how-to-change-the-title-from-emacshost-to-file-name, https://www.gnu.org/software/emacs/manual/html_node/elisp/_0025_002dConstructs.html, https://www.gnu.org/software/emacs/manual/html_node/elisp/Mode-Line-Variables.html
(setq inhibit-startup-screen t) ;           No start-up screen.
(setq initial-scratch-message nil) ;        Blank scratch buffer at start-up.
(setq-default truncate-lines t) ;           Truncate long lines. Unlike in ‘visual-line-mode’, line editing commands such as ‘kill-line’ still act on the whole line as if it wasn’t wrapped. Andy Gimblett, https://stackoverflow.com/questions/3281581/how-to-word-wrap-in-emacs#comment24345478_3282132; https://www.emacswiki.org/emacs/TruncateLines
(setq truncate-partial-width-windows nil) ; Always respect the value of ‘truncate-lines’.
(setq-default word-wrap t) ;                Don’t break words at the end of soft-wrapped lines.
(global-font-lock-mode) ;                   Enable syntax highlighting. http://community.schemewiki.org/?emacs-tutorial
(set-face-background 'region "#ffc494") ;   Region background colour.

(define-minor-mode soft-wrap-mode
  "Fold long lines using context-aware indentation of continuation
lines. Unlike in ‘visual-line-mode’, line editing commands still work on
the entire logical line."
  :lighter " ↩"
  (let ((screen-line (1+ (cdr (nth 6 (posn-at-point))))))
    (if soft-wrap-mode
        (progn
          (let ((inhibit-message t)) (toggle-truncate-lines -1)) ; Suppress ‘toggle-truncate-lines’’s message in order to let the one about the mode’s activation or deactivation be displayed.
          (adaptive-wrap-prefix-mode +1))
      (let ((inhibit-message t)) (toggle-truncate-lines +1))
      (adaptive-wrap-prefix-mode -1)
      (when (> (current-column) (window-width))
        (my-horizontal-recenter))) ; REVIEW: Isn’t there a built-in function for recentering the point horizontally?
    (set-window-start nil (save-excursion
                            (vertical-motion (- (1- screen-line)))
                            (point)))))
(put 'soft-wrap-mode 'scroll-command t) ; Don’t exit Isearch on invocation of ‘soft-wrap-mode’ (provided that ‘isearch-allow-scroll’ is non-nil).
(global-set-key [f9] #'soft-wrap-mode)

;;; Cursor

;; Default to bar cursor
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Cursor-Display.html
(setq-default cursor-type 'bar)

;; Make it blink
;; (When Emacs is started as a daemon, ‘blink-cursor-mode’ is not enabled by default.)
(blink-cursor-mode)

;; Use a block cursor in overwrite mode
;; https://emacs.stackexchange.com/questions/44650/how-can-i-make-the-cursor-change-to-block-in-overwrite-mode
(defun set-up-overwrite-mode ()
  "Function with customizations for ‘overwrite-mode’."
  (setq cursor-type (if overwrite-mode t 'bar)))
(add-hook 'overwrite-mode-hook #'set-up-overwrite-mode)

;;; Mode line

(setq column-number-mode t) ; Display the column number. https://www.gnu.org/software/emacs/manual/html_node/efaq/Displaying-the-current-line-or-column.html
(set-face-attribute 'mode-line nil :background lighter-grey-focused)

;; Flat mode line
;; https://www.reddit.com/r/emacs/comments/23l9oi/flat_modeline/
(set-face-attribute 'mode-line nil :box nil)
(set-face-attribute 'mode-line-inactive nil :box nil)

;; Flat tab bar
;; TODO: Take the colours from the theme. REVIEW: ‘face-attribute’ forms in these settings might mess up the colours when using Solarized.
(set-face-attribute 'tab-bar nil
                    :background lightest-grey-focused)
(set-face-attribute 'tab-bar-tab nil
                    :background (face-attribute 'default :background)
                    :box nil)
(set-face-attribute 'tab-bar-tab-inactive nil
                    :background light-grey-focused
                    :box nil)

;; Hide some lighters
;; https://emacs.stackexchange.com/questions/3925/hide-list-of-minor-modes-in-mode-line/3928#3928

(defvar hidden-minor-modes
  '(;; Modes active at Emacs start-up, independently of the major mode
    ;; Modes turned on on a per-major-mode basis
    buffer-face-mode
    highlight-indent-guides-mode
    subword-mode)
  "List of minor modes whose lighter is to be hidden.")

(defun purge-minor-modes ()
  "Hide the lighter of the minor modes listed in
‘hidden-minor-modes’."
  (dolist (x hidden-minor-modes)
    (let ((trg (cdr (assoc x minor-mode-alist))))
      (when trg
        (setcar trg "")))))
(add-hook 'after-change-major-mode-hook #'purge-minor-modes)

;;; Scroll bars

(setq scroll-bar-adjust-thumb-portion nil) ; Make the thumb bottom out when the end of the buffer becomes visible.

(defun hide-minibuffer-scroll-bar ()
  "Hide the minibuffer window’s scroll bar."
  ;; https://emacs.stackexchange.com/questions/39438/how-can-i-disable-the-vertical-scroll-bar-on-just-the-minibuffer
  ;; REVIEW: ‘(set-window-scroll-bars (minibuffer-window) nil nil)’ should suffice. The rest is a workaround for bug #36193.
  ;;   See http://emacs.1067599.n8.nabble.com/bug-36193-26-2-set-window-scroll-bars-setting-doesn-t-take-effect-in-emacsclient-session-td486662.html#a487294
  (progn
    (set-window-scroll-bars (minibuffer-window) nil nil)
    (with-current-buffer (get-buffer-create " *Echo Area 0*")
      (setq scroll-bar-width 0))
    (with-current-buffer (get-buffer-create " *Echo Area 1*")
      (setq scroll-bar-width 0))
    (with-current-buffer (get-buffer-create " *Minibuf-0*")
      (setq scroll-bar-width 0))
    (with-current-buffer (get-buffer-create " *Minibuf-1*")
      (setq scroll-bar-width 0))))
(if (daemonp)
    (add-hook 'server-after-make-frame-hook #'hide-minibuffer-scroll-bar)
  (hide-minibuffer-scroll-bar))

;;; Visual aids

;; Current line highlighter

(global-hl-line-mode)

(set-face-background 'hl-line "#e0eeff")

;; Fill column indicator

(setq-default display-fill-column-indicator-character ?│) ; REVIEW: If ‘use-default-font-for-symbols’ is nil, Emacs defaults to using | (#x7C, 124) instead of │ (#x2502, 9474).

;; Copy the colour used by highlight-indent-guides
;; FIXME (bug?): The indent guides and the fill column indicator turn black when they’re inside the active region. Apparently this happens only when they use a grey lighter than #999999.
(defun fci-copy-hig-colour ()
  (highlight-indent-guides-auto-set-faces)
  (set-face-attribute 'fill-column-indicator nil :foreground (face-attribute 'highlight-indent-guides-character-face :foreground)))
(add-hook 'display-fill-column-indicator-mode-hook #'fci-copy-hig-colour)

;; Indentation guides
;; https://github.com/DarthFennec/highlight-indent-guides

;; Use thin lines as indentation guides
(setq highlight-indent-guides-method 'character) ; FIXME (bug?): Same as for the fill column indicator, search “turn black when they’re inside the active region”.
;; Use this as a replacement for the ‘'character’ method once the following issue is sorted:
;; REVIEW: These guides change the cursor’s shape to a hollow block when it’s over them.
;;   See https://github.com/DarthFennec/highlight-indent-guides/issues/105
;;       https://emacs.stackexchange.com/questions/9882/selecting-the-cursor-type-when-using-an-overlay-cursor-property
;(setq highlight-indent-guides-method 'bitmap)
;(setq highlight-indent-guides-bitmap-function #'highlight-indent-guides--bitmap-line)

;; Let indentation guides in new client frames get the right colour
;; REVIEW: This is a workaround for https://github.com/DarthFennec/highlight-indent-guides/issues/67
;; I've commented it out because it’s included in the setting of the fill column indicator colour, below.
;(add-hook 'server-after-make-frame-hook #'highlight-indent-guides-auto-set-faces)

;; Alternating line background colours
;; https://gitlab.com/stepnem/stripes-el

(setq stripes-unit 1)
(setq stripes-overlay-priority -60)

(add-hook 'csv-mode-hook #'stripes-mode)

;;; View with narrow text area

;; TODO: Make the margins grey but keep the fringe indicators visible and close to the window text. See https://emacs.stackexchange.com/questions/70248/is-it-possible-to-move-the-fringe-indicators-closer-to-the-window-text

(with-eval-after-load 'olivetti
  (setq olivetti-mode-on-hook (delete 'visual-line-mode olivetti-mode-on-hook))) ; Prevent ‘olivetti-mode’ from activating ‘visual-line-mode’.
(setq olivetti-lighter "") ;                                                       Hide ‘olivetti-mode’’s lighter.

(defun set-up-olivetti-mode ()
  "Function with customizations for ‘olivetti-mode’."
  ;; Paint the fringes the same colour as the background and make the indicators subtler.
  (set-face-attribute 'olivetti-fringe nil
                      :foreground (pcase (car custom-enabled-themes)
                                    ('nil light-grey-focused)
                                    ('solarized "#dacda4"))
                      :background (face-attribute 'default :background))
  ;; Make the text block ‘fill-column’ characters wide.
  ;; I’d set ‘olivetti-body-width’ to ‘fill-column’ but… REVIEW (bug?): When both ‘olivetti-mode’ and ‘buffer-face-mode’ are enabled, the actual line width is different from the ‘olivetti-body-width’. See the function ‘olivetti-scale-width’ in ‘olivetti.el’.
  (setq olivetti-body-width
        (let ((family (plist-get (cadr (assq 'default face-remapping-alist)) :family))) ; Adapted from ‘olivetti-scale-width’.
          (if (equal family "Inconsolata")
              (round (/ fill-column 1.22)) ; This returns a value for ‘olivetti-body-width’ that results in an actual line width closer to the value of ‘fill-column’, at least in the interval [70,80].
            fill-column))))
(add-hook 'olivetti-mode-hook #'set-up-olivetti-mode)

(global-set-key [f6] #'olivetti-mode)

;;; Themes

;; Dim down some interface elements’ colour when the frame isn’t focused.

;; FIXME: This set-up causes the frame to blank out while using the menu bar, when Solarized is enabled.

(defun dim-interface-colours ()
  "Dim down some interface elements’ colour."
  (pcase (car custom-enabled-themes)
    ('nil
     (set-face-background 'hl-line "#f1f4f9")
     (set-face-background 'region "#f0d5c0")
     (set-face-background 'mode-line lighter-grey-unfocused)
     (set-face-background 'mode-line-inactive lightest-grey-unfocused)
     (set-face-background 'tab-bar lightest-grey-unfocused)
     (set-face-background 'tab-bar-tab-inactive light-grey-unfocused))
    ('solarized
     (custom-theme-set-faces 'solarized
                             '(hl-line ((t (:background "#f4f0e7"))))
                             '(region ((t (:background "#fefaf1" :foreground "#b2bcbc" :inverse-video t))))
                             '(default ((t (:background "#fefaf1"))))
                             '(fringe ((t (:background "#f4f0e7" :foreground "#b2bcbc")))))
     (enable-theme 'solarized))))
(add-hook 'my-focus-out-hook #'dim-interface-colours)

;; TODO: Read the colours from the theme instead of hard-coding them here.
(defun restore-interface-colours ()
  "Restore the colours that were changed by
‘dim-interface-colours’."
  (pcase (car custom-enabled-themes)
    ('nil
     (set-face-background 'hl-line "#e0eeff")
     (set-face-background 'region "#ffc494")
     (set-face-background 'mode-line lighter-grey-focused)
     (set-face-background 'mode-line-inactive lightest-grey-focused)
     (set-face-background 'tab-bar lightest-grey-focused)
     (set-face-background 'tab-bar-tab-inactive light-grey-focused))
    ('solarized
     (custom-theme-set-faces 'solarized
                             '(hl-line ((t (:background "#eee8d5"))))
                             '(region ((t (:background "#fdf6e3" :foreground "#93a1a1" :inverse-video t))))
                             '(default ((t (:background "#fdf6e3"))))
                             '(fringe ((t (:background "#eee8d5" :foreground "#93a1a1")))))
     (enable-theme 'solarized))))
(add-hook 'my-focus-in-hook #'restore-interface-colours)



;;;;; Text editing

;;;; Marking, killing and yanking

(delete-selection-mode) ;                                          Typing overwrites selected text. https://www.gnu.org/software/emacs/manual/html_node/efaq/Replacing-highlighted-text.html
(global-set-key (kbd "<S-down-mouse-1>") #'mouse-save-then-kill) ; Select text with ‘S-<left mouse button>’. https://superuser.com/questions/521223/shift-click-to-extend-marked-region
;(setq mouse-drag-and-drop-region 'control) ;                      Drag and drop text with the mouse. ‘C-<drop>’ copies it. https://emacs.stackexchange.com/questions/21210/drag-selected-text-with-the-mouse
(setq save-interprogram-paste-before-kill t) ;                     Save existing clipboard text into the kill ring before replacing it. https://www.gnu.org/software/emacs/manual/html_node/emacs/Clipboard.html
(setq kill-do-not-save-duplicates t) ;                             Don’t keep duplicates in the kill ring.
(dolist (prop '(face ;                                             Don’t copy the face property (i.e. colour, size, etc.). https://stackoverflow.com/questions/22024765/how-to-copy-paste-without-source-font-lock-in-emacs
                display)) ;                                        This is to avoid copying highlight-indentation-guides’ indicators.
  (add-to-list 'yank-excluded-properties prop))
(setq set-mark-command-repeat-pop t) ;                             Just repeat ‘C-SPC’ to pop the mark again after popping it once with ‘C-u C-SPC’.

;; TODO: Readjust the overwriting after a ‘yank-pop’.
;; https://stackoverflow.com/questions/22074118/how-to-overwrite-text-by-yank-in-emacs/24764223#24764223
(define-advice yank (:before (&optional arg) maybe-overwriting)
  "Overwrite on yanking when ‘overwrite-mode’ is on."
  (when (bound-and-true-p overwrite-mode)
    (delete-char (length (current-kill 0)))))

(defun my-adjust-region-text (beg end)
  "In the region, replace
 - the full path to the user’s home directory with “~”;
 - ‘./PBT/’ with the full path to the directory ‘PBT’ in my
   external drive;
 - URLs to GNU manual nodes with the Elisp code for opening
   ‘info’ on those nodes;
 - full YouTube URLs with the youtu.be version."
  (interactive "*r")
  (let ((home-path (expand-file-name "~"))
        (full-ehd-path (concat "/run/media/" user-login-name "/III-TOSHI/PBT/"))
        (gnu-info-url "\\(?:https?://\\)?\\(?:www\\.\\)?gnu\\.org/software/\\([^/]+\\)/manual/html_node\\(?:/\\([^/[:space:]]+\\)\\)?/\\(.+\\)\\.html\\(?:#[^[:space:]]+\\)?")
        (youtube-url "\\(?:https?://\\)?\\(?:www\\.\\)?youtube\\.com/watch\\?v=")
        (counter 0))
    (save-excursion
      ;; We can’t just use ‘end’ as the ‘bound’ argument of ‘re-search-forward’ because ‘end’ is fixed at the time this command is invoked while any iteration of the ‘while’ loops can change the number of characters in the region and thus move the text at ‘region-end’ away from the the ‘end’ position. Instead, we set the accessible portion of the buffer to the initial region and let ‘re-search-forward’ run free within it. (An alternative would be to use ‘re-search-forward’ or ‘replace-regexp-in-region’ with ‘region-end’ but then we wouldn’t be able to specify the boundary as argument of this command.)
      (save-restriction
        (narrow-to-region beg end)
        ;; Home directory path, ‘PBT’ path and YouTube URLs
        (seq-mapn
         (lambda (regexp replacement)
           (goto-char (point-min))
           (while (re-search-forward regexp nil 'noerror)
             (replace-match replacement 'fixedcase)
             (setq counter (1+ counter))))
         ;; List of regexps
         (list home-path "\\./PBT/" youtube-url)
         ;; List of replacements
         (list "~" full-ehd-path "https://youtu.be/"))
        ;; GNU manual node URLs
        (goto-char (point-min))
        (while (re-search-forward gnu-info-url nil 'noerror)
          (replace-match
           ;; We can’t put the following ‘concat’ form in the list of replacements of the ‘seqn-mapn’ form because the elements of that list are evaluated before calling ‘re-search-forward’, so before ‘match-string’ can access the match data.
           (concat "(info \"(" (or (match-string 2) (match-string 1)) ") " ; Name of the manual. The first subexpression holds the name of the manual if it comes after ‘software/’, as in ‘gnu.org/software/gawk/manual/html_node/Expressions.html’; in this case ‘(match-string 2)’ returns nil. The second subexpression holds the name of the manual if it comes after ‘html_node/’, as in ‘gnu.org/software/emacs/manual/html_node/elisp/Replacing-Match.html’.
                   (string-replace "-" " " (match-string 3)) "\")")) ;       Name of the node.
          (setq counter (1+ counter)))))
    (message "Replaced %d occurrence%s" counter (if (= counter 1) "" "s"))))
(global-set-key (kbd "C-M-y") #'my-adjust-region-text)

;; REVIEW: Why doesn’t ‘yank’ overwrite the active region by itself?
;; Adapted from https://emacs.stackexchange.com/questions/19461/insert-lines-when-yanking-rectangle-rather-than-inserting-among-following-lines
(defun my-insert-rectangle-push-lines ()
  "Yank a rectangle as if it was an ordinary kill."
  (interactive "*")
  (when (and (use-region-p) delete-selection-mode)
    (delete-region (region-beginning) (region-end))
    (undo-boundary)) ; makes Emacs undo the yank and the deletion of the region text in two separate steps, like for other commands that overwrite the region when ‘delete-selection-mode’ is enabled.
  (save-restriction
    (narrow-to-region (point) (point))
    (yank)))
(global-set-key (kbd "C-x r C-y") #'my-insert-rectangle-push-lines)

;; Reverse yank-pop
;; https://www.emacswiki.org/emacs/KillingAndYanking
(defun yank-pop-forwards (&optional arg)
  "Yank a later kill.
This rotates the ‘kill-ring’ ARG positions towards the most
recent kill. ARG defaults to 1."
  (interactive "*p")
  (yank-pop (- arg)))
(global-set-key "\S-\M-y" #'yank-pop-forwards)

;; Kill ring visualizer
;; https://github.com/browse-kill-ring/browse-kill-ring
;; See also https://www.emacswiki.org/emacs/BrowseKillRing
(setq browse-kill-ring-separator "────────")
(setq browse-kill-ring-show-preview nil)
(setq browse-kill-ring-highlight-current-entry t)
(setq browse-kill-ring-highlight-inserted-item nil)

;; Select or copy “text objects” more quickly
;; It’s a bit redundant with expand-region, but it has some useful features that are unique to it.
;; https://github.com/leoliu/easy-kill
(global-set-key (kbd "C-M-SPC") #'easy-mark) ; I rebind ‘C-M-SPC’ instead of remapping ‘mark-sexp’ as is suggested in the readme, so that ‘mark-sexp’ stays bound to ‘C-M-@’.
(global-set-key (kbd "C-c e") #'easy-kill) ;   Again, I don’t use the suggested remapping so that ‘M-w’ remains available for doing tricks with the mark and point.

;; expand-region
;; REVIEW: I had a note suggesting to load this before multiple-cursors. Is it really necessary?
;; It’s a bit redundant with easy-kill, but it’s the only one that works with multiple-cursors.
;; https://github.com/magnars/expand-region.el
(global-set-key (kbd "C-=") #'er/expand-region)

;; Multiple cursors
;; https://github.com/magnars/multiple-cursors.el
;; https://stackoverflow.com/questions/761706/in-emacs-edit-multiple-lines-at-once#comment66825532_15072957
;; TODO: Enable Company completion while using multiple cursors.

(setq mc/match-cursor-style nil) ; Bar fake cursors are barely visible. This makes multiple-cursors use box fake cursors.
(setq mc/list-file (expand-file-name "mc-lists.el" user-emacs-directory)) ; Unhide it (the default is ‘.mc-lists.el’).

(global-set-key (kbd "C->") #'mc/mark-next-like-this)
(global-set-key (kbd "C-<") #'mc/mark-previous-like-this)
(global-set-key (kbd "C-M->") #'mc/skip-to-next-like-this)
(global-set-key (kbd "C-M-<") #'mc/skip-to-previous-like-this)
(global-set-key (kbd "C-c C->") #'mc/mark-all-like-this)
(global-set-key (kbd "C-c C-<") #'mc/mark-more-like-this-extended)
(global-set-key (kbd "C-M-<mouse-1>") #'mc/add-cursor-on-click)

;; Invoke ‘string-rectangle’ when you type in a character or yank while in ‘rectangle-mark-mode’. This way you don’t need to type ‘C-t’ to start writing on each line of the rectangle.
;; Based on Stefan Monnier’s answer on https://www.reddit.com/r/emacs/comments/8s617r/avoid_having_to_invoke_stringrectangle_to_insert/e0zfstz/
;; also on https://emacs.stackexchange.com/questions/39414/immediately-invoke-string-rectangle-upon-rectangle-mark-mode-selection
(defun string-rectangle-with-initial (char)
  "Call ‘string-rectangle’ before processing CHAR."
  (interactive (list last-input-event))
  (barf-if-buffer-read-only) ; Emulate ‘(interactive "*")’.
  (unless executing-kbd-macro
    (push char unread-command-events))
  (call-interactively #'string-rectangle))
(with-eval-after-load 'rect
  (define-key rectangle-mark-mode-map [remap self-insert-command] #'string-rectangle-with-initial)
  (define-key rectangle-mark-mode-map [remap yank] #'string-rectangle-with-initial)
  (define-key rectangle-mark-mode-map [remap quoted-insert] #'string-rectangle-with-initial))

;; Reluctant kill commands

(defun reluctant-kill (&optional arg)
  "Kill characters ‘reluctant-forward’ would skip."
  (interactive "*p")
  (kill-region (point) (progn (reluctant-forward arg) (point))))
(global-set-key (kbd "M-<delete>") #'reluctant-kill)

(defun reluctant-kill-backward (&optional arg)
  "Kill characters ‘reluctant-backward’ would skip."
  (interactive "*p")
  (reluctant-kill (- arg)))
(global-set-key (kbd "M-<backspace>") #'reluctant-kill-backward)

;; ‘kill-whole-line’ derivatives

(defun kill-whole-lines-maybe-indent (&optional arg)
  "Kill the current line. If there’s an active region, also kill the
lines that make part of the region.
With prefix argument ARG, kill abs(ARG) - 1 additional lines.
If ARG is negative, kill backward, also killing the preceding newline.
If ARG is zero, kill the current line but exclude the trailing newline.
Reindent the line where the point lands if RET would do so."
  (interactive "*p")
  (when (use-region-p)
    (let ((beg (save-excursion
                 (goto-char (region-beginning))
                 (line-beginning-position)))
          (end (save-excursion
                 (goto-char (region-end))
                 (line-end-position))))
      (kill-region beg end)
      (append-next-kill)))
  (kill-whole-line arg)
  (when (or (eq (key-binding (kbd "RET")) 'reindent-then-newline-and-indent)
            electric-indent-mode)
    (indent-according-to-mode)))
(global-set-key (kbd "<C-S-delete>") #'kill-whole-lines-maybe-indent) ; replaces ‘kill-whole-line’.

(defun reverse-kill-whole-lines-maybe-indent (&optional arg)
  "Like ‘kill-whole-lines-maybe-indent’, except this command goes
backwards with positive ARG, and vice versa."
  (interactive "*p")
  (kill-whole-lines-maybe-indent (- arg)))
(global-set-key (kbd "<C-S-backspace>") #'reverse-kill-whole-lines-maybe-indent)



;;;; Scrolling

;; FIXME (bug?): Don’t quit query replacement (‘query-replace’, ‘query-replace-regexp’) on invocation of commands with non-nil property ‘isearch-scroll’.
;;   As it stands, query replacement doesn’t make any use of the property ‘isearch-scroll’ (the treatment of ‘recenter-top-bottom’ is hard-coded).
;;   See ‘replace.el’, in particular the variable ‘query-replace-map’ and the function ‘perform-replace’ (search “recenter”).

;; Don’t move the point while scrolling (unless it reaches the top or bottom margins)
;; REVIEW: With ‘scroll-preserve-screen-position’ set to nil, when the point reaches the top (bottom) margin and is pushed downwards (upwards) on scrolling further forward (backward), it’s moved to the beginning of the line below (above) even if there’s room enough to place it at its previous horizontal position. Make it retain its horizontal position instead, even after crossing short lines where it’s forced to the left.
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Scrolling.html
(setq scroll-preserve-screen-position nil)

;; Move the point to the first (last) line of the buffer when it’s not possible to scroll backward (forward) any more while keeping the cursor at the same screen line
;; REVIEW (bug?): Make it influence mouse scrolling as well.
(setq scroll-error-top-bottom t)

;; Settings that change based on some conditions
;; https://www.emacswiki.org/emacs/SmoothScrolling
;; https://gist.github.com/dragonwasrobot/3716926
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Auto-Scrolling.html
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Horizontal-Scrolling.html

(setq scroll-margin 1) ;                                          Number of lines to keep between the cursor and the horizontal edges of the window while moving vertically.
(defun adjust-scroll-margin ()
  (setq-local scroll-margin
              (if (bound-and-true-p follow-mode) ;                When Follow mode is active, having the ‘scroll-margin’ set to 0 during searches avoids breaking the sequential arrangement of text between windows when the match is close to the window border.
                  0
                (if (bound-and-true-p buffer-face-mode) 2 1)))) ; Buffers that use Inconsolata need a ‘scroll-margin’ of 2 to actually keep one line between the cursor and the edge of the window.
(add-hook 'follow-mode-hook #'adjust-scroll-margin)
(add-hook 'buffer-face-mode-hook #'adjust-scroll-margin)

(defun set-up-scroll-never-recenter ()
  "Set up scrolling so that the point is never recentered.
This function is intended to be called from a hook, to override
the setup applied by ‘set-up-scroll-recenter’."
  (setq scroll-conservatively 101) ; Never recenter the point while moving vertically.
  (setq hscroll-step 1) ;            Never recenter the point while moving horizontally. When ‘hscroll-margin’ is set to 0, smoother automatic scrolling can be achieved by using a floating-point ‘hscroll-step’: if it’s set to a small enough value, the cursor stays right at the edge of the window while writing; if, instead, an integer value N is used, the window is always scrolled N columns back when the cursor reaches its right edge (but only if the point is at the end of the line). REVIEW: 0.004 or smaller values cause the cursor to disappear beyond the right edge of the window in some cases (especially with small characters (as after repeated calls to ‘text-scale-adjust’), in narrow windows or when the point is not at the end of the line). On the other hand, values greater of 0.004 fail at smoothing automatic scrolling in wide windows.
  (setq hscroll-margin 1)) ;         Number of lines to keep between the cursor and the vertical edges of the window while moving horizontally. FIXME (bug?): The cursor is allowed to move beyond the ‘hscroll-margin’ and then brought back to it when it reaches the right edge of the window, resulting in a jerky scrolling. See also the note to “setq hscroll-step”.
(set-up-scroll-never-recenter)

(defun set-up-scroll-recenter ()
  "Set up scrolling so that the point is recentered on moving off-screen.
This function is intended to be called from a hook, to override
the setup applied by ‘set-up-scroll-never-recenter’."
  ;; All variables are set to their default value here.
  (setq scroll-conservatively 0)
  (setq hscroll-step 0)
  (setq hscroll-margin 5))

;; Cf. https://emacs.stackexchange.com/questions/10898/query-replace-leaves-potential-match-to-be-replaced-at-bottom-of-window/10902#10902
;;     https://github.com/emacscollective/auto-compile/blob/c46fb16c919d1f821cd69a43cc6e396757c51b2f/auto-compile.el#L597
;;     https://www.gnu.org/software/emacs/manual/html_node/elisp/Porting-old-advice.html
(defun recentering-on-jump (orig-fn &rest args)
  "Recenter the point when the advised function moves it
out of view."
  (let ((scroll-conservatively 0)
        (hscroll-step 0)
        (hscroll-margin 5))
    (apply orig-fn args)))

(defun my-recenter-after-jump (window _window-start-after)
  "Recenter the point after a non-scroll command brings it out of view.
This function is meant to be called from the hook
‘window-scroll-functions’."
  (with-selected-window window
    (unless (or (> 0.001 (float-time (time-subtract (current-time) buffer-display-time))) ; Don’t run this function after a change of buffer. Non-nil here means the selected window displayed the current buffer less than a thousandth of a second ago, so it was most likely a change of buffer that triggered ‘window-scroll-functions’, not a scroll. See also https://emacs.stackexchange.com/questions/63558/how-to-distinguish-a-scroll-from-a-change-of-buffer-in-a-function-called-from-w
                ;; Don’t recenter for these
                ;;   - major modes
                (memq major-mode '(help-mode ;   ‘my-recenter-after-jump’ breaks scrolling with SPC/<backspace> in Help mode and Info mode because they don’t obey ‘scroll-preserve-screen-position’ and ‘scroll-error-top-bottom’.
                                   Info-mode))
                ;;   - minor modes
                isearch-mode ;                   For Isearch, change the value of ‘scroll-conservatively’ and ‘hscroll-step’ in ‘isearch-update-post-hook’ and ‘isearch-mode-end-hook’. Doing so allows recentering the point even after a purely horizontal scroll.
                (bound-and-true-p view-mode) ;   ‘my-recenter-after-jump’ breaks scrolling with SPC/<backspace> in View mode because View mode doesn’t obey ‘scroll-preserve-screen-position’ and ‘scroll-error-top-bottom’.
                (bound-and-true-p follow-mode)
                ;;   - commands
                ;;     To prevent a command from triggering ‘my-recenter-after-jump’, give it the property ‘my-do-not-recenter’ with a non-nil value.
                (and (symbolp last-command) ;    Don’t check the properties of anonymous commands (they can’t be t anyway).
                     (or (get last-command 'scroll-command)
                         (get last-command 'my-do-not-recenter))))
      ;; We need to know the values of some position both before and after the jump. As far as I know ‘window-end’ is the only function which allows doing that without too much fuss, thanks to its ‘update’ argument.
      (let* ((bottom-line-before (line-number-at-pos (window-end)))
             (bottom-line-after (line-number-at-pos (window-end nil 'update))) ; NB: ‘window-start’ doesn’t take the ‘update’ argument.
             (vertical-displacement (- bottom-line-after bottom-line-before)))
        (unless (> 2 (abs vertical-displacement)) ; The purpose of having the first ‘unless’ separate from this one is to call ‘line-number-at-pos’ only when it’s needed, because it can be slow. See (2 links) https://emacs.stackexchange.com/questions/51648/how-to-detect-the-number-of-lines-scrolled-from-scroll-up-down/51664#51664, https://emacs.stackexchange.com/questions/3821/a-faster-method-to-obtain-line-number-at-pos-in-large-buffers
          (recenter))))))
;; FIXME (bug?): The hook ‘window-scroll-functions’ only gets called after scrolling vertically. I need a hook that’s called after scrolling horizontally to recenter the point after jumps that scroll the window only horizontally; see https://emacs.stackexchange.com/questions/37369/hook-variable-for-horizontal-scrolling
(dolist (cmd '(narrow-to-defun narrow-to-page narrow-to-region
               previous-line next-line ;                                 Don’t let these commands trigger ‘my-recenter-after-jump’. It happens when Emacs has a moment of lag during which the point slips beyond the scroll margin.
               previous-logical-line next-logical-line
               query-replace query-replace-regexp query-replace-strict ; Query replacement is taken care of by the ‘recentering-on-jump’ advice to ‘perform-replace’, which allows recentering the point after a purely horizontal scroll.
               recenter-top-bottom))
  (put cmd 'my-do-not-recenter t))
(add-hook 'window-scroll-functions #'my-recenter-after-jump)

(defun bringing-continuation-lines-into-view (&rest _args)
  "Bring into view as many continuation lines of the current line
as there is room for in the window."
  (when (or (not truncate-lines) visual-line-mode)
    (redisplay) ; Update the values of ‘pos-visible-in-window-p’, ‘posn-at-point’ and ‘window-end’. Note that even ‘(window-end nil 'update)’ returns an outdated value if called before ‘redisplay’. REVIEW: ‘redisplay’ makes the screen flicker. Is there another way to update these functions? Apparently no: “Nothing except redisplay automatically changes the window-start position; if you move point, do not expect the window-start position to change in response until after the next redisplay.” https://www.gnu.org/software/emacs/manual/html_node/elisp/Window-Start-and-End.html
    (unless (pos-visible-in-window-p (line-end-position))
      (let ((screen-line-minus-one (cdr (nth 6 (posn-at-point)))) ; I’ve called it ‘screen-line-minus-one’ because ‘posn-at-point’ counts from zero, so the value it returns is one less than the actual line number.
            (hidden-cont-lines (1+ (count-screen-lines (window-end) (line-end-position)))))
        (scroll-up (min (- screen-line-minus-one scroll-margin) hidden-cont-lines))))))
(advice-add 'next-logical-line :after #'bringing-continuation-lines-into-view)

;;; Scrolling with the touchpad

;; https://www.emacswiki.org/emacs/SmoothScrolling
;; https://gist.github.com/dragonwasrobot/3716926

(setq mouse-wheel-follow-mouse t) ;                   Scroll the window under the mouse pointer instead of the active one.
(setq mouse-wheel-scroll-amount '(1 ((shift) . 5))) ; The first number specifies how many lines to scroll by when no modifier key is pressed, the second one how many lines to scroll by while holding down the shift key.
(setq mouse-wheel-progressive-speed t) ;              Make the scroll speed proportional to the mouse wheel speed.
(setq mouse-wheel-tilt-scroll t) ;                    Enable horizontal scrolling with the mouse.
(setq mouse-wheel-flip-direction t) ;                 “Natural” horizontal scrolling.

;; Fix scrolling in unfocused frames
;; REVIEW (bug?): Without this fix, when I try to scroll in an unfocused frame using the touchpad gesture, Emacs just says “<s-mouse-4> is undefined” (or “<s-mouse-5>…”, or “<s-double-mouse…” or “<s-triple-mouse…”).
;;   Cf. commit 585be2b. What fixed it that time?
;; Scroll forward
(define-key key-translation-map (kbd "<s-mouse-5>")        (kbd "<mouse-5>"))
(define-key key-translation-map (kbd "<s-double-mouse-5>") (kbd "<mouse-5>"))
(define-key key-translation-map (kbd "<s-triple-mouse-5>") (kbd "<mouse-5>"))
;; Scroll backward
(define-key key-translation-map (kbd "<s-mouse-4>")        (kbd "<mouse-4>"))
(define-key key-translation-map (kbd "<s-double-mouse-4>") (kbd "<mouse-4>"))
(define-key key-translation-map (kbd "<s-triple-mouse-4>") (kbd "<mouse-4>"))

;;; Scrolling with the keyboard

(defun anchor-point-scroll-backward-line (&optional arg)
  "Scroll backward ARG lines, or one line if called without ARG.
This command always scrolls without moving the point (except at
the bottom margin of the window), disregarding the value of
‘scroll-preserve-screen-position’."
  (interactive "p")
  (let ((scroll-preserve-screen-position nil))
    (scroll-down arg)))
(put 'anchor-point-scroll-backward-line 'scroll-command t)
(global-set-key (kbd "C-S-y") #'anchor-point-scroll-backward-line) ; The key binding refers to Vim’s ‘C-y’.
(global-set-key (kbd "M-P") #'anchor-point-scroll-backward-line)

(defun anchor-point-scroll-forward-line (&optional arg)
  "Scroll forward ARG lines, or one line if called without ARG.
This command always scrolls without moving the point (except at
the top margin of the window), disregarding the value of
‘scroll-preserve-screen-position’."
  (interactive "p")
  (let ((scroll-preserve-screen-position nil))
    (scroll-up arg)))
(put 'anchor-point-scroll-forward-line 'scroll-command t)
(global-set-key (kbd "C-S-e") #'anchor-point-scroll-forward-line) ; The key binding refers to Vim’s ‘C-e’.
(global-set-key (kbd "M-N") #'anchor-point-scroll-forward-line)

;; FIXME: The following horizontal scrolling commands don’t always work as expected during a search. I can’t reproduce the issue consistently.
;;   Note: from ‘isearch.el’’s commentary, under “scrolling within Isearch mode”,
;;     “It is unacceptable for the search string to be scrolled out of the current window.  If a command attempts this, we scroll the text back again.”
;;     “Horizontal scrolling commands are currently not catered for.”
;;   See https://lists.gnu.org/archive/html/help-gnu-emacs/2019-11/msg00049.html

;; FIXME: (Same as for ‘scroll-eol’.) When the point ends up outside the window, the cursor is displayed at the left edge of the window even if the point is not actually there.
(defun my-scroll-left (&optional arg)
  "Same as ‘scroll-left’ except
 - argument SET-MINIMUM is nil, so no lower bound for automatic
   scrolling is set;
 - the default width that is scrolled corresponds to
     ‘window-width’ − ‘hscroll-margin’ − 1."
  (interactive "P")
  (scroll-left (or arg (- (window-width) hscroll-margin 1))))
(put 'my-scroll-left 'isearch-scroll t)
(global-set-key (kbd "C-x <") #'my-scroll-left) ; replaces ‘scroll-left’.

(defun my-scroll-right (&optional arg)
  "Same as ‘scroll-right’ except
 - argument SET-MINIMUM is nil, so no lower bound for automatic
   scrolling is set;
 - the default width that is scrolled corresponds to
     ‘window-width’ − ‘hscroll-margin’ − 1."
  (interactive "P")
  (scroll-right (or arg (- (window-width) hscroll-margin 1))))
(put 'my-scroll-right 'isearch-scroll t)
(global-set-key (kbd "C-x >") #'my-scroll-right) ; replaces ‘scroll-right’.

(defun my-scroll-bol (&optional arg)
  "Scroll right to the beginning of the line.
With argument ARG, leave ARG columns off the left of the window."
  (interactive "P")
  (set-window-hscroll (selected-window) (or arg 0)))
(put 'my-scroll-bol 'isearch-scroll t)
(global-set-key (kbd "C-S-k") #'my-scroll-bol)

;; FIXME: Same as for ‘my-scroll-left’.
(defun my-scroll-eol (&optional arg)
  "Place the end of the line at the right edge of the window.
With argument ARG, leave ARG columns off the right of the
window."
  (interactive "P")
  (let ((line-len (save-excursion (end-of-line) (current-column)))
        (win-width (window-width))
        (narg (or arg 0)))
    (set-window-hscroll (selected-window) (- line-len win-width narg))))
(put 'my-scroll-eol 'isearch-scroll t)
(global-set-key (kbd "C-Ç") #'my-scroll-eol) ; REVIEW: ‘C-Ç’ is ‘C-S-ò’, but binding it using ‘(kbd "C-S-ò")’ doesn’t work.

;; Adapted from https://stackoverflow.com/questions/1249497/command-to-center-screen-horizontally-around-cursor-on-emacs
;; TODO: Handle folded lines.
(defun my-horizontal-recenter (&optional arg)
  "Make the point horizontally centered in the window.
If empty space beyond the end of the line would become visible,
place the end of the line at the right edge of the window
instead.
With argument ARG, scroll that many columns further left."
  (interactive "P")
  (let* ((mid (/ (window-width) 2))
         (line-len (save-excursion (end-of-line) (current-column)))
         (cur (current-column))
         (target-hscroll (- cur (- mid (or arg 0))))
         (max-hscroll (- line-len (window-width))))
    (if (< 0 target-hscroll)
        (if (< target-hscroll max-hscroll)
            (set-window-hscroll (selected-window) target-hscroll)
          (set-window-hscroll (selected-window) max-hscroll))
      (set-window-hscroll (selected-window) 0))))
(put 'my-horizontal-recenter 'isearch-scroll t)

;; REVIEW: Changing font size with ‘text-scale-adjust’ messes it up (it moves the view to the wrong places).
;; Adapted from ‘recenter-top-bottom’.
(defun cycle-horizontal-view-pos (&optional arg)
  "If called without argument, make the point horizontally
centered in the window. If empty space beyond the end of the line
would become visible, place the end of the line at the right edge
of the window instead. Successive calls bring into view,
ciclically, the beginning of the line, the point (centered in the
window) and the end of the line.

With argument ARG, if ARG is positive, place the left edge of the
window at the ARG-th column; if ARG is zero or negative, place
the right edge of the window -ARG columns before the end of the
current line (provided it is at least -ARG columns longer than
the window)."
  (interactive "P")
  (make-local-variable 'horiz-recenter-last-op-memo)
  (if arg
      (cond
       ((> arg 0)
        (my-scroll-bol arg))
       ((<= arg 0)
        (my-scroll-eol (- arg))))
    (let ((horiz-recenter-positions '(point bol eol))) ; This list defines the order in which positions are cycled.
      (setq horiz-recenter-last-op-memo
            (if (eq this-command last-command)
                (car (or (cdr (memq horiz-recenter-last-op-memo horiz-recenter-positions))
                         horiz-recenter-positions))
              (car horiz-recenter-positions)))
      (cond ((eq horiz-recenter-last-op-memo 'point)
             (with-temp-message "Point"
               (my-horizontal-recenter)
               (sit-for 2))) ; 2 is the default value of the variable ‘minibuffer-message-timeout’. During searches the variable is set to nil, however, so it wouldn’t work here.
            ((eq horiz-recenter-last-op-memo 'bol)
             (with-temp-message "Beginning of line"
               (my-scroll-bol)
               (sit-for 2)))
            ((eq horiz-recenter-last-op-memo 'eol)
             (with-temp-message "End of line"
               (my-scroll-eol)
               (sit-for 2)))))))
(put 'cycle-horizontal-view-pos 'isearch-scroll t)
(global-set-key (kbd "C-S-l") #'cycle-horizontal-view-pos)



;;;; Movement

;; Commands in the section “Scrolling” don’t move the point unless ‘scroll-preserve-screen-position’ is non-nil and non-t. Those in this section work essentially by moving the point around.
;; Movement commands typically should have the ‘^’ code in their ‘interactive’ form (see the documentation of the function ‘handle-shift-selection’).

;; Avy
;; https://github.com/abo-abo/avy
;; See also https://karthinks.com/software/avy-can-do-anything/

(global-set-key (kbd "C-S-r") #'avy-goto-char-2)
(global-set-key (kbd "C-S-s") #'avy-goto-char-2)

;; Point detour
;; Adapted from https://github.com/ska2342/detour
;; see also https://emacs.stackexchange.com/questions/3421/how-to-switch-back-and-forth-between-two-locations-in-a-buffer/41502#41502

(defvar detour-register ?↺
  "Register used to store the cursor position.")

(defun point-detour (&optional arg)
  "Jump to the marker stored in ‘detour-register’. Save the
current location in its place.
If called with argument or if ‘detour-register’ is empty, just
save the current location in the register without moving the
point."
  (interactive "^P")
  (if (or arg (null (get-register detour-register)))
      (progn
        (point-to-register detour-register)
        (message "Detour location saved"))
    (let ((tmp (point-marker)))
      (jump-to-register detour-register)
      (set-register detour-register tmp))))

(global-set-key (kbd "C-.") #'point-detour)

;; Simpler and faster alternative to ‘previous-line’ and ‘next-line’
;; It’s like ‘forward-line’ except that it throws an error when it reaches the beginning or the end of the buffer. Unlike ‘previous-line’ and ‘next-line’ it doesn’t care about visual lines and the ‘goal-column’ (it always moves to the beginning of the line).
(defun my-line-move (arg)
  "Move ARG lines forward if ARG is positive, backward if it is
negative. Signal an error if such motion is impossible."
  (cond
   ((<= arg 0)
    (unless (zerop (forward-line arg))
      (signal 'beginning-of-buffer nil)))
   ((< 0 arg)
    (unless (and (zerop (forward-line arg))
                 (char-equal (char-before) (string-to-char "\n"))) ; Handle ‘forward-line’’s exception, where it moves to the end of the last line if it is non-empty, not across it, but it returns 0 anyway.
      (signal 'end-of-buffer nil)))))

;; Reluctant motion commands
;; Unlike ‘forward-word’ or ‘backward-word’ these stop on symbols floating in whitespace.
;; See https://stackoverflow.com/questions/18675201/alternative-to-forward-word-backward-word-to-include-symbols-e-g
;;     https://emacs.stackexchange.com/questions/63012/how-to-change-emacs-word-movement-behaviour

(defun reluctant-forward (&optional arg)
  "Move past all characters with the same syntax class, treating
the newline as having a class of its own.
With prefix argument ARG, do it ARG times if positive, or move
backwards ARG times if negative."
  (interactive "^p")
  (while (< arg 0)
    (cond
     ((= (point) (point-min))
      (signal 'beginning-of-buffer nil))
     ((/= 0 (skip-chars-backward " \t")))
     ((/= 0 (skip-chars-backward "\n")))
     ((let ((regexp (format "\\S%s" (char-to-string (char-syntax (char-before))))))
        (goto-char (if (re-search-backward regexp nil t)
                       (match-end 0)
                     (point-min))))))
    (setq arg (1+ arg)))
  (while (< 0 arg)
    (cond
     ((= (point) (point-max))
      (signal 'end-of-buffer nil))
     ((/= 0 (skip-chars-forward " \t")))
     ((/= 0 (skip-chars-forward "\n")))
     ((let ((regexp (format "\\S%s" (char-to-string (char-syntax (char-after))))))
        (goto-char (if (re-search-forward regexp nil t)
                       (match-beginning 0)
                     (point-max))))))
    (setq arg (1- arg))))
(global-set-key (kbd "M-<right>") #'reluctant-forward) ; replaces ‘right-word’.

(defun reluctant-backward (&optional arg)
  "Move backwards before all characters with the same syntax
class, treating the newline as having a class of its own.
With prefix argument ARG, do it ARG times if positive, or move
forwards ARG times if negative."
  (interactive "^p")
  (reluctant-forward (- arg)))
(global-set-key (kbd "M-<left>") #'reluctant-backward) ; replaces ‘left-word’.

;; Jump between parens (e.g. groups in TeX commands)
;; REVIEW: They work well in ‘latex-mode’ but not in ‘emacs-lisp-mode’ (they don’t stay at the same level of nesting).

(defun jump-into-previous-parens ()
  "Move to the previous closing paren at the same level of
nesting."
  (interactive "^")
  (with-demoted-errors "‘backward-up-list’ error: %S"
    (backward-up-list))
  (down-list -1))

(defun jump-into-next-parens ()
  "Move to the next opening paren at the same level of nesting."
  (interactive "^")
  (with-demoted-errors "‘backward-up-list’ error: %S"
    (backward-up-list -1))
  (down-list))

(defun move-to-fill-column ()
  "Jump to the ‘fill-column’."
  (interactive "^")
  (move-to-column fill-column))
(global-set-key (kbd "M-g f") #'move-to-fill-column)

(defun move-to-column-forcefully (column)
  "Move point to column COLUMN in the current line."
  (interactive "^NMove to column: ")
  (move-to-column column t))
(global-set-key (kbd "M-g <tab>") #'move-to-column-forcefully) ; replaces the weak ‘move-to-column’.
(global-set-key (kbd "M-g TAB") #'move-to-column-forcefully) ; Otherwise Emacs reports ‘move-to-column’ as still bound to ‘M-g TAB’. Apparently Emacs considers ‘M-g <tab>’ and ‘M-g TAB’ as two different key combinations. REVIEW: Maybe which one of them is actually recognized is machine-dependent.

;; REVIEW: It doesn’t scroll by the amount specified in ‘mouse-wheel-scroll-amount’ when the shift key is held down (not even if it’s bound to ‘<C-S-mouse-4>’ and ‘<C-S-mouse-5>’).
(defun mwheel-scroll-fixed-screen-line ()
  "Scroll by keeping the cursor in the current screen line and
moving the text underneath it.
This command disregards the value of
‘scroll-preserve-screen-position’."
  (interactive)
  (let ((scroll-preserve-screen-position 'always))
    (call-interactively #'mwheel-scroll)))
(put 'mwheel-scroll-fixed-screen-line 'scroll-command t) ; This makes ‘mwheel-scroll-fixed-screen-line’ preserve the point’s horizontal position.
(global-set-key (kbd "<C-mouse-4>") #'mwheel-scroll-fixed-screen-line)
(global-set-key (kbd "<C-mouse-5>") #'mwheel-scroll-fixed-screen-line)

;; Skip continuation lines
(global-set-key (kbd "C-p") #'previous-logical-line) ; replaces ‘previous-line’.
(global-set-key (kbd "C-n") #'next-logical-line) ; replaces ‘next-line’.

(defun same-screen-line-previous-text-line (&optional arg)
  "Move the point up ARG lines, or one line if called without
ARG, keeping the cursor in the same screen line.
This command disregards the value of
‘scroll-preserve-screen-position’."
  (interactive "^p")
  (let ((scroll-preserve-screen-position 'always))
    (scroll-down-command arg))) ; ‘scroll-down-command’ obeys ‘scroll-error-top-bottom’, ‘scroll-down’ doesn’t.
(put 'same-screen-line-previous-text-line 'scroll-command t) ; This makes ‘same-screen-line-previous-text-line’ preserve the point’s horizontal position.
(global-set-key (kbd "M-p") #'same-screen-line-previous-text-line)

(defun same-screen-line-next-text-line (&optional arg)
  "Move the point down ARG lines, or one line if called without
ARG, keeping the cursor in the same screen line.
This command disregards the value of
‘scroll-preserve-screen-position’."
  (interactive "^p")
  (let ((scroll-preserve-screen-position 'always))
    (if (<= (line-number-at-pos) scroll-margin)
        (next-line)
      (scroll-up-command arg))))
(put 'same-screen-line-next-text-line 'scroll-command t)
(global-set-key (kbd "M-n") #'same-screen-line-next-text-line)

(defun same-screen-line-previous-page (&optional arg)
  "Like ‘scroll-down-command’, except this command keeps the
point at a fixed screen line, disregarding the value of
‘scroll-preserve-screen-position’."
  (interactive "^P")
  (let ((scroll-preserve-screen-position 'always))
    (scroll-down-command arg)))
(put 'same-screen-line-previous-page 'scroll-command t) ; This makes ‘same-screen-line-previous-page’ preserve the point’s horizontal position.
(global-set-key [remap scroll-down-command] #'same-screen-line-previous-page)

(defun same-screen-line-next-page (&optional arg)
  "Like ‘scroll-up-command’, except this command keeps the point
at a fixed screen line, disregarding the value of
‘scroll-preserve-screen-position’."
  (interactive "^P")
  (let ((scroll-preserve-screen-position 'always))
    (scroll-up-command arg)))
(put 'same-screen-line-next-page 'scroll-command t)
(global-set-key [remap scroll-up-command] #'same-screen-line-next-page)

;; Additional commands for ‘scroll-all-mode’

(defvar additional-scroll-all-commands
  '(previous-logical-line
    next-logical-line
    same-screen-line-previous-text-line
    same-screen-line-next-text-line
    same-screen-line-previous-page
    same-screen-line-next-page)
  "Additional commands to consider when checking if a motion or
scroll is to be done in all windows.
Only commands that take exactly one argument are supported.
Furthermore, the prefix argument is passed in all windows only
to commands whose interactive spec is a form or whose interactive
code letter is ‘P’ or ‘p’.")

(define-advice scroll-all-check-to-scroll (:after-until () checking-for-additional-commands)
  "Consider the commands listed in ‘additional-scroll-all-commands’
when checking if a motion or scroll is to be done in all windows."
  ;; Since ‘scroll-all-function-all’ uses ‘(funcall func arg)’, this function only supports commands that take exactly one argument.
  ;; If the command’s interactive spec isn’t a form nor the code letter ‘P’ or ‘p’, silently ignore the argument in the other windows.
  ;; If the command doesn’t take exactly one argument, fail silently, moving or scrolling just the current buffer.
  (let* ((scroll-all-command (seq-find (lambda (cmd) (eq cmd this-command))
                                       additional-scroll-all-commands))
         (interactive-spec (cadr (interactive-form scroll-all-command)))
         (interactive-letter (when (stringp interactive-spec)
                               (string-match "[[:alpha:]]" interactive-spec)
                               (match-string 0 interactive-spec))))
    (when scroll-all-command
      (call-interactively
       (lambda (arg)
         (interactive (cond
                       ((string-equal interactive-letter "P")
                        (list current-prefix-arg))
                       ((string-equal interactive-letter "p")
                        (list (prefix-numeric-value current-prefix-arg)))
                       ((listp (eval interactive-spec))
                        (eval interactive-spec))))
         (scroll-all-function-all scroll-all-command arg)
         (setq this-command (intern (concat "scroll-all-" (symbol-name this-command))))))))) ; Give this command some name otherwise it might get called untimely from the ‘post-command-hook’.

;;; Move vertically across blocks of non-whitespace characters
;;; Based on Omar Antolín Camarena’s answer on https://emacs.stackexchange.com/questions/44151/jump-to-non-whitespace-characters-along-a-column
;;; See also https://emacs.stackexchange.com/questions/22091/how-to-jump-up-or-down-to-first-non-whitespace-character-in-same-column
;;;          https://gitlab.com/ideasman42/emacs-spatial-navigate

;; REVIEW (bug?): The ‘defun’ is just for calling the ‘mapc’ form from the ‘after-init-hook’. Delaying the call to ‘mapc’ allows ‘documentation’ to retrieve pre-existing information such as whether the function is advised.
;;   Is it a bug that ‘advice--make-docstring’ overwrites the doc string created with ‘put’ if it comes after it?
(defun block-move--make-doc-strings ()
  "Make the doc strings for ‘block-move-forward’ and
‘block-move-backward’."
  (mapc (lambda (cmd)
          (put cmd 'function-documentation
               (concat
                ;; The ‘(apply 'format …)’ trick comes from https://stackoverflow.com/questions/13041979/how-do-i-format-a-list-of-strings
                (apply 'format "Move %s vertically to the next edge of a block of non-whitespace
characters. If there isn’t any block to jump to, move %1$s vertically
to the %s-most line long enough to keep the point at the
horizontal position it starts from, even if it ends up surrounded by
indentation.

Move %s if ARG is negative. Repeat abs(ARG) times.

“Block of non-whitespace characters” here means a line or a sequence
of lines separated from others by either one that doesn’t reach the
horizontal position of the point or one indented past that horizontal
position."
                       (cond
                        ((eq cmd 'block-move-backward)
                         '("up" "top" "downward"))
                        ((eq cmd 'block-move-forward)
                         '("down" "bottom" "upward"))))
                (documentation cmd))))
        '(block-move-forward
          block-move-backward)))
(add-hook 'after-init-hook #'block-move--make-doc-strings)

(defun block-move-backward (&optional arg)
  (interactive "^p")
  (block-move-forward (- arg)))
(global-set-key (kbd "M-è")    #'block-move-backward)
(global-set-key (kbd "M-<up>") #'block-move-backward)

(defun block-move-forward (arg)
  (interactive "^p")
  (let (destination
        (goal-column (current-column)))
    (dotimes (_ (abs arg))
      (condition-case err
          (save-excursion
            (if (and (block-move--inside-block-p)
                     (block-move--adjacent-line-inside-block-p arg))
                (block-move--move-to-block-edge arg)
              (block-move--jump-to-block arg))
            (setq destination (point)))
        ((beginning-of-buffer end-of-buffer)
         (save-excursion
           (block-move--move-to-column-end arg)
           (setq destination (point)))
         (when (= (point) destination)
           (signal (car err) (cdr err)))))
      (goto-char destination))))
(global-set-key (kbd "M-ò")      #'block-move-forward)
(global-set-key (kbd "M-<down>") #'block-move-forward)

(defun block-move--inside-block-p ()
  "Return t if the point is at the same column as that from which
‘block-move’ was called and inside a block of non-whitespace
characters."
  ;; When there’s trailing horizontal whitespace around the point, this function treats the newline the same as a non-whitespace character (it returns t unless the newline is indented, i.e. unless there’s only horizontal whitespace up to and at the point).
  (and
   (not (and (bolp) (eolp))) ;                     Return nil if the current line is empty.
   (= (current-column) goal-column) ;              Return nil if the current line is too short to reach the column from which ‘block-move’ was called.
   (>= (current-column) (current-indentation)))) ; Return nil if the current line is indented; more precisely, return nil if the current line contains only horizontal whitespace up to and including the column from which ‘block-move’ was called.

(defun block-move--adjacent-line-inside-block-p (arg)
  "Return t if the position directly above the point (if ARG is
negative) or below it (if ARG is positive) is inside a block of
non-whitespace characters."
  (save-excursion
    (block-move--move-one-line arg)
    (block-move--inside-block-p)))

(defun block-move--jump-to-block (arg)
  "Normally this function is called from the upper edge of a block
with negative ARG, or from the lower edge with positive ARG, or
from outside blocks. In those cases it moves the point to the
closest edge of a neighbouring block, backward if ARG is
negative, forward if it is positive."
  (while (progn
           (block-move--move-one-line arg)
           (not (block-move--inside-block-p)))))

(defun block-move--move-to-block-edge (arg)
  "Normally this function is called from within a block.
In that case, it moves the point to the edge of that block,
backward if ARG is negative, forward if it is positive."
  (while (block-move--inside-block-p)
    (block-move--move-one-line arg))
  (block-move--move-one-line (- arg)))

(defun block-move--move-to-column-end (arg)
  "Move to the top-most line (if ARG is negative) or the
bottom-most one (if ARG is positive) that allows placing the
point at the same horizontal position it started from."
  (goto-char (if (< arg 0) (point-min) (point-max)))
  (while (/= (move-to-column goal-column) goal-column)
    (block-move--move-one-line (- arg))))

;; Faster alternative to ‘previous-line’ and ‘next-line’
(defun block-move--move-one-line (arg)
  "Move one line forward if ARG is positive, backward if it is
negative. Signal an error if such motion is not possible.
Stay as close to the ‘goal-column’ as possible."
  (cond
   ((<= arg 0)
    (unless (zerop (forward-line -1))
      (signal 'beginning-of-buffer nil)))
   ((< 0 arg)
    (unless (and (zerop (forward-line 1))
                 (char-equal (char-before) (string-to-char "\n"))) ; Handle ‘forward-line’’s exception, where it moves to the end of the last line if it is non-empty, not across it, but it returns 0 anyway.
      (signal 'end-of-buffer nil))))
  (move-to-column goal-column))

;;; Position history

(add-to-list 'load-path (expand-file-name "marker-stack" my-lib-dir))
(require 'marker-stack)

(dolist (fn '(avy-goto-char-2
              goto-saved-place
              spell-check-backwards
              xref-find-definitions))
  (add-to-list 'marker-stack-functions-every-call fn))

(dolist (fn '(block-move-backward block-move-forward
              undo undo-redo))
  (add-to-list 'marker-stack-functions-first-call fn))

(marker-stack-setup-advices)

(global-set-key (kbd "C-M-,") #'marker-stack-backward-stack-pop)
(global-set-key (kbd "C-M-.") #'marker-stack-forward-stack-pop) ; replaces ‘xref-find-apropos’.
(global-set-key (kbd "C-M-ò") #'marker-stack-push)



;;;; Whitespace and newlines

(setq sentence-end-double-space nil) ; Use just one space between sentences (for, e.g., ‘M-a’ and ‘M-e’). See (3 links) https://www.gnu.org/software/emacs/manual/html_node/emacs/Sentences.html, https://www.gnu.org/software/emacs/manual/html_node/eintr/sentence_002dend.html, https://emacs.stackexchange.com/questions/2766/any-functionality-differences-using-a-two-space-vs-one-space-convention-at-the-e

(defun set-tab-width (width)
  "Prompt for a new value for ‘tab-width’."
  (interactive "NWidth: ")
  (setq tab-width width)) ; Note: ‘tab-width’ automatically becomes buffer-local when set, so ‘setq-local’ is not needed.

(global-set-key (kbd "C-c l") #'indent-relative) ; Align point to symbols in the previous non-blank line.

(defun delete-horizontal-space-backwards ()
  "Delete spaces and tabs before the point."
  (interactive "*")
  (delete-horizontal-space t))
(global-set-key (kbd "C-|") #'delete-horizontal-space-backwards) ; REVIEW: Binding these keys with ‘(kbd "C-S-\\")’ doesn’t work.

;; Adapted from https://stackoverflow.com/questions/24783792/how-to-insert-space-after-cursor
(defun my-insert-space-after-point (&optional arg)
  "Insert ARG spaces after the point."
  (interactive "*p")
  (undo-auto-amalgamate)
  (dotimes (_ arg)
    (save-excursion (insert " "))))
(global-set-key (kbd "S-SPC") #'my-insert-space-after-point)

;; Emulate Vim’s ‘o’ and ‘O’.

(defun spawn-line-below (&optional arg)
  "Insert a new line below the current one.
With prefix argument ARG, insert that many new lines. If ARG is
negative, insert the new lines above the current one.
Delete trailing whitespace in the current line and indent the
line where the point lands if RET would do so."
  (interactive "*p")
  (let ((auto-indentation
         (or (memq (key-binding (kbd "RET")) '(newline-and-indent
                                               reindent-then-newline-and-indent))
             electric-indent-mode)))
    (undo-auto-amalgamate)
    (when (and auto-indentation
               (looking-at "[ \t]*$"))
      (delete-horizontal-space))
    (cond
     ((>= arg 0)
      (move-end-of-line 1)
      (newline arg))
     ((< arg 0)
      (move-beginning-of-line 1)
      (newline (- arg))
      (forward-line arg)))
    (when auto-indentation
      (indent-according-to-mode))))
(global-set-key "\M-o" #'spawn-line-below) ; replaces ‘facemenu-keymap’.

(defun spawn-line-above (&optional arg)
  "Insert a new line above the current one.
With prefix argument ARG, insert that many new lines. If ARG is
negative, insert the new lines below the current one.
Delete trailing whitespace in the current line and indent the
line where the point lands if RET would do so."
  (interactive "*p")
  (spawn-line-below (- arg)))
(global-set-key "\S-\M-o" #'spawn-line-above) ; WARNING: ‘M-O’ may conflict with the arrow keys in a terminal, see (2 links) https://emacs.stackexchange.com/questions/36639/how-to-use-m-o-on-emacs-in-terminal, https://www.reddit.com/r/emacs/comments/2m6nvu/dont_bind_mo_if_you_want_to_use_arrow_keys_in/

(defun make-room-below (&optional arg)
  "Insert a new line below the current one without moving the point.
With prefix argument ARG, insert that many new lines. If ARG is
negative, delete -ARG empty lines below the current one (deletion
stops when there are no more empty lines below the current one)."
  (interactive "*p")
  (save-excursion
    (cond
     ((>= arg 0)
      (move-end-of-line 1)
      (newline arg))
     ((< arg 0)
      (dotimes (_ (abs arg))
        (my-line-move 1)
        (if (and (bolp) (eolp))
            (delete-char -1)
          (user-error "Non-empty line below")))))))
(global-set-key (kbd "S-<return>") #'make-room-below)

(defun make-room-above (&optional arg)
  "Insert a new line above the current one without moving the point.
With prefix argument ARG, insert that many new lines. If ARG is
negative, delete -ARG empty lines above the current one (deletion
stops when there are no more empty lines above the current one)."
  (interactive "*p")
  (cond
   ((>= arg 0)
    (if (bolp) ; If the point is at the beginning of the line when the command is invoked, ‘save-excursion’ keeps it on the current line instead of the one with the text that follows it.
        (newline arg)
      (save-excursion
        (move-beginning-of-line 1)
        (newline arg))))
   ((< arg 0)
    (save-excursion
      (dotimes (_ (abs arg))
        (my-line-move -1)
        (if (and (bolp) (eolp))
            (delete-char 1)
          (user-error "Non-empty line above")))))))
(global-set-key (kbd "C-M-S-o") #'make-room-above)

;; Adapted from http://emacsredux.com/blog/2013/05/30/joining-lines/
;; It’s like Vim’s ‘j’.
(defun top-join-line ()
  "Join the current line with the one below it.
If called with an active region, join all the lines in the
region."
  (interactive "*")
  (if (use-region-p)
      (let ((line-count (1- (count-lines (region-beginning) (region-end)))))
        (save-excursion
          (goto-char (region-beginning))
          (dotimes (_ line-count)
            (delete-indentation t)))
        (deactivate-mark)) ; ensures the mark is deactivated even when the line count is zero and ‘delete-indentation’ never gets called. This way ‘top-join-line’ consistently deactivates the mark, which makes it easier to use in keyboard macros.
    (delete-indentation t)))
(global-set-key (kbd "M-ì") #'top-join-line)

;;; Indentation

;; Indent with spaces instead of tabs
;; https://www.emacswiki.org/emacs/NoTabs
(setq-default indent-tabs-mode nil)

;; Never reindent the current line if not set to do so
;; I prefer to set up automatic indentation by rebinding RET to ‘newline-and-indent’ or ‘reindent-then-newline-and-indent’, and ‘electric-indent-mode’ overrides those settings.
;; https://emacs.stackexchange.com/questions/5939/how-to-disable-auto-indentation-of-new-lines
(electric-indent-mode -1)

;;; Trailing whitespace and blank lines

;(add-hook 'before-save-hook #'delete-trailing-whitespace) ;            Delete trailing whitespace on saving.
;(setq delete-trailing-lines nil) ;                                     Suppress deletion of trailing empty lines when ‘delete-trailing-whitespace’ is called on the entire buffer.
(set-face-attribute 'trailing-whitespace nil :background "gainsboro") ; Colour used for displaying trailing whitespace. https://www.gnu.org/software/emacs/manual/html_node/emacs/Useless-Whitespace.html
(setq-default indicate-empty-lines t) ;                                 Mark unused lines after the end of the buffer with an indicator in the left fringe.

;; See https://www.gnu.org/software/emacs/manual/html_node/emacs/Useless-Whitespace.html
(defun maybe-display-leftover-whitespace ()
  "Visualize spaces and tabs at the end of lines unless the
buffer is read-only or running a shell."
  (setq show-trailing-whitespace
        (and (derived-mode-p 'text-mode 'prog-mode) ;  Heuristic for determining whether the buffer is for text editing. It filters out ‘shell-mode’ and the like.
             (not buffer-read-only)
             (not (bound-and-true-p artist-mode))))) ; Don’t display leftover spaces while ‘artist-mode’ (a minor mode) is on. https://stackoverflow.com/questions/10088168/how-to-check-whether-a-minor-mode-e-g-flymake-mode-is-on
(add-hook 'after-change-major-mode-hook #'maybe-display-leftover-whitespace)

;; Enable or disable highlighting of trailing whitespace whenever a buffer becomes writable or read-only, respectively.
;; See ‘(info "(elisp) Watching Variables")’.
(defun leftover-whitespace-readonly-watcher (_symbol newval op where)
  (when (and (eq op 'set)
             (buffer-live-p where)) ; From https://emacs.stackexchange.com/questions/44759/set-a-global-default-directory/44792#44792
    (if newval
        (setq show-trailing-whitespace nil)
      (let ((buffer-read-only newval)) ; Note: it doesn’t work with ‘symbol’ in place of ‘buffer-read-only’ (and in place of ‘_symbol’ in the argument list).
        (maybe-display-leftover-whitespace)))))
(add-variable-watcher 'buffer-read-only #'leftover-whitespace-readonly-watcher)



;;;; Paired delimiters

;; REVIEW: Fix bug #57057 (‘electric-quote-mode’ merges single quotes)
;;   Upstream it’s fixed in Emacs 29.0.50.
;;   https://debbugs.gnu.org/cgi/bugreport.cgi?bug=57057
;;   https://git.savannah.gnu.org/cgit/emacs.git/commit/?id=41169b55340878120fdf695eb4ac1fcb2679e7b8
(load-file "~/.emacs.d/local/patches/electric/bug57057_keep-consecutive-single-quotes-separate.el")
(setq electric-quote-replace-consecutive nil)

(setq electric-quote-replace-double t) ;    Replace straight double quotes with curly ones in ‘electric-pair-mode’.
(setq electric-quote-context-sensitive t) ; Replace ‘'’ and ‘''’ with an opening quote after a line break, whitespace, opening parenthesis or quote, and leave ‘`’ alone.

(defun delete-surrounding-pair (&optional arg)
  "Delete the innermost enclosing paired delimiters around point.
With argument ARG, delete the ARG-th level of enclosing
parentheses, where 1 is the innermost level."
  (interactive "*p")
  (let ((delete-pair-blink-delay 0))
    (backward-up-list arg 'escape-strings 'no-syntax-crossing)
    (delete-pair)))
(global-set-key (kbd "C-c u") #'delete-surrounding-pair)

(global-set-key (kbd "C-c k") #'blink-matching-open) ; Momentarily echo the the beginning of the sexp before point if it’s above or below the visible portion of the buffer. TODO: Make it work also when the beginning of the sexp is outside the left or right edge of the window.

;;; Automatic pairing

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Matching.html

(defun my-electric-pair-inhibit (char)
  "Predicate to be used as value of
‘electric-pair-inhibit-predicate’."
  ;; Based on ‘electric-pair-conservative-inhibit’.
  ;; Note: only characters whose syntax class is ‘(’, ‘"’ or ‘$’ can trigger the ‘electric-pair-inhibit-predicate’; see the definition of ‘electric-pair-post-self-insert-function’.
  (let ((prec-char (char-before (1- (point))))
        (foll-char (following-char)))
    (or
     ;; Do not pair when the same character is next.
     (eq char foll-char)
     ;; When the same character is preceding, pair dissimilar delimiters (such as parentheses) but do not pair identical delimiters (such as straight quotes).
     (and
      (eq (char-syntax char) ?\")
      (> (point) (1+ (point-min))) ; Handle point at beginning of buffer.
      (eq char prec-char))
     ;; Do not pair right before a word, a symbol or an expression prefix character (e.g. a sigil).
     (and
      (< (point) (point-max)) ;      Handle point at end of buffer.
      (memq (char-syntax foll-char) '(?w ?_ ?')))
     ;; After a word, a symbol or a punctuation character, pair dissimilar delimiters (such as parentheses) but do not pair identical delimiters (such as straight quotes).
     (and
      (eq (char-syntax char) ?\")
      (> (point) (1+ (point-min))) ; Handle point at beginning of buffer.
      (memq (char-syntax prec-char) '(?w ?_ ?.))))))

(setq electric-pair-preserve-balance nil) ;           REVIEW: Is it of any use with my ‘electric-pair-inhibit-predicate’, and ‘electric-pair-skip-self’ explicitly set?
(setq electric-pair-inhibit-predicate #'my-electric-pair-inhibit)
(setq electric-pair-skip-self nil) ;                  Always insert the closing delimiter, don’t skip it if it’s the character ahead of point.
;(setq electric-pair-skip-whitespace nil) ;           Only consider a closing delimiter right next to the point for skipping, don’t traverse whitespace. With ‘electric-pair-skip-self’ set to nil this variable doesn’t have any effect. FIXME (bug?): Why is this variable locally set to ‘chomp’ in ‘emacs-lisp-mode’ buffers?
;(setq electric-pair-delete-adjacent-pairs t) ;       When backspacing over an opening delimiter, also delete the closing one if it’s right next to it. (Default.)
(setq electric-pair-open-newline-between-pairs nil) ; See also its buffer-local settings in major mode hooks.

(electric-pair-mode)

;;; Highlight matching delimiters

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Matching.html
;; http://community.schemewiki.org/?emacs-tutorial

(setq show-paren-delay 0)
(setq show-paren-when-point-inside-paren t)
(setq show-paren-priority -1) ; Prevent Show Paren mode from overriding the background colour of the active region. https://emacs.stackexchange.com/questions/17324/how-to-make-the-region-face-take-priority-over-the-show-paren-mode-face

(show-paren-mode)



;;;; Search and replace

;;; Search

;; REVIEW: Fix bug #52356 (When character folding is enabled, Isearch’s prompt changes briefly at every character typed into the search string)
;;   Upstream it’s fixed in Emacs 29.0.50.
;;   https://debbugs.gnu.org/cgi/bugreport.cgi?bug=52356
(load-file "~/.emacs.d/local/patches/isearch/bug52356_isearch-adjusted-lax.el")

(setq isearch-lax-whitespace nil) ;                  Match exactly as many spaces as there are in the search string. You can toggle the value of this variable by the command ‘isearch-toggle-lax-whitespace’. If non-nil, when you enter a space or spaces in ordinary incremental search, it will match any sequence matched by the regexp defined by the variable ‘search-whitespace-regexp’. See ‘(info "(emacs) Lax Search")’.
(setq search-upper-case nil) ;                       Don’t turn on case sensitivity automatically when there are uppercase letters in the search string. Prevent downcasing of text yanked into the search string. See ‘search-upper-case’’s doc string and https://www.gnu.org/software/emacs/manual/html_node/emacs/Lax-Search.html
;(customize-set-variable 'char-fold-symmetric nil) ; If nil (the default), character folding matches only the “base” character with any of its set. The others match only themselves. See ‘C-h v char-fold-symmetric’ and ‘(info "(emacs) Lax Search")’.
(setq isearch-allow-scroll 'unlimited) ;             Don’t exit Isearch on scrolling. Allow scrolling the current match off-screen. https://emacs.stackexchange.com/questions/10307/how-to-center-the-current-line-vertically-during-isearch
(setq isearch-yank-on-move 'shift) ;                 Shrink or extend the search string using the arrow keys while holding down the shift key.
(setq isearch-lazy-count t) ;                        Show a match counter in the Isearch prompt.
(setq lazy-count-prefix-format nil ;                 Match counter set-up.
      lazy-count-suffix-format " [%s/%s]")
(setq search-ring-max 200 ;                          Default for both of them is 16. https://github.com/astoff/isearch-mb
      regexp-search-ring-max 200)
(setq reb-re-syntax 'string) ;                       This RE Builder syntax requires less escaping than the default ‘read’. See https://masteringemacs.org/article/re-builder-interactive-regexp-builder

(global-set-key (kbd "M-s '") #'isearch-toggle-char-fold) ;  Start an Isearch with character folding enabled (that is, when searching for ‘'’, for example, match also ‘’’). Within an Isearch, ‘isearch-toggle-char-fold’ toggles character folding for that search. See ‘(info "(emacs) Lax Search")’.
(define-key isearch-mode-map (kbd "C-g") #'isearch-cancel) ; Make ‘C-g’ always exit Isearch (even if the initial part of the current search string matches).
(define-key isearch-mode-map (kbd "C-z") #'isearch-abort) ;  ‘isearch-abort’ is the command bound to ‘C-g’ by default.

;; This set-up allows recentering the point after a purely horizontal scroll, which the hook ‘window-scroll-functions’, used to call ‘my-recenter-after-jump’, ignores.
;; FIXME (bug?): It doesn’t work for ‘isearch-{backward,forward}-regexp’. Try searching for “\_<newline-and-indent\_>” in an 80 characters wide window, it fails on a couple of occurrences (tested on a version of this file dated 2020/12/12).
(add-hook 'isearch-update-post-hook #'set-up-scroll-recenter)
(add-hook 'isearch-mode-end-hook #'set-up-scroll-never-recenter)
;; Same for RE Builder.
(advice-add 'reb-prev-match :around #'recentering-on-jump)
(advice-add 'reb-next-match :around #'recentering-on-jump)

;; Use the region as search string
;; Works only in ‘transient-mark-mode’.
;; Adapted from https://stackoverflow.com/questions/202803/searching-for-marked-selected-text-in-emacs/32002122#32002122
(defun jrh-isearch-with-region ()
  "Use region as search string."
  (when (and mark-active transient-mark-mode)
    (let ((region (funcall region-extract-function nil)))
      (goto-char (region-beginning)) ; This prevents going straight to the next match when the function is called.
      (deactivate-mark)
      (isearch-yank-string region))))
(add-hook 'isearch-mode-hook #'jrh-isearch-with-region)

;; Adapted from https://www.emacswiki.org/emacs/ZapToISearch
;; via https://endlessparentheses.com/leave-the-cursor-at-start-of-match-after-isearch.html
(defun isearch-exit-other-end ()
  "Exit Isearch at the other end of the current match."
  ;; It can be done also by hitting ‘C-s’, after searching backward, or ‘C-r’, after searching forward, before ‘RET’.
  ;; Note: The defaults are such that another search in the same direction for the same pattern will not find the same match you found last time. https://emacs.stackexchange.com/questions/32373/go-to-start-of-search-string-after-ret#comment49874_32376
  (interactive)
  (isearch-exit)
  (goto-char (or isearch-other-end (point))))
(define-key isearch-mode-map [(control return)] #'isearch-exit-other-end) ; ‘(kbd "C-RET")’ doesn’t work.

;; Exit Isearch leaving the current match marked as if it was shift-selected
;; FIXME: Rectangular selections mess it up.
;; https://emacs.stackexchange.com/questions/31320/selecting-i-e-as-the-region-the-current-match-in-incremental-search
;; https://emacs.stackexchange.com/questions/44362/make-region-transient-in-elisp-function
;; see ‘M-x find-function handle-shift-selection’.
(defun isearch-exit-mark-match ()
  "Exit Isearch and mark the current match."
  (interactive)
  (isearch-exit)
  (setq-local transient-mark-mode (cons 'only transient-mark-mode))
  (push-mark isearch-other-end nil t))
(define-key isearch-mode-map (kbd "<S-return>") #'isearch-exit-mark-match)

;; Adapted from ‘isearch-yank-kill’ and ‘isearch-yank-string’. Inspired by https://www.reddit.com/r/emacs/comments/8aepnk/yank_text_in_isearch_without_escape_character#t1_dwzib7n
(defun isearch-yank-kill-literally ()
  "Pull string from kill ring into search string without adapting
it to the type of search in progress (cf. ‘isearch-yank-kill’)."
  (interactive)
  (unless isearch-mode (isearch-mode t))
  (let ((string (current-kill 0)))
    ;; Don't move cursor in reverse search.
    (setq isearch-yank-flag t)
    (isearch-process-search-string
     string (mapconcat 'isearch-text-char-description string ""))))
(define-key isearch-mode-map (kbd "C-S-y") #'isearch-yank-kill-literally)

;;; Replace

;; The advice allows recentering the point after a purely horizontal scroll, which the hook ‘window-scroll-functions’, used to call ‘my-recenter-after-jump’, ignores.
;; REVIEW (bug?): There’s no hook that’s run on quitting query replacement, so I can’t use ‘set-up-scroll-recenter’ and ‘set-up-scroll-never-recenter’ as I do for Isearch.
(advice-add 'perform-replace :around #'recentering-on-jump)

;; See https://emacs.stackexchange.com/questions/12780/how-to-perform-case-sensitive-query-replace
(defun query-replace-strict ()
  "Search and replace, matching the text to be replaced
literally."
  (interactive)
  (let ((case-fold-search nil)
        (replace-char-fold nil)
        (replace-lax-whitespace nil))
    (call-interactively (key-binding "\M-%")))) ; Call whatever is bound to ‘M-%’ (it could be ‘query-replace’ or ‘anzu-query-replace’, for example).
(global-set-key (kbd "C-c r") #'query-replace-strict)

;;; Highlight text

;; http://chopmo.dk/2016/10/27/emacs-highlighting-current-word.html
(defun jpt-toggle-mark-word-at-point ()
  "Toggle ‘highlight-symbol-at-point’."
  (interactive)
  (require 'hi-lock)
  (if hi-lock-interactive-patterns
      (unhighlight-regexp (car (car hi-lock-interactive-patterns)))
    (highlight-symbol-at-point)))
(global-set-key (kbd "C-c h") #'jpt-toggle-mark-word-at-point)

;; Highlight region

(let ((colours '("Pink" "Burlywood1" "LightGoldenrod1" "#bbf6bb" "#c2eeff" "#d5d1ff" "#fac2ff")))
  (defvar colour-ring (make-ring (length colours))
    "Colours that ‘highlight-region’ cycles through.")
  (dolist (elem colours) (ring-insert colour-ring elem)))

(defun highlight-region (beg end &optional arg)
  "Highlight the region text using the first element in
‘colour-ring’ as background colour.
With prefix argument, prompt for the colour to use instead.
Rotate ‘colour-ring’ unless a colour different from the
default one is specified."
  (interactive "r\nP") ; See https://lists.gnu.org/archive/html/help-gnu-emacs/2013-05/msg00654.html
  (let* ((default (ring-ref colour-ring -1))
         (colour (if arg
                     (minibuffer-with-setup-hook ; Put region around the initial input.
                         (lambda () (set-mark (minibuffer-prompt-end)))
                       (completing-read
                        "Highlight using colour: "
                        (defined-colors) nil nil default 'minibuffer-history))
                   default))
         (overlay (make-overlay beg end)))
    (when (equal (color-values colour) (color-values default))
      (ring-insert colour-ring colour))
    (overlay-put overlay 'face `(:background ,colour))
    (overlay-put overlay 'my-region-highlight t)
    (deactivate-mark)))

(defun unhighlight-regions ()
  "Remove all highlights made with ‘highlight-region’."
  (interactive)
  (remove-overlays nil nil 'my-region-highlight t))



;;;; Undo/redo

;; Increase undo lists’ size limits
;; https://codeberg.org/ideasman42/emacs-undo-fu#undo-limits
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Maintaining-Undo.html
(setq undo-limit         (* 96 1024 1024)) ;  96 MB. The change group at which this size is exceeded is the last one kept.
(setq undo-strong-limit (* 128 1024 1024)) ; 128 MB. The change group at which this size is exceeded is discarded itself (along with all older change groups). There is one exception: the very latest change group is only discarded if it exceeds ‘undo-outer-limit’.
(setq undo-outer-limit (* 1024 1024 1024)) ;   1 GB. If at garbage collection time the undo info for the current command exceeds this limit, Emacs discards the info and displays a warning. This is a last ditch limit to prevent memory overflow.

(setq undo-no-redo t)
(global-set-key (kbd "M-_") #'undo-redo)

;;; Persistent undo history

;; https://codeberg.org/ideasman42/emacs-undo-fu-session

(setq undo-fu-session-compression 'zst)
(setq undo-fu-session-file-limit 350)

(undo-fu-session-global-mode)

;;; Visual undo tree navigation

;; https://github.com/casouri/vundo

(with-eval-after-load 'vundo
  (setq vundo-glyph-alist vundo-unicode-symbols)
  (set-face-attribute 'vundo-highlight nil :foreground "black"))
(setq vundo-compact-display t)



;;;; Syntax checking

;; Make Flycheck display errors immediately
;; REVIEW: Does it mess up with other things that use the the minibuffer to display messages? (In that case, there are packages that might solve the problem, see http://www.flycheck.org/en/latest/community/extensions.html)
(setq flycheck-display-errors-delay 0)



;;;; Spell checking

;; In Fedora, this requires the packages ‘aspell-en’ and ‘aspell-it’.

(setq ispell-program-name "/usr/bin/aspell") ; https://www.reddit.com/r/emacs/comments/6k9e79/spelling_check_in_emacs_252_on_mac_os_sierra/

;; REVIEW: Fix bug #53773 (The region stays active after typing in a character when ‘flyspell-mode’ (in addition to ‘delete-selection-mode’ and ‘transient-mark-mode’) is enabled)
;;   Upstream it’s fixed in Emacs 29.0.50.
;;   https://debbugs.gnu.org/cgi/bugreport.cgi?bug=53773
;;   https://git.savannah.gnu.org/cgit/emacs.git/commit/?id=118a911159e75f3ad9305cd7f298816bfb59d715
(with-eval-after-load 'flyspell
  (load-file "~/.emacs.d/local/patches/flyspell/bug53773_fix-region-deactivation-delay.el"))

;; Scan back for typos
;; TODO: Add a way to quit that leaves the point where it is instead of bringing it back to the position it had before calling the function.

(autoload 'ispell-get-word "ispell")

(defun spell-check-backwards ()
  "Spell check backwards, starting from the current point
position, until the user either accepts a correction or quits the
spell checker."
  (interactive "^")
  (set-up-scroll-recenter)
  (let ((orig-pos (point-marker)))
    (unwind-protect
        (while
            (and
             (pcase (ispell-word nil 'quietly) ; Note: ‘ispell-word’ returns nil when the word is correct or it’s left unchanged, see ‘C-h v ispell-word’.
               ;; REVIEW: On quitting ‘ispell-word’ with ‘C-g’ or ‘X’ Emacs says “Spell-checking suspended; use C-u M-$ to resume”, but if you press ‘C-u M-$’ it says “No session to continue.  Use ’X’ command when checking!”.
               ('quit nil) ; If the user quits ‘ispell-word’ (either with ‘q’, ‘x’, ‘X’ or ‘C-g’), return nil to quit the loop;
               ('nil  t) ;   if the word is correct or its spelling is accepted, return t to continue iterating;
               (_     t)) ;  otherwise it means that the word has been corrected, then also return t to continue iterating.
             (pcase (backward-word)
               ('t t) ; We've successfully moved to the word preceding the one that’s just been spell checked. Go on with the next iteration;
               ('nil ;  we've reached the beginning of buffer, return nil to exit the loop.
                (message "No errors found")
                nil))))
      (goto-char (marker-position orig-pos))
      (set-up-scroll-never-recenter))))
(global-set-key (kbd "C-c q") #'spell-check-backwards)

;;; Highlight all misspelled words in the window

;; https://codeberg.org/ideasman42/emacs-spell-fu

(setq spell-fu-idle-delay 0) ;                                Mark misspelled words immediately. REVIEW: This delay applies to both the word being typed and the text that comes into view as the buffer is scrolled. I’d rather use a little delay but just for the word being typed. See https://codeberg.org/ideasman42/emacs-spell-fu/issues/41
(add-to-list 'minor-mode-alist '(spell-fu-mode " SpellFu")) ; Give the mode a lighter.

;; REVIEW (bug?): Make Spell Fu find and load the default personal dictionary by itself.
(defun my-spell-fu-add-default-personal-dictionary ()
  "Add the personal dictionary of the default language to those
used in ‘spell-fu-mode’."
  (spell-fu-dictionary-add (spell-fu-get-personal-dictionary "en" "~/.aspell.en.pws")))

;; Adapted from https://codeberg.org/ideasman42/emacs-spell-fu#usage
(defun my-spell-fu-add-italian-dictionaries ()
  "Add the Italian dictionaries to those used in ‘spell-fu-mode’."
  (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "it"))
  (spell-fu-dictionary-add (spell-fu-get-personal-dictionary "it" "~/.aspell.it.pws")))

(add-hook 'spell-fu-mode-hook ;#'my-spell-fu-add-italian-dictionaries) ;        FIXME: I would like to use the Italian dictionary, but it makes 20 million entries cache files that occupy 550 MB in total and it slows down Spell Fu. See https://codeberg.org/ideasman42/emacs-spell-fu/issues/40
                               #'my-spell-fu-add-default-personal-dictionary)

;; REVIEW: Use ‘Flyspell’ for spell checking Italian until the issue with Spell Fu and the Italian dictionary is solved.
(defun flyspell-it ()
  "Toggle ‘flyspell-mode’ and switch it to the Italian dictionary."
  (interactive)
  (ispell-change-dictionary "it")
  (flyspell-mode 'toggle))

(global-set-key [f7] #'spell-fu-mode)



;;;; Word completion

;;; Company

;; See https://company-mode.github.io/
;;     descriptions in ‘M-x customize-group RET company’
;;     ‘M-x describe-minor-mode RET company-mode’

(setq company-require-match 'never) ;                       Cancel selections by typing non-matching characters. https://github.com/company-mode/company-mode/wiki/Switching-from-AC
(setq company-auto-complete nil) ;                          If non-nil, the characters in ‘company-auto-complete-chars’ trigger the insertion of the selected completion candidate. https://github.com/company-mode/company-mode/wiki/Switching-from-AC
(with-eval-after-load 'company
  (setq company-auto-complete-chars
        (append '(?\( ?\\) company-auto-complete-chars))) ; Autocomplete on typing an opening paren or an escape character, in addition to the default characters. It has no effect if ‘company-auto-complete’ is nil.
(setq company-minimum-prefix-length 1) ;                    Enable completion for words as short as one character.
(setq company-tooltip-align-annotations t) ;                Align annotations to the right. Annotations are, for example, the Unicode symbols corresponding to candidate commands in LaTeX. See https://github.com/vspinu/company-math#further-customization
(setq company-dabbrev-downcase nil) ;                       Don’t downcase completion candidates. (2 links) https://emacs.stackexchange.com/questions/10837/how-to-make-company-mode-be-case-sensitive-on-plain-text, https://github.com/company-mode/company-mode/issues/14
(setq company-dabbrev-ignore-case nil) ;                    This avoids getting duplicate candidates when ‘company-dabbrev-downcase’ is nil. https://github.com/company-mode/company-mode/issues/413
(setq company-selection-wrap-around t) ;                    Wrap around after reaching the end of the candidates list.
(with-eval-after-load 'company
  (add-to-list 'company-transformers 'company-sort-prefer-same-case-prefix))
(setq company-tooltip-flip-when-above t) ;                  Flip the candidates list upside-down when it’s drawn above the word at point.

;; Bind these functions to the up and down keys to get them to adjust to a flipped tooltip
;; See https://github.com/company-mode/company-mode/issues/282

(defun company-select-above-or-abort (&optional arg)
  "Select the candidate above if there are more than one, else
abort and invoke the normal binding.
With ARG, move by that many elements."
  (interactive "p")
  (let ((ov company-pseudo-tooltip-overlay))
    (if (and ov (< (overlay-get ov 'company-height) 0))
        (company-select-next-or-abort arg)
      (company-select-previous-or-abort arg))))

(defun company-select-below-or-abort (&optional arg)
  "Select the candidate below if there are more than one, else
abort and invoke the normal binding.
With ARG, move by that many elements."
  (interactive "p")
  (let ((ov company-pseudo-tooltip-overlay))
    (if (and ov (< (overlay-get ov 'company-height) 0))
        (company-select-previous-or-abort arg)
      (company-select-next-or-abort arg))))

;; Always display the current candidate inline
;; https://github.com/company-mode/company-mode/wiki/Switching-from-AC
(setq company-frontends
      '(company-pseudo-tooltip-unless-just-one-frontend
        company-preview-frontend
        company-echo-metadata-frontend))

;; Don’t start completing automatically…
;; https://github.com/company-mode/company-mode/issues/554
(setq company-idle-delay nil)
;; …wait until I press TAB.
;; https://www.emacswiki.org/emacs/CompanyMode#toc6
;; https://www.emacswiki.org/emacs/CompanyMode#toc10
;; See also https://github.com/company-mode/company-mode/issues/94
(defun indent-or-company-complete ()
  "Complete if the point is at the end of a symbol, otherwise
indent the current line."
  (interactive)
  (if (looking-at "\\_>")
      (company-complete-common)
    (indent-for-tab-command)))
(with-eval-after-load 'company
  (define-key company-mode-map [remap indent-for-tab-command] #'indent-or-company-complete))

(with-eval-after-load 'company
  ;; Make ‘TAB’ cycle between candidates and ‘S-TAB’ cycle the other way around.
  ;; Make sure ‘company-selection-wrap-around’ is set to t, otherwise backtab won’t cycle.
  ;; https://github.com/company-mode/company-mode/wiki/Switching-from-AC
  (define-key company-active-map (kbd "TAB") #'company-complete-common-or-cycle)
  (define-key company-active-map (kbd "<tab>") #'company-complete-common-or-cycle)
  (define-key company-active-map (kbd "S-TAB") #'company-select-previous)
  (define-key company-active-map (kbd "<backtab>") #'company-select-previous)
  ;; Preserve direction of the up and down keys when the tooltip is flipped.
  (define-key company-active-map (kbd "<up>") #'company-select-above-or-abort)
  (define-key company-active-map (kbd "<down>") #'company-select-below-or-abort)
  ;; Make the right arrow key insert the selected candidate.
  (define-key company-active-map (kbd "<right>") #'company-complete-selection)
  ;; Make ‘forward-word’ insert the current selection, then behave as usual.
  (define-key company-active-map [remap forward-word]
    (lambda (arg)
      (interactive "p")
      (company-complete-selection)
      (forward-word (1- arg))))
  ;; Make ‘move-end-of-line’ insert the current selection, then behave as usual.
  (define-key company-active-map [remap move-end-of-line]
    (lambda (arg)
      (interactive "p")
      (company-complete-selection)
      (move-end-of-line arg))))

;; Enable Company mode everywhere
(add-hook 'after-init-hook #'global-company-mode)

;;; Abbrev

(setq save-abbrevs nil) ;                                                Don’t offer to save newly added abbrevs when quitting Emacs. REVIEW: So far it has asked only if an ESS buffer had been opened during the session. Isn’t there an ESS-specific setting?



;;;; Snippet manager

;; To use YASnippet as a non-global minor mode, don’t call ‘yas-global-mode’; instead call ‘yas-reload-all’ to load the snippet tables and then call ‘yas-minor-mode’ from the hooks of major modes where you want YASnippet enabled.
;; See https://github.com/joaotavora/yasnippet

(setq yas-snippet-dirs
      `(,(expand-file-name "snippets" my-emacs-directory))) ; Do not load bundled snippets.
(setq yas-indent-line 'fixed) ;                               Indent snippets to the current column; do not apply automatic indentation on expansion. See https://emacs.stackexchange.com/questions/12479/indent-each-yasnippet-line-according-to-major-mode

(with-eval-after-load 'yasnippet (yas-reload-all))

;; Group related snippets into filesets for quick access
(customize-set-variable
 'filesets-data
 (let ((LaTeX-dir (concat my-emacs-directory "snippets/latex-mode/")))
   (append
    `(("LaTeX-figures-snippets"
       (:pattern ,LaTeX-dir "^figure"))
      ("LaTeX-tables-snippets"
       (:pattern ,LaTeX-dir "^table"))
      ("LaTeX-templates"
       (:pattern ,(concat LaTeX-dir "templates/") "^[^.].*[^~]$"))) ; The regex matches any file except hidden or backup files.
    filesets-data)))



;;;; Key bindings (text editing)

(global-set-key (kbd "<C-M-backspace>") #'backward-kill-sexp) ; See https://lists.gnu.org/archive/html/emacs-pretest-bug/2004-10/msg00186.html
(global-set-key (kbd "C-c j") #'raise-sexp) ; https://dawranliou.com/blog/structural-editing-in-vanilla-emacs/

;; Rebind ‘M-{l,c,u}’ to smarter commands.
;; This frees up ‘C-x C-l’ and ‘C-x C-u’ for more useful bindings than the default ‘downcase-region’ and ‘upcase-region’.
(global-set-key (kbd "M-l") #'downcase-dwim) ; replaces ‘downcase-word’.
(global-set-key (kbd "M-c") #'capitalize-dwim) ; replaces ‘capitalize-word’.
(global-set-key (kbd "M-u") #'upcase-dwim) ; replaces ‘upcase-word’.

;;; Shortcuts (text editing)

(defun filenamify (beg end)
  "Replace bad file name characters with underscores in the
active region."
  (interactive "*r")
  (unless (use-region-p)
    (user-error "Select a region first"))
  (replace-regexp-in-region "[/:\n ]" "_" beg end))

;; Adapted from https://www.emacswiki.org/emacs/InsertingTodaysDate
;; See also https://irreal.org/blog/?p=1613
(defun datestamp (&optional arg)
  "Insert today’s date as yyyy/mm/dd.
With prefix argument, use the ISO 8601 format."
  (interactive "*P")
  (when (and (use-region-p) delete-selection-mode)
    (delete-region (region-beginning) (region-end))
    (undo-boundary)) ; makes Emacs undo the insertion of the date and the deletion of the region text in two separate steps, like for other commands that overwrite the region when ‘delete-selection-mode’ is enabled.
  (insert (format-time-string (if arg
                                  "%Y-%m-%d"
                                "%Y/%-m/%-d"))))
(global-set-key (kbd "C-c o") #'datestamp)



;;;; Language-related configurations

;;; Plain text

(define-key text-mode-map (kbd "<triple-mouse-1>") #'mark-paragraph)

;; Adapted from https://www.emacswiki.org/emacs/EmacsSyntaxTable
(defvar text-syntax-table
  (let ((table (make-syntax-table text-mode-syntax-table)))
    (modify-syntax-entry  ?…  "." table) ; Default is ‘_’ (symbol).
    (modify-syntax-entry  ?   " " table) ; Default is ‘.’ (punctuation).
    ;; The ‘electric-pair-inhibit-predicate’ can be used to control the automatic insertion of closing delimiters in ‘electric-pair-mode’. Since only characters whose syntax class is ‘(’, ‘"’ or ‘$’ can trigger it [1], this syntax table assigns one of these syntax classes to characters whose pairing is not controlled by the ‘electric-pair-inhibit-predicate’ by default.
    ;; ‘'’ and ‘`’ are assigned the syntax class of string delimiters in order to enable their automatic pairing, under the control of ‘electric-pair-inhibit-predicate’.
    ;; Note: the manual says that the open and close parenthesis syntax classes are for “characters used in dissimilar pairs to surround sentences or expressions”. See [2].
    ;; The default syntax class is ‘.’ (punctuation) except where otherwise noted.
    ;; 1 See the definition of ‘electric-pair-post-self-insert-function’.
    ;; 2 ‘(info "(elisp) Syntax Class Table")’.
    (modify-syntax-entry ?\" "\"" table)
    (modify-syntax-entry  ?' "\"" table) ; Default is ‘w’ (word).
    (modify-syntax-entry  ?` "\"" table)
    (modify-syntax-entry  ?« "(»" table)
    (modify-syntax-entry  ?» ")«" table)
    (modify-syntax-entry  ?“ "(”" table)
    (modify-syntax-entry  ?” ")“" table)
    (modify-syntax-entry  ?‘ "(’" table)
    (modify-syntax-entry  ?’ ")‘" table)
    table)
  "Syntax table for non source code text.
To use this syntax table for strings and comments in any mode call
‘(setq-local electric-pair-text-syntax-table text-syntax-table)’
from the mode’s hook.")

;; Adapted from https://stackoverflow.com/questions/32106180/electric-pair-mode-not-detected-for-some-characters-with-german-keyboard-layout
(defun toggle-markup-pair (char)
  "Insert CHAR as a paired delimiter. If there is already a pair
of CHARs around the point, or around the region if it is active,
delete them instead."
  (let* ((last-command-event char)
         (original-syntax (string (char-syntax char)))
         (beg (if (use-region-p) (region-beginning) (point)))
         (end (if (use-region-p) (region-end) (point))))
    ;; REVIEW: Don’t use ‘(with-syntax-table (make-syntax-table (syntax-table)) …)’ here, it makes unpaired straight quotes interfere with the pairing of the underscore in ‘toggle-markdown-italic’. You can use ‘(with-syntax-table text-syntax-table …)’ but still ‘with-syntax-table’ is a bit overkill for changing the syntax class of one character. See https://emacs.stackexchange.com/questions/12494/change-character-syntax-temporarily
    (if (and (eq (char-before beg) char)
             (eq (char-after end) char))
        (save-excursion
          (goto-char end)
          (delete-char 1)
          (goto-char beg)
          (delete-char -1))
      (modify-syntax-entry char "\"")
      (self-insert-command 1)
      (modify-syntax-entry char original-syntax))))

(defun toggle-markdown-italic ()
  "Insert ‘_’ as a paired delimiter. If there is already a pair
of underscores around the point, or around the region if it is
active, delete them instead."
  (interactive "*")
  (toggle-markup-pair ?_))
(define-key text-mode-map (kbd "C-c i") #'toggle-markdown-italic)

(defun toggle-straight-single-quotes (arg)
  "Insert ‘'’ as a paired delimiter. If there is already a pair
of straight single quotes around the point, or around the region
if it is active, delete them instead.
With prefix argument ARG, insert ‘\"’ instead."
  (interactive "P*")
  (let ((electric-quote-mode nil))
    (if arg
        (toggle-markup-pair ?\")
      (toggle-markup-pair ?'))))
(define-key text-mode-map (kbd "C-c k") #'toggle-straight-single-quotes)
(define-key text-mode-map (kbd "C-c K") (lambda () (interactive) (toggle-straight-single-quotes 'double)))

(defun show-paren--no-single-quotes ()
  "Same as ‘show-paren--default’ except this always ignores
single quotes.
Pairs of single quotes can’t be matched reliably without some
form of escaping, because the closing curly single quote is the
same character as the curly apostrophe and the straight single
quote is the same character as the straight apostrophe."
  (with-syntax-table (make-syntax-table (syntax-table))
    (modify-syntax-entry  ?' ".")
    (modify-syntax-entry  ?‘ ".")
    (modify-syntax-entry  ?’ ".")
    (show-paren--default)))

(defun set-up-text-modes ()
  "Function with customizations for editing non source code
text."
  (set-syntax-table text-syntax-table)
  (setq-local electric-pair-text-syntax-table text-syntax-table)
  (setq-local show-paren-data-function #'show-paren--no-single-quotes)
  (setq-local blink-paren-function nil) ;                 ‘blink-matching-open’, the default value, is useless with ‘show-paren-mode’ enabled, and the syntax table used in ‘text-mode’ breaks it: treating ‘"’ and ‘'’ as string delimiters tricks it into seeing mismatched parentheses in strings like ‘Alice's (Bob's)’ (because the opening parenthesis is between single quotes (i.e. inside a string, in its view) and the closing one isn’t), and it considers an apostrophe (which is the same character as the closing single quote) as a mismatched delimiter when its syntax class is ‘)’.
  (electric-quote-local-mode)
  (setq-local search-default-mode 'char-fold-to-regexp) ; makes searching for quotes and apostrophes easier after writing with ‘electric-quote-mode’ enabled. Use ‘M-s '’ to revert back to literal search. See ‘(info "(emacs) Lax Search")’. FIXME (bug #52394, fixable in Emacs 29.0.50 [1]): Isearch doesn’t match long equivalent (but unequal) strings. Char folding is regexp-based and converts the search string into a very long regexp that matches all character variants. But the regexp have limitations when matching runs out of internal stack space. This is described in the Info node: ‘(info "(elisp) Regexp Problems")’. See [1]. 1 https://lists.gnu.org/archive/html/bug-gnu-emacs/2021-12/msg00839.html
  (setq-local replace-char-fold t)) ;                     Enable character folding for matching in ‘query-replace’ and ‘replace-string’. This setting does not affect the replacement text, only how Emacs finds the text to replace. It also doesn’t affect ‘replace-regexp’. See ‘(info "(emacs) Replacement and Lax Matches")’.

(defun set-up-text-mode ()
  "Function with customizations for ‘text-mode’."
  (when (eq major-mode 'text-mode) ; Don’t apply these settings to all modes derived from ‘text-mode’.
    (set-up-text-modes)))
(add-hook 'text-mode-hook #'set-up-text-mode)

(add-hook 'artist-mode-hook #'maybe-display-leftover-whitespace)



;;; Spreadsheets

;; https://elpa.gnu.org/packages/csv-mode.html

;; Note: the variables ‘csv-separators’, ‘csv-field-quotes’ and ‘csv-comment-start-default’ use ‘:set’ters in their definitions, so they must be set using ‘customize-set-variable’ (or ‘custom-set-variables’). See “What to use to set customizable variables” in my ‘emacsguide’.
(setq csv-align-padding 2)
;(setq csv-invisibility-default nil) ; FIXME (bug?): If ‘csv-invisibility-default’ is nil, on calling ‘csv-align-fields’ (‘C-c C-a’) Emacs returns “funcall-interactively: Wrong type argument: listp, t”. You can use ‘C-c C-v C-c C-a’ to toggle the display of field separators.



;;; Markdown

;; https://jblevins.org/projects/markdown-mode/

(setq markdown-command "pandoc")
(setq markdown-asymmetric-header t)
(setq markdown-header-scaling t)
(setq markdown-header-scaling-values '(1.3 1.24 1.18 1.12 1.06 1))
(setq markdown-list-indent-width 2)
(setq markdown-enable-math t)
;(setq markdown-fontify-code-blocks-natively t)
(setq markdown-gfm-uppercase-checkbox t) ; This is useful for compatibility with org-mode, which doesn’t recognize the lowercase variant.

;; Adapted from https://stackoverflow.com/questions/19676181/electric-pair-mode-and-python-triple-quotes
(defun markdown-electric-pair-bold-delimiter ()
  "Set up ‘**’ as a paired delimiter."
  (when (and electric-pair-mode
             (eq last-command-event ?*)
             (not (use-region-p))
             (let ((count 0))
               (while (eq (char-before (- (point) count)) last-command-event)
                 (setq count (1+ count)))
               (= count 2)))
    (save-excursion
      ;; Insert one additional asterisk if Emacs has already paired the first, otherwise insert two.
      (insert (make-string (if (and (eq (char-syntax ?*) ?\")
                                    (looking-at "*"))
                               1
                             2)
                           last-command-event)))))

(defun set-up-markdown-mode ()
  "Function with customizations for ‘markdown-mode’."
  (set-up-text-modes)
  (add-hook 'post-self-insert-hook #'markdown-electric-pair-bold-delimiter ; Set up ‘**’ as a paired delimiter.
            'append 'local))
(add-hook 'markdown-mode-hook #'set-up-markdown-mode)



;;; TeX

;; Automatic parsing and customization
;; see https://www.gnu.org/software/auctex/manual/auctex/Parsing-Files.html
;;     https://www.gnu.org/software/auctex/manual/auctex/Automatic.html
;; WARNING: Pay attention to the order of setting of these variables, especially if you set them inside ‘with-eval-after-load’ blocks: the mechanism that sets ‘TeX-style-path’ might get tangled.
;; REVIEW (bug that won’t be fixed); ‘TeX-complete-symbol’ doesn’t work in math mode for user-defined commands. See AUCTeX bug #35074, https://lists.gnu.org/archive/html/bug-auctex/2019-04/msg00000.html
;; REVIEW: As I understand it, AUCTeX doesn’t directly look for paths to search in in the customization variables (e.g. ‘TeX-auto-private’), rather, it gathers up the paths contained in those variables in another variable, ‘TeX-style-path’, and then searches through the paths listed there.
;;   The problem arises when I need to defer the setting of one of the automatic customization variables (e.g. ‘TeX-macro-private’, ‘TeX-auto-private’) to after AUCTeX has been loaded (because, for example, I want to set the value of ‘TeX-auto-private’ based on that of ‘TeX-macro-private’; ‘TeX-macro-private’ is empty at start-up, so I’d have to wrap the setting in a ‘with-eval-after-load "latex"’ block. In that case ‘TeX-style-path’ takes the value that ‘TeX-auto-private’ had *before* my definition took effect, so no real changes are made to the list of paths in which AUCTeX searches through.
;;   See https://emacs.stackexchange.com/questions/22657/auctex-isnt-loading-local-per-package-style-files-is-ignoring-tex-auto-local
(setq TeX-parse-self t) ;                                    Enable parse on load. https://www.gnu.org/software/auctex/manual/auctex/Parsing-Files.html
(setq TeX-auto-save t) ;                                     Save the information generated from parsing in a subdirectory. The name of the subdirectory is specified by the variable ‘TeX-auto-local’. https://www.gnu.org/software/auctex/manual/auctex/Parsing-Files.html
(setq TeX-auto-local ".auto") ;                              The name of the subdirectory containing the automatically generated TeX information. The default is ‘auto’; this makes it hidden. https://www.gnu.org/software/auctex/manual/auctex/Automatic-Local.html
(setq TeX-macro-private '("~/.texmf/tex/latex/packages/")) ; Directories where you store your personal TeX macros. The extracted information will go to the directories listed in ‘TeX-auto-private’. https://www.gnu.org/software/auctex/manual/auctex/Automatic-Private.html
(setq TeX-auto-private ;                                     Directories where the information extracted from the directories in ‘TeX-macro-private’ is stored; see https://www.gnu.org/software/auctex/manual/auctex/Automatic-Private.html
      (mapcar (lambda (c) (concat c TeX-auto-local)) TeX-macro-private)) ; Here ‘mapcar’ makes a list with each element of ‘TeX-macro-private’ concatenated with the value of ‘TeX-auto-local’. Function from https://stackoverflow.com/questions/22622257/how-to-add-a-common-prefix-suffix-to-a-list-of-strings
(setq-default TeX-master nil) ;                              Query for master file.

;; AUCTeX auto-completion
;; FIXME (bug?): ‘TeX-insert-macro’ wraps the argument of ‘\emdash’ (from ‘mymacros.sty’) in braces instead of square brackets when called with an active region. This even though AUCTeX is aware that ‘\emdash’ takes a single optional argument.
;;   https://www.gnu.org/software/auctex/manual/auctex/Adding-Macros.html
;;   ~/src/latex/packages/.auto/mymacros.el
(setq TeX-insert-macro-default-style 'mandatory-args-only) ; Make ‘TeX-insert-macro’ ask for optional arguments only when it’s called with ‘C-u’. https://www.gnu.org/software/auctex/manual/auctex/Completion.html
(setq TeX-default-macro "cref") ;                            Default macro for ‘C-c C-m’.
;(setq TeX-electric-escape t) ;                              This binds ‘\’ to ‘TeX-electric-macro’, which prompts for the name of a TeX macro. Space will complete and exit. By default AUCTeX will put an empty set of curly braces (‘{}’) after a macro without arguments. https://www.gnu.org/software/auctex/manual/auctex/Completion.html
;(setq TeX-complete-expert-commands t) ;                     Enable auto-completion for expert commands
(setq LaTeX-electric-left-right-brace t) ;                   Auto-pair braces. https://www.gnu.org/software/auctex/manual/auctex/Quotes.html#Braces
(setq TeX-electric-math (cons "$" "$")) ;                    Auto-pair inline math delimiters. https://www.gnu.org/software/auctex/manual/auctex/Quotes.html#Dollar-Signs
;(setq TeX-electric-sub-and-superscript t) ;                 Insert braces after typing ‘^’ or ‘_’ in math mode. https://www.gnu.org/software/auctex/manual/auctex/Mathematics.html
(setq LaTeX-csquotes-open-quote "\\enquote{") ;              Make ‘"’ expand into csquotes macros. https://tex.stackexchange.com/questions/39575/auctexs-csquotes-integration-does-not-work-together-with-babel
(setq LaTeX-csquotes-close-quote "}")
(setq LaTeX-default-environment "tabular") ;                 See ‘C-h f LaTeX-environment’.
(setq LaTeX-float "htb") ;                                   See ‘C-h f LaTeX-environment’.
(setq LaTeX-default-position nil) ;                          See ‘C-h f LaTeX-environment’.
(setq reftex-insert-label-flags '("s" "sfte")) ;             Derive section labels from headings (with confirmation). Prompt for figure, table and equation labels. Use simple labels without confirmation for everything else. https://www.gnu.org/software/auctex/manual/reftex.html#SEC60
(setq company-reftex-max-annotation-length 30) ;             Annotate label completion candidates with their contents and citation completion candidates with the title of the cited publication. See also ‘company-reftex-annotate-labels’ and ‘company-reftex-annotate-citations’.

;; Fontify some macros
;; https://www.gnu.org/software/auctex/manual/auctex/Fontification-of-macros.html
;; https://tex.stackexchange.com/questions/326706/efficiently-highlight-known-math-macros-in-auctex
(setq font-latex-user-keyword-classes
      ;; ‘\mpunct’ is defined in ‘mymacros.sty’.
      ;; Since it’s defined as a wrapper around ‘\mbox’, I use the same face as ‘\mbox’ for it.
      ;; ‘\mbox’ is classified as ‘function’ in ‘font-latex.el’ so it uses the face ‘font-lock-function-name-face’.
      '(("my-function"  (("mpunct" "{")) font-lock-function-name-face command)))

;; Other variables
(setq reftex-plug-into-AUCTeX t) ;                           Integrate RefTeX with AucTeX. See (2 links) https://www.gnu.org/software/emacs/manual/html_node/reftex/AUCTeX_002dRefTeX-Interface.html, https://www.gnu.org/software/auctex/manual/reftex.html#SEC68
(setq TeX-source-correlate-start-server t) ;                 Start the correlation server for SyncTeX without asking.
(setq TeX-save-query nil) ;                                  Don’t ask to save before LaTeXing.
(setq TeX-newline-function 'newline-and-indent) ;            Automatically indent new lines. https://www.gnu.org/software/auctex/manual/auctex/Indenting.html
(setq LaTeX-syntactic-comments nil) ;                        Don’t move the cursor past the comment delimiters after inserting a newline; see (2 links) https://emacs.stackexchange.com/questions/36059/disable-strange-comment-behavior-in-auctex, https://www.gnu.org/software/auctex/manual/auctex/Indenting.html

(with-eval-after-load "latex" ;                              ‘"latex"’ because ‘C-h f TeX-latex-mode’ says that ‘TeX-latex-mode’ is defined in ‘latex.el’ (which is part of AUCTeX). It overrides plain Emacs’ ‘latex-mode’ (and its alias ‘LaTeX-mode’), which is defined in ‘tex-mode.el’. Type ‘C-h f latex-mode’ while in ‘TeX-latex-mode’ to get more information.
  (add-to-list 'TeX-view-program-selection ;                 Make SyncTeX use Zathura; see https://tex.stackexchange.com/questions/207889/how-to-set-up-forward-inverse-searches-with-auctex-and-zathura/291730#291730
               '(output-pdf "Zathura"))
  (LaTeX-math-mode-auto-dollars) ;                           Wrap symbols inserted using LaTeX Math mode in dollars, if necessary.
  ;; Choose the dictionary based on document class or babel language
  ;; Requires parsing the file.
  ;; NOTE: If ‘(setq TeX-auto-local ".auto")’ is inside a ‘with-eval-after-load "latex"’ block, the choice of dictionary in turn has to be delayed.
  ;; https://www.gnu.org/software/auctex/manual/auctex.html#Style-Files-for-Different-Languages
  (add-hook 'TeX-language-en-hook
    (lambda () (ispell-change-dictionary "english")))
  (add-hook 'TeX-language-it-hook
    (lambda () (ispell-change-dictionary "italian")))
  ;; Note: don’t call ‘define-key’ from hooks except for very specific modes that aggressively enforce their own binding (e.g. ‘flyspell-mode’).
  ;; See http://stackoverflow.com/questions/7598433/how-to-remove-a-key-from-a-minor-mode-keymap-in-emacs#comment9218326_7598754
  (define-key TeX-mode-map (kbd "C-M-<left>")  #'jump-into-previous-parens) ; replaces ‘backward-sexp’, which is still bound to ‘C-M-b’.
  (define-key TeX-mode-map (kbd "C-M-<right>") #'jump-into-next-parens) ; replaces ‘forward-sexp’, which is still bound to ‘C-M-f’.
  (define-key TeX-mode-map (kbd "<triple-mouse-1>") #'mark-paragraph)
  (define-key LaTeX-mode-map (kbd "C-c '") nil)) ;           Prevent AUCTeX from overriding my binding to ‘goto-saved-place’. It replaces ‘TeX-comment-or-uncomment-paragraph’, which is still bound to ‘C-c %’.

;; See https://www.gnu.org/software/auctex/manual/auctex/Fontification-of-math.html
(with-eval-after-load 'font-latex
  (defun toggle-LaTeX-script-fontification ()
    "Toggle raising of superscript and lowering of subscripts in
the current buffer."
    (interactive)
    (make-local-variable 'previous-font-latex-fontify-script-memo)
    (when font-latex-fontify-script
      (setq previous-font-latex-fontify-script-memo font-latex-fontify-script))
    (setq-local font-latex-fontify-script
                (unless font-latex-fontify-script
                  previous-font-latex-fontify-script-memo))
    (font-lock-fontify-buffer)))

;; REVIEW: Ugly hack
(defun TeX-insert-quote-maybe-around-region ()
  "Insert the appropriate quotation mark for TeX. If there’s
an active region, insert the appropriate marks around it."
  (interactive)
  (if (use-region-p)
      (save-restriction
        (narrow-to-region (region-beginning) (region-end)) ; Narrow to the active region so that ‘TeX-insert-quote’ always inserts an opening quote at the start of the region. The narrowing is also used later for placing the point at its final position.
        (goto-char (point-max))
        (save-excursion
          (insert-char ?&) ;                                 Insert a character (one that makes ‘latex’ raise an error if left over) at the end of the region, so that ‘TeX-insert-quote’ always inserts a closing quote there.
          (call-interactively #'TeX-insert-quote))
        (delete-char 1)
        (goto-char 1)
        (call-interactively #'TeX-insert-quote)
        ;; Now the point is after the opening quotation mark. We leave it after the closing mark if it was at the end of the region when the command was invoked.
        (when (< (mark) (point))
          (goto-char (point-max))))
    (call-interactively #'TeX-insert-quote)))
(global-set-key [remap TeX-insert-quote] #'TeX-insert-quote-maybe-around-region)

;; See https://tex.stackexchange.com/questions/148563/how-can-i-streamline-insertion-of-latex-math-mode-symbols-in-auctex
(defun LaTeX-math-mode-auto-dollars ()
  "Put dollar signs around symbols inserted using
‘LaTeX-math-mode’ shortcuts without requiring the universal
argument, unless the point is already inside a math environment.

This allows typing just \\<LaTeX-math-mode-map>\\[LaTeX-math-alpha] instead of \\[universal-argument] \\[LaTeX-math-alpha] to insert $\\alpha$."
  (let ((math (reverse (append LaTeX-math-list LaTeX-math-default))))
    (while math
      (let ((entry (car math))
            value)
        (setq math (cdr math))
        (if (listp (cdr entry))
            (setq value (nth 1 entry))
          (setq value (cdr entry)))
        (when (stringp value)
          (fset (intern (concat "LaTeX-math-" value))
                (list 'lambda (list 'arg) (list 'interactive "*P")
                      (list 'LaTeX-math-insert value
                            '(null (texmathp))))))))))

;; See https://emacs.stackexchange.com/questions/6045/how-to-delete-a-latex-macro-while-preserving-its-text-content
(defun mg-TeX-delete-current-macro (&optional arg)
  "Remove the current macro.
With an optional argument ARG, delete just the ARG-th macro
starting from the innermost."
  (interactive "*p")
  (let (macro end)
    (when (dotimes (_ arg macro)
            (goto-char (TeX-find-macro-start))
            (setq macro (TeX-current-macro)
                  end (TeX-find-macro-end))
            ;; If we need to look for an outer macro we have to “exit” from the current one.
            (backward-char))
      ;; Return to the beginning of the macro to be deleted.
      (forward-char)
      (re-search-forward
       (concat (regexp-quote TeX-esc) macro "\\(?:\\[[^]]*\\]\\)?"
               TeX-grop "\\(\\(.\\|\n\\)*\\)")
       end t)
      (replace-match "\\1")
      ;; Delete the closing brace.
      (delete-backward-char 1))))
(with-eval-after-load 'latex
  (define-key LaTeX-mode-map (kbd "C-c DEL") #'mg-TeX-delete-current-macro))

(defun set-up-company-mode-LaTeX ()
  "Company mode set-up for LaTeX."
  (add-to-list (make-local-variable 'company-backends)
    ;; Note: changing the order might break some backend’s functionality.
    '(company-math-symbols-latex ;                           Symbol annotations (by default, active only on LaTeX math faces). From the company-math package, https://github.com/vspinu/company-math
      company-capf ;                                         Use candidates from plain Emacs’ and AUCTeX’s ‘completion-at-point-functions’. This adds user-defined commands to the candidates list. REVIEW: Candidates from ‘completion-at-point-functions’ are available even if the backend ‘company-capf’ isn’t explicitly loaded.
      company-dabbrev-code ;                                 REVIEW: I thought this backend would keep the list of completion candidates updated as new commands are defined, without the need to re-parse the document every time, but it doesn’t. Note: trying to complete a newly defined command with ‘M-x company-dabbrev-code’ returns “user-error: Cannot complete at point”.
      ;company-latex-commands ;                              REVIEW: It provides some more candidates but they’re not really useful. From the company-math package, https://github.com/vspinu/company-math
      company-reftex-labels ;                                https://github.com/TheBB/company-reftex
      company-reftex-citations))) ;                          https://github.com/TheBB/company-reftex

(defun set-up-LaTeX-mode ()
  "Function with customizations for ‘LaTeX-mode’."
  ;; Note about typefaces: Inconsolata doesn’t have a slanted font, which AUCTeX can make use of.
  (setq-local electric-pair-open-newline-between-pairs t) ;  Move the closing brace down one additional line when you hit RET with the point between two braces.
  (set-up-company-mode-LaTeX) ;                              Set up word completion.
  (yas-minor-mode) ;                                         Use YASnippet.
  (spell-fu-mode) ;                                          Check spelling.
  (dolist (face '(font-latex-sedate-face ;                   Disable spell checking for LaTeX command names,
                  font-lock-function-name-face ;             package names and
                  font-lock-variable-name-face)) ;           package option keys.
    (add-to-list 'spell-fu-faces-exclude face))
  (outline-minor-mode) ;                                     See https://www.gnu.org/software/emacs/manual/html_node/emacs/Outline-Mode.html
  (TeX-fold-mode) ;                                          Fold (La)TeX commands with ‘C-c C-o C-b’, unfold with ‘C-c C-o b’. https://tex.stackexchange.com/questions/52179/what-is-your-favorite-emacs-and-or-auctex-command-trick
  (turn-on-reftex) ;                                         Turn on RefTeX Mode for all LaTeX files. https://www.gnu.org/software/auctex/manual/reftex.html#SEC7
  (TeX-source-correlate-mode) ;                              Enable SyncTeX.
  (LaTeX-math-mode) ;                                        Enable LaTeX Math mode. https://www.gnu.org/software/auctex/manual/auctex/Mathematics.html
  (setq-local blink-matching-delay 0) ;                      Stop Emacs from moving the cursor temporarily to the opening dollar sign, without disabling ‘blink-matching-paren’.
  ;(modify-syntax-entry ?\\ "w") ;                           Treat the backslash as a word constituent (try ‘M-f’/‘M-b’ around a command). Its original syntax class is ‘/’. https://www.gnu.org/software/emacs/manual/html_node/elisp/Syntax-Class-Table.html
  ;(modify-syntax-entry ?& "w") ;                            Treat the ampersand as a word constituent (try ‘M-f’/‘M-b’ in a table). Its original syntax class is ‘.’.
  (modify-syntax-entry ?' ".")) ;                            Treat the straight apostrophe as a punctuation character (try ‘C-DEL’ at the end of two words joined by an apostrophe, like ‘doesn't’). Its original syntax class is ‘w‘.
(add-hook 'LaTeX-mode-hook #'set-up-LaTeX-mode)



;;; Any programming language whose mode derives from ‘prog-mode’.

(define-key prog-mode-map (kbd "RET") #'newline-and-indent) ; Make RET indent the new line.

(defun set-up-prog-mode ()
  "Function with customizations for ‘prog-mode’."
  (setq fill-column 80) ;       Column at which long lines are wrapped by fill commands.
  (setq comment-column 40) ;    Column at which in-line comments are aligned.
  (setq buffer-face-mode-face ; Use Inconsolata.
        '(:family "Inconsolata" :height 120))
  (buffer-face-mode))
(add-hook 'prog-mode-hook #'set-up-prog-mode)



;;; gnuplot

;; https://github.com/emacs-gnuplot/gnuplot

(add-to-list 'auto-mode-alist '("\\.gp\\'" . gnuplot-mode))
;(setq gnuplot-quote-character "\'") ; The default.

;; Set-up Eldoc for gnuplot
;; See “Retrieve gnuplot’s Info manual (‘gnuplot.info*’) and Eldoc strings library (‘gnuplot-eldoc.el’)” in the emacsguide.
(add-to-list 'load-path (expand-file-name "gnuplot-eldoc" my-lib-dir))
(setq gnuplot-eldoc-mode t)

(defun gnuplot-restart-and-send-buffer-to-gnuplot ()
  "Kill the gnuplot process and send the current buffer to a new one."
  (interactive)
  (gnuplot-kill-gnuplot-buffer)
  (gnuplot-send-buffer-to-gnuplot))
(with-eval-after-load 'gnuplot
  (define-key gnuplot-mode-map (kbd "C-c C-c") #'gnuplot-restart-and-send-buffer-to-gnuplot)) ; replaces ‘comment-region’.

;; Export and open plots
;; TODO: Add support for (La)TeX terminals.

(defun gnuplot-c&r-set-compile-command ()
  "Set the file extension and the ‘compile-command’
for gnuplot plots."
  (setq c&r-compiled-file-extension ; Ask for the extension to use for the compiled plot. Preselect the initial input, which is the extension that was last used; see https://emacs.stackexchange.com/questions/72153/how-to-preselect-initial-text-in-the-minibuffer-with-elisp
        (minibuffer-with-setup-hook
            (lambda () (set-mark (minibuffer-prompt-end)))
          (read-string "Extension: " c&r-compiled-file-extension)))
  (unless (assq 'compile-command file-local-variables-alist) ; Don’t override the file-local value. Note: don’t just check if ‘compile-command’ already has a value because it’s a global variable and its value might not be the right one for the current buffer.
    (setq-local compile-command
                (mapconcat #'identity ; Here I need to concatenate strings with a space as separator. ‘concat’ doesn’t use a separator, so I use ‘mapconcat #'identity’ instead. See also ‘string-join’ (which requires loading ‘subr-x.el’, however), via https://stackoverflow.com/questions/12999530/is-there-a-function-that-joins-a-string-into-a-delimited-string/40633410#40633410
                           (list "gnuplot"
                                 (c&r-source-file-name)
                                 ">"
                                 (c&r-compiled-file-name)) " "))))

(defun gnuplot-c&r-set-run-command ()
  "Set the shell command that opens the plot after a
successful compilation."
  (unless (assq 'c&r-run-command file-local-variables-alist)
    (setq-local c&r-run-command (concat "gthumb " (c&r-compiled-file-name)))))

(defun set-up-gnuplot-mode ()
  "Function with customizations for ‘gnuplot-mode’."
  ;; REVIEW: ‘gnuplot-mode’ is deaf to ‘(electric-quote-local-mode)’ and ‘(setq-local electric-pair-text-syntax-table text-syntax-table))’.
  (add-hook 'before-save-hook #'delete-trailing-whitespace ; Delete trailing whitespace on save.
            nil 'local)
  (setq c&r-use-terminal nil) ;                              Set up Compile and Run.
  (setq c&r-set-compile-command-function #'gnuplot-c&r-set-compile-command)
  (setq c&r-set-run-command-function #'gnuplot-c&r-set-run-command))
(add-hook 'gnuplot-mode-hook #'set-up-gnuplot-mode)



;;; Scheme

;; Enhanced Emacs support for Racket and Scheme
;; http://www.neilvandyke.org/quack/
(autoload 'scheme-mode "quack" nil t)

(setq quack-pretty-lambda-p t) ;                             Display lambdas as λ’s.

(defun set-up-scheme-mode ()
  "Function with customizations for ‘scheme-mode’."
  (add-hook 'before-save-hook #'delete-trailing-whitespace ; Delete trailing whitespace on save.
            nil 'local))
(add-hook 'scheme-mode-hook #'set-up-scheme-mode)



;;; Common Lisp

;; https://joaotavora.github.io/sly/

(setq inferior-lisp-program "/usr/bin/sbcl")
(with-eval-after-load 'sly
  (define-key sly-mode-map (kbd "M-_") nil)) ; Leave ‘M-_’ bound to ‘undo-redo’. SLY would bind it to ‘sly-edit-uses’, which it also binds to ‘M-?’.



;;; Emacs Lisp

(defun set-up-emacs-lisp-mode ()
  "Function with customizations for ‘emacs-lisp-mode’."
  (electric-quote-local-mode)
  (setq-local electric-pair-text-syntax-table ;              Use ‘text-mode’’s automatic pairing set-up in strings and comments.
              text-syntax-table)
  (add-hook 'before-save-hook #'delete-trailing-whitespace ; Delete trailing whitespace on save.
            nil 'local))
(add-hook 'emacs-lisp-mode-hook #'set-up-emacs-lisp-mode)



;;; Shell script

(defun set-up-sh-mode ()
  "Function with customizations for ‘sh-mode’."
  (add-hook 'before-save-hook #'delete-trailing-whitespace ; Delete trailing whitespace on save.
            nil 'local)
  (setq-local electric-pair-open-newline-between-pairs t) ;  Move the closing brace down one additional line when you hit RET with the point between two braces.
  (setq-local electric-pair-text-syntax-table ;              Use ‘text-mode’’s automatic pairing set-up in strings and comments.
              text-syntax-table))
(add-hook 'sh-mode-hook #'set-up-sh-mode)



;;; Python

;; Set up IPython as the Python shell in Emacs
;; FIXME: IPython uses virtual environments unreliably:
;;     import sys; print(sys.prefix)
;;   returns “/usr” even when a virtual environment has been activated (with ‘pythonic-activate’). Note that
;;     import sys; print(sys.path)
;;   returns the virtual environment’s path among others. See the pythonguide for more information.
;;   This issue is related to IPython and independent of Emacs (it’s the same outside Emacs).
;;   The ‘python’ shell is working fine so I'm using that one until the issue with IPython is fixed.
;;   See https://coderwall.com/p/xdox9a/running-ipython-cleanly-inside-a-virtualenv
;;       https://stackoverflow.com/questions/20327621/calling-ipython-from-a-virtualenv/38488717#38488717
;; REVIEW: Why is ‘"ipython"’ in the default value of ‘python-shell-completion-native-disabled-interpreters’? That causes the message “Shell native completion is disabled, using fallback” to appear in the echo area after calling ‘run-python’. If ‘ipython’ is removed from ‘python-shell-completion-native-disabled-interpreters’, IPython returns “In [1]: python.el: native completion setup loaded” after invoking it.
;;   Related? https://ipython.readthedocs.io/en/stable/whatsnew/version5.html#id2
;(setq python-shell-interpreter "ipython")
;(setq python-shell-interpreter-args "--simple-prompt --pprint -i")) ; See (2 links) https://ipython.readthedocs.io/en/stable/whatsnew/version5.html#id2, https://emacs.stackexchange.com/questions/37867/ipython-doesnt-work-in-a-virtual-environment

;; REVIEW: With ‘colored-stats’ enabled in ‘~/.inputrc’ and the interpreter set to ‘python’, the following warning is raised on invoking ‘run-python’:
;;     Warning (python): Your ‘python-shell-interpreter’ doesn’t seem to support readline, yet ‘python-shell-completion-native-enable’ was t and "python" is not part of the ‘python-shell-completion-native-disabled-interpreters’ list.  Native completions have been disabled locally.
;;   This can be fixed by enabling ‘colored-stats’ conditionally in ‘~/.inputrc’ with
;;       $if Bash
;;       set colored-stats on
;;       $endif
;;     See https://emacs.stackexchange.com/questions/30082/your-python-shell-interpreter-doesn-t-seem-to-support-readline
;;         https://debbugs.gnu.org/cgi/bugreport.cgi?bug=24401

(with-eval-after-load 'python
  (load (expand-file-name "python/venvs.el" my-emacs-directory))) ; This file contains “shortcut” functions for quick activation of virtual environments.
(setq company-anaconda-case-insensitive nil) ;                      REVIEW: This should make completion case sensitive but it doesn’t really seem to do its job.

(defun launch-terminal-activate-python-venv ()
  "Open a terminal in the current directory.
If a virtual environment is currently active in Emacs, activate
it in the terminal session as well."
  (interactive)
  (if python-shell-virtualenv-root
      (let ((command
             (concat
              ;; NOTE: This works with Gnome Terminal and Bash. It might break with other terminals or shells.
              ;; See https://stackoverflow.com/questions/7120426/how-to-invoke-bash-run-commands-inside-the-new-shell-and-then-give-control-bac/56834451#56834451
              my-terminal " -- " my-shell " -c '" my-shell " --rcfile <(echo \"source $HOME/.bashrc; source " python-shell-virtualenv-root "/bin/activate\")'")))
        (call-process-shell-command command nil 0))
    (ag-open-terminal-here)))
(with-eval-after-load 'python
  (define-key python-mode-map          (kbd "C-c t") #'launch-terminal-activate-python-venv)
  (define-key inferior-python-mode-map (kbd "C-c t") #'launch-terminal-activate-python-venv))

(defun set-up-python-mode ()
  "Function with customizations for ‘python-mode’."
  (add-hook 'before-save-hook #'delete-trailing-whitespace ; Delete trailing whitespace on save.
            nil 'local)
  (add-to-list (make-local-variable 'company-backends) ;     Company mode set-up for Python; see https://github.com/proofit404/company-anaconda
               '(company-anaconda :with company-capf))
  (scratchvenv) ;                                            Activate the “safety” virtual environment. See https://emacs.stackexchange.com/questions/18059/how-to-activate-python-virtual-environment-in-init-file
  (anaconda-mode) ;                                          Use ‘anaconda-mode’; see https://github.com/pythonic-emacs/anaconda-mode
  (anaconda-eldoc-mode))
(add-hook 'python-mode-hook #'set-up-python-mode)



;;; ESS set-up

;; These settings apply in both the R and Julia ESS modes.
;; https://ess.r-project.org/index.php?Section=documentation&subSection=manuals

(setq ess-use-auto-complete nil) ; Maybe this relieves Emacs of some unneeded burden.

;; Prevent ESS from overriding my TAB key binding
;; The original functions are described on https://ess.r-project.org/Manual/ess.html#Indenting
;; REVIEW: Does the ‘ess-style’ still apply when ESS’s indentation commands are unbound? It looks like it does, even though ‘indent-line-function’ (which sets the function that ‘indent-for-tab-command’ calls to indent the current line) is set to ‘indent-relative’.
(with-eval-after-load 'ess-mode
  (define-key ess-mode-map (kbd "TAB") nil)
  (define-key ess-mode-map (kbd "<tab>") nil))



;;; R

;; https://ess.r-project.org/index.php?Section=documentation&subSection=manuals

(with-eval-after-load 'ess-r-mode
  (set-keymap-parent ess-mode-map prog-mode-map) ;            REVIEW: This setting should be superfluous in future versions of ESS. “In the dev version ‘ess-mode’ derives from ‘prog-mode’ and so its keymap now inherits from the ‘prog-mode-map’.” https://github.com/emacs-ess/ESS/issues/1117
  (define-key ess-r-mode-map "_" #'ess-insert-assign) ;       Make the key ‘_’ insert the assigment operator ‘<-’.
  (define-key inferior-ess-r-mode-map "_" #'ess-insert-assign)
  (define-key inferior-ess-r-mode-map [home] #'comint-bol)) ; Make ‘<home>’ place the cursor after the prompt, not before it. ‘comint-bol’ is the function ‘C-a’ is bound to in the R REPL buffer.

(defun set-up-company-mode-R ()
  "Use ‘company-dabbrev-code’ together with other R-specific
backends."
  (setq-local ess-use-company nil) ;                          When ‘ess-use-company’ is set to t (the default) ESS takes care of loading some backends for Company. Setting it to nil allows complete control over which backends are loaded.
  (add-to-list (make-local-variable 'company-backends)
    '(:separate ;                                             Group candidates from different backends separately.
       company-R-library company-R-args company-R-objects ;   These are the backends ESS enables when ‘ess-use-company’ is set to t.
       company-dabbrev-code))) ;                              ‘company-dabbrev-code’ makes variables that have not yet been evaluated available as completion candidates. See https://github.com/emacs-ess/ESS/issues/565#issuecomment-396233702

;; ‘inferior-ess-r-mode’ (“iESS”) is used in the R shell buffer.
(defun set-up-inferior-ess-r-mode ()
  "Function with customizations for ‘inferior-ess-r-mode’."
  (add-hook 'before-save-hook #'delete-trailing-whitespace ;  Delete trailing whitespace on save. (2 links) https://www.emacswiki.org/emacs/DeletingWhitespace#toc3, https://stackoverflow.com/questions/6138029/how-to-add-a-hook-to-only-run-in-a-particular-mode
            nil 'local)
  (subword-mode) ;                                            Distinguish words in camelCase text. http://ergoemacs.org/emacs/emacs_subword-mode_superword-mode.html
  (set-up-company-mode-R)) ;                                  Set up word completion.
(add-hook 'inferior-ess-r-mode-hook #'set-up-inferior-ess-r-mode)

;; ‘ess-r-mode’ is used in R script (non-interactive) buffers.
(defun set-up-ess-r-mode ()
  "Function with customizations for ‘ess-r-mode’."
  (set-up-inferior-ess-r-mode)
  (display-fill-column-indicator-mode)) ;                     Display the ‘fill-column’ indicator (only for R scripts).
(add-hook 'ess-r-mode-hook #'set-up-ess-r-mode)



;;; Julia

;; https://github.com/emacs-ess/ESS/wiki/Julia

(add-to-list 'auto-mode-alist '("\\.jl\\'" . ess-julia-mode)) ; Open source code files in ‘ess-julia-mode’ instead of plain ‘julia-mode’.

(with-eval-after-load 'ess-julia
  (define-key inferior-ess-julia-mode-map [home] #'comint-bol)) ; Make ‘<home>’ place the cursor after the prompt, not before it. ‘comint-bol’ is the function ‘C-a’ is bound to in the Julia REPL buffer.

;; ‘inferior-ess-julia-mode’ (“iESS”) is used in the Julia REPL buffer.
(defun set-up-inferior-ess-julia-mode ()
  "Function with customizations for ‘inferior-ess-julia-mode’."
  (add-hook 'before-save-hook #'delete-trailing-whitespace ; Delete trailing whitespace on save.
            nil 'local)
  (setq-local electric-pair-text-syntax-table ;              Use ‘text-mode’’s automatic pairing set-up in strings and comments.
              text-syntax-table))
(add-hook 'inferior-ess-julia-mode-hook #'set-up-inferior-ess-julia-mode)

;; ‘ess-julia-mode’ is used in Julia source code buffers.
(defun set-up-ess-julia-mode ()
  "Function with customizations for ‘ess-julia-mode’."
  (set-up-inferior-ess-julia-mode)
  (display-fill-column-indicator-mode)) ;                    Display the ‘fill-column’ indicator (only in source code buffers).
(add-hook 'ess-julia-mode-hook #'set-up-ess-julia-mode)



;;; Fortran

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Fortran.html

;; Custom fontification
;; NOTE: these fontification instructions don’t work on continuation lines.
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Search_002dbased-Fontification.html
;; https://swsnr.de/blog/2014/03/26/search-based-fontification-with-keywords/
(font-lock-add-keywords 'f90-mode
  '(;; Fontify mathematical operators
    ;; https://lists.gnu.org/archive/html/help-gnu-emacs/2009-05/msg00012.html
    ("\\+" . font-lock-function-name-face)
    ("\\-" . font-lock-function-name-face)
    ("\\(\\*+\\)\\(?:[[:space:]]*\\)\\(?:\\.?[[:digit:]]\\|[[:alpha:]]\\|(\\)" 1 font-lock-function-name-face) ; The regexp tries to match only asterisks that mean multiplication or exponentiation by matching one or more asterisks that -- after any number of whitespace characters -- are followed by either a digit (optionally preceded by one dot), a letter or an opening parenthesis.
    ("\\/" . font-lock-function-name-face)
    ("\="  . font-lock-function-name-face)
    ("\<"  . font-lock-function-name-face)
    ("\>"  . font-lock-function-name-face)
    ;; Fontify specifiers
    ;; TODO: Add more statements (other than ‘open’ and ‘allocate’) and maybe more specifiers for the ‘open’ statement (other than ‘file’, ‘action’, etc.).
    ;;       See ~/Documents/Scuola/Informatica/Fortran/Handouts/Fortran mini-seminar series, mostly by Mark Branson/kiwi.atmos.colostate.edu/fortran/docs/2017/fortranIO2017.pdf
    ;;           https://annefou.github.io/Fortran/files/files.html
    ("allocate[[:space:]]*(.*\\(stat\\)" 1 font-lock-keyword-face)
    ("open[[:space:]]*(.*\\(access\\)"   1 font-lock-keyword-face)
    ("open[[:space:]]*(.*\\(action\\)"   1 font-lock-keyword-face)
    ("open[[:space:]]*(.*\\(file\\)"     1 font-lock-keyword-face)
    ("open[[:space:]]*(.*\\(form\\)"     1 font-lock-keyword-face)
    ("open[[:space:]]*(.*\\(iostat\\)"   1 font-lock-keyword-face)
    ("open[[:space:]]*(.*\\(position\\)" 1 font-lock-keyword-face)
    ("open[[:space:]]*(.*\\(recl\\)"     1 font-lock-keyword-face)
    ("open[[:space:]]*(.*\\(status\\)"   1 font-lock-keyword-face)
    ("open[[:space:]]*(.*\\(unit\\)"     1 font-lock-keyword-face)))

(setq f90-program-indent 3) ;                                       Chosen so that code that comes after a 3-digit label is aligned with the rest of the code.
(setq f90-do-indent 3)
(setq f90-if-indent 3)
(setq f90-type-indent 3)
(setq f90-continuation-indent 6) ;                                  Indent continued lines double the number of spaces that each block is indented, as suggested on http://www.fortran.com/Fortran_Style.pdf
(setq f90-beginning-ampersand nil) ;                                Don’t insert the ampersand at the beginning of the continuation line.
(setq f90-smart-end 'no-blink) ;                                    Reindent (using ‘indent-for-tab-command’) and insert ‘end’ statements (using ‘f90-insert-end’) without moving the cursor around.
(with-eval-after-load 'f90
  (setq f90-font-lock-keywords f90-font-lock-keywords-4) ;          Use the maximum level of syntax highlighting available; see ‘f90.el’ and https://jblevins.org/log/f90-mode. REVIEW: Why isn’t this the default? (It should be set to the maximum level because ‘font-lock-maximum-decoration’ is set to t.)
  (define-key f90-mode-map (kbd "RET") #'reindent-then-newline-and-indent)) ; Make RET indent the current line and the one it inserts. https://stackoverflow.com/questions/823745/how-do-i-make-emacs-auto-indent-my-c-code
(setq flycheck-gfortran-args ;                                      This variable tells Flycheck’s checker which ‘gfortran’ options to use. I also use it to set the ‘compile-command’. REVIEW: Maybe it makes more sense to do it the other way around, adjusting this variable to the ‘compile-command’, but this way I can set only ‘flycheck-gfortran-args’ and have the ‘compile-command’ adjusts on its own; doing it in the opposite direction is not as easy.
      '(;"-ffree-line-length-none" ;                                Remove the line length limit.
        "-fcheck=bounds" ;                                          Generate run-time checks to ensure that array subscripts stay within bounds.
        "-std=f2018")) ;                                            Enforce strict conformance to the Fortran 2018 standard.

;; Compile and run programs

;; Change the default command for ‘M-x compile’, unless a makefile already exists or the file local variables already specify it. See ‘C-h v compile-command’.
(defun f90-c&r-set-compile-command ()
  "Set the ‘compile-command’ for ‘f90-mode’ buffers."
  (unless (or (file-exists-p "makefile")
              (file-exists-p "Makefile")
              (assq 'compile-command file-local-variables-alist)) ; Don’t override the file-local value. Note: don’t just check if ‘compile-command’ already has a value because it’s a global variable and its value might not be the right one for the current buffer.
    (setq-local compile-command
                (mapconcat #'identity ; Here I need to concatenate strings with a space as separator. ‘concat’ doesn’t use a separator, so I use ‘mapconcat #'identity’ instead. See also ‘string-join’ (which requires loading ‘subr-x.el’, however), via https://stackoverflow.com/questions/12999530/is-there-a-function-that-joins-a-string-into-a-delimited-string/40633410#40633410
                           (list "gfortran"
                                 (combine-and-quote-strings flycheck-gfortran-args)
                                 (c&r-source-file-name)
                                 "-o"
                                 (c&r-compiled-file-name)) " "))))

(defun f90-c&r-set-run-command ()
  "Set the shell command that calls the executable after a
successful compilation."
  (unless (assq 'c&r-run-command file-local-variables-alist)
    (setq-local c&r-run-command (c&r-compiled-file-name))))

;; Note about the ‘fill-column’: the standard imposes a limit of 132 characters to the line length [1, § 6.3.2.1]. 80 characters as a limit is suggested on p. 5 of [2]. 1 https://j3-fortran.org/doc/year/18/18-007r1.pdf   2 http://www.fortran.com/Fortran_Style.pdf
(defun set-up-f90-mode ()
  "Function with customizations for ‘f90-mode’."
  ;(setq buffer-face-mode-face ;                                          Use Letter Gothic. It hasn’t got a dotted or slashed zero.
  ;      '(:family "LetterGothic" :weight bold :height 110))
  ;(buffer-face-mode)
  (highlight-indent-guides-mode) ;                                        Display indentation guides.
  (display-fill-column-indicator-mode) ;                                  Display the ‘fill-column’ indicator.
  (add-hook 'before-save-hook #'delete-trailing-whitespace ;              Delete trailing whitespace on save. (2 links) https://www.emacswiki.org/emacs/DeletingWhitespace#toc3, https://stackoverflow.com/questions/6138029/how-to-add-a-hook-to-only-run-in-a-particular-mode
            nil 'local)
  (setq-local electric-pair-text-syntax-table ;                           Use ‘text-mode’’s automatic pairing set-up in strings and comments.
              text-syntax-table)
  (yas-minor-mode) ;                                                      Use YASnippet.
  (setq c&r-compiled-file-extension "out") ;                              Set up Compile and Run.
  (setq c&r-set-compile-command-function #'f90-c&r-set-compile-command)
  (setq c&r-set-run-command-function #'f90-c&r-set-run-command))
(add-hook 'f90-mode-hook #'set-up-f90-mode)
